﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TitleManager : MonoBehaviour
{
    public int Shift;//タイトル画面のフロー管理
    public int SelectNum;//選択中の項目
    private float CoolTime;//選択中の項目が移動しすぎないようにするため
    public GameObject MainTitle;//メインタイトル
    private GameObject[] TitleTextList = new GameObject[5];//選択肢をまとめて管理する
    private bool OptionFlag;

    public GameObject Knight;//騎士
    private Vector3 KnightPos;//騎士の移動履歴取得用
    private float Angle;//騎士の向き調整用
    private float AngleReal;//向きを滑らかに変更する用
    private float X ;//画面手前で騎士を往復、テキストを上下に動かすのに使用
    private float Y ;//タイトル画面起動時のカメラ位置調整用
    private float CameraSpeed;//カメラのスピード
    public GameObject MainCamera;//カメラの位置調整用

    // Start is called before the first frame update
  
    void Start()
    {
        //騎士が最初に表示される位置をランダムにする
        X = Random.Range(0,10000);
        //カメラ位置調整用
        Y = -90;
        //初めにオープニングを再生する
        Shift =2;     
        //カメラ、タイトルテキストの初期位置
        MainCamera.transform.position = new Vector3(0,3,135);
        MainCamera.transform.rotation = Quaternion.Euler(0,0,0);
        MainTitle.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, -130 + Y * 5);
        //プレイデータをロードする
        PlayDataManager.DataLoad();
        //テキストのオブジェクトをリストにまとめる
        if (TitleTextList[0] == null)for (int i = 0; i < 5; i++) TitleTextList[i] = GameObject.Find("TitleText" + i);
        
        //テキストなどの表示を整える
        TitleStatus();
        //カメラの移動速度を０にする
        CameraSpeed = 0;
        //オプション画面を非表示にする
        OptionFlag = false;
        //画面が開いたら音楽を再生する
        SoundManager.Instance.PlayBgmByName("BGM1", "Main");
        //別なシーンから移動してきた場合はスクリーンを下げ、オープニングをスキップする
        if (SceneMove.SceneName != null)
        {
            MainCamera.transform.position = new Vector3(0, 30, -40);
            MainCamera.transform.rotation = Quaternion.Euler(5, 0, 0);
            MainTitle.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, -130);

            //ここから鳥の鳴き声が聞こえるようにする
            SoundManager.Instance.PlayBgmByName("TitleBird", "Sub");
            Shift = 4;
        }
    }
    

    //操作関連のアップデート
    void Update()
    {
        switch (Shift)
        {
            case 2://オープニング中にボタンを押したら演出をスキップする
                if (Input.anyKeyDown) Y = 0;
                break;
            case 1://メニュー画面の動作
                TitleMain();
                break;
        }
    }
    //
    void FixedUpdate()
    {
        X++;
        //もくじ
        switch (Shift)
        {           
            case 0://文字の大きさ、位置を調整する
                TitleStatus();
                Shift = 1;
                break;           
            case 1: //メニュー画面の動作
                TitleMainFix();
                break;            
            case 2://タイトル画面を開いた時のカメラ動作
                TitleOpning();
                break;           
            case 3: //次のシーンへ移動する
                NextScene();
                break;
            case 4://別なシーンから移動してきたときの動作
                BackTitle();
                break;
        }
        //騎士を歩かせる
        KnightMove();
    }
    //文字の大きさ、位置の調整
    void TitleStatus()
    {       
        //キャラ1のロックが解除されていない場合(データの有無確認の為)
        if (PlayDataManager.CharaLock[0] == 0)
        {
            //コンテニューを選択できないように
            if (SelectNum < 1) SelectNum = 1;
            //コンテニューを非表示にする
            TitleTextList[0].SetActive(false);
        }
        else TitleTextList[0].SetActive(true);
        //最終ステージクリア済みの場合クレジットを再生できるように
        if(PlayDataManager.ClearStage == PlayDataManager.StageMax)
        {
            TitleTextList[3].GetComponent<Text>().text = "CREDIT";
            TitleTextList[4].GetComponent<Text>().text = "EXIT";
        }
        else
        {
            TitleTextList[3].GetComponent<Text>().text = "EXIT";
            TitleTextList[4].GetComponent<Text>().text = "";
        }
        //テキストの位置を初期化する
        for(int i = 0;i<5;i++)TitleTextList[i].GetComponent<RectTransform>().anchoredPosition = new Vector3(0,130-i*110,0);
        //一度全てのテキストの大きさを元に戻す
        for (int i = 0; i < 5; i++) TitleTextList[i].transform.localScale = new Vector3(1, 1, 1);
        //選択中の項目のみ大きく表示する
        TitleTextList[SelectNum].transform.localScale = new Vector3(1.3f, 1.3f, 1.3f);      
        CoolTime = 20;
    }
    //タイトル画面の動作
    void TitleMain()
    {
        if (Input.GetButtonDown("Mouse_StartButton"))
        {
            PlayDataManager.DataDebug();
            Shift = 0;
        }
        if (Input.GetKeyDown(KeyCode.RightShift))
        {
            Shift = 0;
            PlayDataManager.DataDelete();
        }
        //選択中の項目の履歴を録る
        int SelectNumLog = SelectNum;
        //上下入力で選択した値の変更
        if (Input.GetAxisRaw("Vertical") == 0 && Input.GetAxisRaw("Pad_D_V") == 0) CoolTime = 0;
        if ((Input.GetKeyDown(KeyCode.UpArrow) || Input.GetAxisRaw("Vertical") > 0 || Input.GetAxisRaw("Pad_D_V") > 0 || Input.GetAxis("Mouse_ScrollWheel") > 0) && CoolTime <= 0) SelectNum--;
        if ((Input.GetKeyDown(KeyCode.DownArrow) || Input.GetAxisRaw("Vertical") < 0 || Input.GetAxisRaw("Pad_D_V") < 0 || Input.GetAxis("Mouse_ScrollWheel") < 0) && CoolTime <= 0) SelectNum++;

        //上限を超えないようにする
        if (SelectNum > 4) SelectNum = 4;
        if (SelectNum < 0) SelectNum = 0;
        //キャラ1のロックが解除されていない場合
        if (PlayDataManager.CharaLock[0] != 1 && SelectNum < 1) SelectNum = 1;
        //最終ステージをクリアしていない場合
        if(PlayDataManager.ClearStage != PlayDataManager.StageMax && SelectNum > 3)SelectNum = 3;
        //エンターキーを押したとき次のシーンへ移動
        if (Input.GetKeyDown(KeyCode.KeypadEnter) ||Input.GetButtonDown("Mouse_Fire1") || Input.GetButtonDown("Pad_Fire1"))
        {
            SoundManager.Instance.PlaySeByName("TitleEnter", gameObject);
            Shift = 3;
        }
        //選択中の項目が履歴と異なる場合文字の大きさ、位置を調整して表示を更新する
        if (SelectNum != SelectNumLog)
        {
            SoundManager.Instance.PlaySeByName("TitleSelect", gameObject);
            Shift = 0;
        }
    }
    //タイトル画面の表示
    void TitleMainFix()
    {            
        //選択中の項目のみスイングするように
        TitleTextList[SelectNum].GetComponent<RectTransform>().anchoredPosition = new Vector3(0,130-SelectNum*110+15*Mathf.Cos(X/50),0);
        //クールタイムを減らす
        if(CoolTime-- > 0) { }
    }
    //騎士を歩かせる
    void KnightMove()
    {
        //騎士は一定の場所を往復する
        Knight.transform.position = new Vector3(20 * Mathf.Cos(X / 600), 0, 10);

        //プレイヤーの移動後と移動前の座標から角度を算出
        Angle = Mathf.Atan2(Knight.transform.position.z - KnightPos.z, Knight.transform.position.x - KnightPos.x) * Mathf.Rad2Deg;
        //振り向く角度が180度を上回っていた場合
        if (Angle - AngleReal > 180) AngleReal = 360 + AngleReal;
        if (Angle - AngleReal < -180) AngleReal = -360 + AngleReal;
        //滑らかに方向転換できる関数を用意
        AngleReal += (Angle - AngleReal) / 20;

        //プレイヤーが移動していた場合向きを変更する
        if (Knight.transform.position.x != KnightPos.x || Knight.transform.position.z != KnightPos.z)
            Knight.transform.rotation = Quaternion.Euler(0, -AngleReal + 90.0f, 0);

        //プレイヤーの座標を更新
        KnightPos = Knight.transform.position;
    }
    //カメラを動かす
    void TitleOpning()
    {
        if (CameraSpeed <0.8f)CameraSpeed+=0.01f;
        //一定位置までカメラを後ろに移動
        if (MainCamera.transform.position.z >= -13)
            MainCamera.transform.position = new Vector3(0, 3, MainCamera.transform.position.z - CameraSpeed);
        //一定位置まで移動後、弧を書くようにカメラを移動する
        if (MainCamera.transform.position.z < -13)
        {
            MainCamera.transform.position = new Vector3(0, 30 + 27 * Mathf.Sin(Y * Mathf.Deg2Rad), -13 - 27 * Mathf.Cos(Y * Mathf.Deg2Rad));
            MainCamera.transform.rotation = Quaternion.Euler((float)(Y + 90) / 18, 0, 0);
            MainTitle.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, -130 - Y * Y/5);
            Y += 1.0f;
        }
        //移動し終えているか何かしらの操作をした場合オープニングを終了する
        if (Y >= 0 || Input.anyKeyDown)
        {
            MainCamera.transform.position = new Vector3(0, 30, -40);
            MainCamera.transform.rotation = Quaternion.Euler(5, 0, 0);
            MainTitle.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, -130);
            Shift = 0;

            //ここから鳥の鳴き声が聞こえるようにする
            SoundManager.Instance.PlayBgmByName("TitleBird", "Sub");
        }
    }
    //次のシーンへ移動
    void NextScene()
    {
  
        switch (SelectNum)//タイトルセレクトの値で分岐
        {
            case 0://ステージセレクトへ移動
                if (SceneMove.Close())
                { 
                    SceneManager.LoadScene("StageSelect");
                }
                break;
            case 1://データを初期化し、ステージセレクトへ移動
                PlayDataManager.DataReset();
                PlayDataManager.DataSave();
                if (SceneMove.Close())
                {
                    SceneManager.LoadScene("StageSelect");
                }
                break;
            case 2://オプション画面へ移動する
                if (OptionFlag == false)
                {
                    if (SceneMove.Close())
                    {
                        SceneManager.LoadScene("Option", LoadSceneMode.Additive);
                        OptionFlag = true;
                    }
                }
                if (OptionFlag == true && SceneManager.sceneCount == 1) Start();
                break;
            case 3://エンドロールを再生するかアプリを終了する               
                if (PlayDataManager.ClearStage == PlayDataManager.StageMax)
                {
                    if (SceneMove.Close3())
                    {
                        SceneManager.LoadScene("EndRoll");
                    }
                }
                else Application.Quit();
                break;
            case 4://アプリを終了する
                Application.Quit();
                break;

        }
    }
    //別なシーンからの移動
    void BackTitle()
    {
        //ゲームオーバーかエンドロールから移動してきたときはフェードインする
        if(SceneMove.SceneName == "GameOver" || SceneMove.SceneName == "EndRoll")    
        {
            if(SceneMove.Open3())Shift = 0;
        }
        else if(SceneMove.Open2()) Shift = 0;
    }
}
