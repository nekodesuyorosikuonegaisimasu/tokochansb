﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//主にドラゴンの動きを扱うcs
public class BossDragonManager : MonoBehaviour
{
    private Animator animator; // アニメーション

    [Header("ステータス")]
    [SerializeField] Transform DragonPosition; // 対象オブジェクト
    public int Hp; // 体力
    public int Interval; // 攻撃の間隔
    public float CoolCount; //攻撃の間隔 
    public int Type = 1; // 攻撃の種類
    public GameObject Haze; // 靄

    [Header("火炎")]
    public GameObject Player;   // プレイヤー
    public GameObject BossFire; // 対象オブジェクト
    public GameObject FirePoint; // 火炎が出る場所
    public float span = 1f; // 火炎の次弾までの間隔

    private float step = 0;
    private float speed = 0.01f; //向くスピード(秒速)

    [Header("落石")]
    [SerializeField] int ArrangementMaxRedius = 100; // 配置位置の最大半径
    [SerializeField] int ArrangementMinRedius = 50; // 配置位置の最小半径
    [SerializeField] int ArrangementHeight = 10; // 配置位置の高さ
    public GameObject RockPrefab; // 対象オブジェクト
    public int CreatureLength = 500; // 配置位置の最大
    
    private System.Random random; // 乱数機
    private int x; // 
    private int z; // 
    private double xAbs; // 
    private double zAbs; // 
    private double maxR; // 
    private double minR; // 
    private GameObject[] CreatureRange; // 

    private bool OnceFlag; // コルーチンやfor文に一回だけ入りたいときに使うフラグ
    private bool ScreamFlag; // 叫びは一回だけ入るようにするためのフラグ
    private bool DieFlag; // 死んだときに呼び出されるフラグ

    Quaternion tra;

    void Start()
    {
        Player = GameObject.Find("Player"); // Playerを探す
        // オブジェクト取得
        FirePoint = transform.Find("Root/Spine01/Spine02/Chest/Neck01/Neck02/Neck03/Head/Jaw01/FirePoint").gameObject;

        animator = GetComponent<Animator>();// アニメーションの取得

        DieFlag = false;
        CoolCount = 0; // 直ぐに最初の一撃を始めるために0にする。
        Type = 1; // Typeは待機にしておく
        Hp = 5; // 体力
        if (Interval == 0)
        {
            Interval = 20;
        }

        CreatureRange = new GameObject[CreatureLength];
        random = new System.Random();


        maxR = Math.Pow(ArrangementMaxRedius, 2);
        minR = Math.Pow(ArrangementMinRedius, 2);
    }


    void Update()
    {
        AnimationReset();

        if (CoolCount <= 0 && Haze.GetComponent<BossHaze>().StartFlag)
        {
            Type = UnityEngine.Random.Range(2, 4);
            Debug.Log(Type);
            CoolCount = Interval;
            StopCoroutine("Fire");
            OnceFlag = true;
            ScreamFlag = true;
        }
        CoolCount -= Time.deltaTime;

        if (Hp == 0) Type = 0;

        if (Type == 0)
        {
            StopCoroutine("Fire");
            Die();
        }
        if (Type == 1) // ボス戦が始まるまでの待機
        {
            animator.SetBool("Idle2", true);
        }
        if (Type == 2)// 火炎
        {
            //秒速に直す計算

            step += speed * Time.deltaTime;

            //向き始めと終わりの点を計算して、stepの値より、今向くべき方向を算出

            transform.rotation = Quaternion.Slerp(DragonPosition.rotation, Quaternion.LookRotation((Player.transform.position - DragonPosition.position).normalized), step);
            //transform.rotation = Quaternion.AngleAxis(-transform.rotation.x, Vector3.right);
            transform.rotation = Quaternion.Euler(0, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z);

            if (ScreamFlag) Scream();
            FlameAttack();
        }

        if (Type == 3)// 落石
        {
            if (ScreamFlag) Scream();
            RockFall();
        }
        //if (Type ==4) { } // Typeの数を増やせば攻撃手段等を増やせます。
        
    }

    public void AnimationReset()
    {
        animator.SetBool("Idle2", false);
        animator.SetBool("Scream", false);
        animator.SetBool("Flame Attack", false);
    }

    public void Scream() // 叫び
    {
        SoundManager.Instance.PlaySeByName("DragonVoice", this.gameObject);
        animator.SetBool("Scream", true);
        ScreamFlag = false;
    }

    public void RockFall() // 落石
    {
        animator.SetBool("Scream", true);
        if (OnceFlag)
        {
            OnceFlag = false;
            for (int i = 0; i < CreatureRange.Length; i++)
            {
                while (CreatureRange[i] == null)
                {
                    x = random.Next(-ArrangementMaxRedius, ArrangementMaxRedius);
                    z = random.Next(-ArrangementMaxRedius, ArrangementMaxRedius);

                    xAbs = Math.Abs(Math.Pow(x, 2));
                    zAbs = Math.Abs(Math.Pow(z, 2));

                    // 特定の範囲内化確認
                    if (maxR > xAbs + zAbs && xAbs + zAbs > minR)
                    {
                        GameObject Rock = Instantiate(RockPrefab, (new Vector3(x, ArrangementHeight, z)) + DragonPosition.position, transform.rotation);
                        CreatureRange[i] = Rock;
                    }
                }
            }
        }
    }

    public void FlameAttack() // 火炎
    {
        animator.SetBool("Flame Attack", true);
        if (OnceFlag)
        {
            OnceFlag = false;
            StartCoroutine("Fire");
        }
    }

    public void Die() // 死んだ
    {
        if (!DieFlag)
        {
            GameObject BigRock = Instantiate(RockPrefab, (new Vector3(0, DragonPosition.position.y + 100, 0)) + DragonPosition.position, transform.rotation);
            BigRock.GetComponent<Transform>().transform.localScale = new Vector3(5,5,5);
            animator.SetBool("Die", true);
            DieFlag = true;
        }
        Destroy(gameObject, 10.0f);
    }

    private IEnumerator Fire() // 炎のコルーチン
    {
        while (true)
        {
            Instantiate(BossFire, new Vector3(FirePoint.transform.position.x, FirePoint.transform.position.y + 1.0f, FirePoint.transform.position.z), Quaternion.AngleAxis(gameObject.transform.localEulerAngles.y, Vector3.up));
            yield return new WaitForSeconds(span);
        }
    }
}
