﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Guillotine : MonoBehaviour
{
    public GameObject Blade; //刃
    private int SwingRange;//刃の振れ幅
    public float SwingZ;
    // Start is called before the first frame update
    void Start()
    {
        SwingZ = Random.Range(0,360);
        SwingRange = 55;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        SwingZ ++;        
        Blade.transform.rotation = Quaternion.Euler(transform.eulerAngles.x,transform.eulerAngles.y,transform.eulerAngles.z - SwingRange*(Mathf.Abs(Mathf.Sin(SwingZ * Mathf.Deg2Rad) + Mathf.Sin(90*Mathf.Deg2Rad)))+SwingRange);
    }
}
