﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//扉に関する処理
public class Anim_Door : MonoBehaviour
{
    [SerializeField]
    private Animator animator;//アニメーターをセット
    private GameObject KeyZone;//カギとなる当たり判定
    public GameObject StopCol; //扉がしまっている時姫を止める判定

    int Count; //一回だけ実行する用のカウント

    void Start()
    {
        //扉を閉める。
        animator.SetBool("Flg_Key", false);
        //鍵ゾーンの取得
        int i = 0;
        while (this.gameObject.name != "Door"+i){i++;}//次
        KeyZone = GameObject.Find("KeyZone" +i);
        StopCol.SetActive(true); //止まる
        Count = 0; //空
    }
    //ドアを開く
    void OpenAnime()
    {
        //扉を開ける。
        animator.SetBool("Flg_Key", true);
    }

    void Update()
    {
        //条件：指定のオブジェクトの名前変更。　処理：扉が開く。
        if(KeyZone.name == "Open" && Count==0)
        {
            OpenAnime(); //扉を開く
            StopCol.GetComponent<StopPrincess>().GoPrinces();
            StopCol.SetActive(false); //姫の追跡を開始
            Count++;
        }
    }
}
