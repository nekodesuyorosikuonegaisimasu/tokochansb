﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerChangeManager : MonoBehaviour
{
    // ボタンを押した時にカウント
    public static int ChangeCount;
    // PlayerManager
    private GameObject PlayerManager;
    // Camera
    private GameObject MainCameraObj;
    private GameObject SubCameraObj;
    // UseItem
    private GameObject UseItemObj;
    // L2を押したクールタイム
    private float L2CoolTime;
    // 現在のプレイヤー
    private GameObject PlayerObj;
    private Transform PlayerPos;
    // エフェクト
    [SerializeField, Tooltip("エフェクト")]
    private GameObject Effect;
    private GameObject EffectInstance;
    // プレイヤー切り替えUI
    private GameObject PlayerChangeUIObj;

    // Start is called before the first frame update
    void Start()
    {
        // PlayerManagerを探す
        PlayerManager = GameObject.Find("PlayerManager");
        // カメラを探す
        MainCameraObj = GameObject.Find("Main Camera");
        SubCameraObj = GameObject.Find("Sub Camera");
        // UseItemを探す
        UseItemObj = GameObject.Find("ItemUI");
        // PlayerChangeUIを探す
        PlayerChangeUIObj = GameObject.Find("PlayerChangeUI");
        // ChangeCountの初期化
        ChangeCount = 0;
        // L2を押したフラグの初期化
        L2CoolTime = 0;
    }

    // Update is called once per frame
    void Update()
    {
        // ポーズ中は操作を受け付けない
        if (Mathf.Approximately(Time.timeScale, 0f)) return;

        // L2を押したら
        if ((Input.GetKeyDown("q") /*デバッグ用*/|| Input.GetAxis("Pad_L2") > 0) && L2CoolTime == 0)
        {
            // 切り替えクールタイム設定
            L2CoolTime = 2.0f;
            // 切り替えクールタイム
            Count();
            // 切り替え後のプレイヤー生成
            PlayerManager.GetComponent<PlayerManager>().PlayerSpown(ChangeCount);
            // エフェクト生成
            EffectInstance = (GameObject)Instantiate(Effect, PlayerManager.GetComponent<PlayerManager>().BeforePlayer.transform.position, Quaternion.identity);
            // 切り替え前のプレイヤーを削除
            Destroy(PlayerManager.GetComponent<PlayerManager>().BeforePlayer.gameObject);
            // カメラを切り替え後のプレイヤーに合わせる
            MainCameraObj.GetComponent<Camera>().PlayerChange();
            // アイテムを切り替え後のプレイヤーに合わせる
            UseItemObj.GetComponent<UseItem>().PlayerChange();
            // 切り替えUIを変更する
            PlayerChangeUIObj.GetComponent<PlayerChangeUI>().IsChange = true;
        }
        // プレイヤー切り替えのクールタイム処理
        if(L2CoolTime != 0)
        {
            L2CoolTime -= Time.deltaTime;
            if (L2CoolTime < 0) L2CoolTime = 0;
        }
    }
    // L2を押した回数を変更する
    void Count()
    {
        switch(ChangeCount)
        {
            case 0 :
                ChangeCount++;
                break;
            case 1 :
                ChangeCount--;
                break;
            default :
                break;
        }
    }
}
