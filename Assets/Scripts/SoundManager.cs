﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    // シングルトン
    private static SoundManager instance;

    // インスペクター上で操作可、0～1が範囲、名前のヒント
    [SerializeField, Range(0, 1), Tooltip("マスタ音量")]
    float volume = 1;
    [SerializeField, Range(0, 1), Tooltip("BGMの音量")]
    float bgmVolume = 1;
    [SerializeField, Range(0, 1), Tooltip("SEの音量")]
    float seVolume = 1;

    // ここにBGM,SEを入れる
    AudioClip[] bgm;
    AudioClip[] se;
    // 文字型のキーと整数型の値をBGM,SEそれぞれで定義
    Dictionary<string, int> bgmIndex = new Dictionary<string, int>();
    Dictionary<string, int> seIndex = new Dictionary<string, int>();
    // オーディオソース
    AudioSource bgmAudioSourceMain;
    AudioSource bgmAudioSourceSub;
    AudioSource seAudioSource;

    // 情報を返す
    public static SoundManager Instance
    {
        get
        {
            if(instance == null)
            {
                instance = (SoundManager)FindObjectOfType(typeof(SoundManager));
            }
            return instance;
        }
    }


    // マスタの音量を設定
    public float Volume
    {
        set
        {
            // 音量を0～1に制限
            volume = Mathf.Clamp01(value);
            // BGM,SEの音量を設定
            bgmAudioSourceMain.volume = bgmVolume * volume;
            bgmAudioSourceSub.volume = bgmVolume * volume;
            seAudioSource.volume = seVolume * volume;
        }
        get
        {
            return volume;
        }
    }
    // BGMの音量のみを設定
    public float BgmVolume
    {
        set
        {
            // 音量を0～1に制限
            bgmVolume = Mathf.Clamp01(value);
            // BGMの音量を設定
            bgmAudioSourceMain.volume = bgmVolume * volume;
            bgmAudioSourceSub.volume = bgmVolume * volume;
        }
        get
        {
            return bgmVolume;
        }
    }
    // SEの音量のみを設定
    public float SeVolume
    {
        set
        {
            // 音量を0～1に制限
            seVolume = Mathf.Clamp01(value);
            // SEの音量を設定
            seAudioSource.volume = seVolume * volume;
        }
        get
        {
            return seVolume;
        }
    }

    public void Awake()
    {
        // 別のサウンドマネージャーが生成された場合
        if(this != Instance)
        {
            Destroy(gameObject);
            return;
        }
        // シーンをまたいで使うので無くならないようにする
        DontDestroyOnLoad(gameObject);
        // AudioSourceのアタッチ
        bgmAudioSourceMain = gameObject.AddComponent<AudioSource>();
        bgmAudioSourceSub = gameObject.AddComponent<AudioSource>();
        seAudioSource = gameObject.AddComponent<AudioSource>();
        // AudioClipの読み込み
        bgm = Resources.LoadAll<AudioClip>("Audio/BGM");
        se = Resources.LoadAll<AudioClip>("Audio/SE");
        // DictionaryのところにBGMを入れる
        for(int i = 0; i < bgm.Length; i++)
        {
            bgmIndex.Add(bgm[i].name, i);
        }
        // DictionaryのところにSEを入れる
        for (int i = 0; i < se.Length; i++)
        {
            seIndex.Add(se[i].name, i);
        }
    }

    // 他のスクリプトから飛ばした名前をDictionaryから見つける
    public int GetBgmIndex(string name)
    {
        if(bgmIndex.ContainsKey(name))
        {
            // 名前の番号を返す
            return bgmIndex[name];
        }
        else
        {
            Debug.LogError("その名前のBGMファイルはないので間違っていますよ");
            return 0;
        }
    }
    // 他のスクリプトから飛ばした名前をDictionaryから見つける
    public int GetSeIndex(string name)
    {
        if (seIndex.ContainsKey(name))
        {
            // 名前の番号を返す
            return seIndex[name];
        }
        else
        {
            Debug.LogError("その名前のSEファイルはないので間違っていますよ");
            return 0;
        }
    }

    // BGMの再生
    public void PlayBgm(int index, string playName)
    {
        if(playName == "Main")
        {
            // indexを0～bgm数の範囲内に収める
            index = Mathf.Clamp(index, 0, bgm.Length);
            // BGMをセット
            bgmAudioSourceMain.clip = bgm[index];
            // ループ再生
            bgmAudioSourceMain.loop = true;
            // 音量
            bgmAudioSourceMain.volume = BgmVolume * Volume;
            // 音の再生
            bgmAudioSourceMain.Play();
        }
        if(playName == "Sub")
        {
            // indexを0～bgm数の範囲内に収める
            index = Mathf.Clamp(index, 0, bgm.Length);
            // BGMをセット
            bgmAudioSourceSub.clip = bgm[index];
            // ループ再生
            bgmAudioSourceSub.loop = true;
            // 音量
            bgmAudioSourceSub.volume = BgmVolume * Volume;
            // 音の再生
            bgmAudioSourceSub.Play();
        }
        //Debug.Log("BGM: " + bgm[index]);
    }
    // 見つけたBGMを再生処理に飛ばす
    public void PlayBgmByName(string Bgmname, string playName)
    {
        PlayBgm(GetBgmIndex(Bgmname), playName);
        PlayBgm(GetBgmIndex(Bgmname), playName);
    }
    // BGMの停止処理
    public void StopBgm()
    {
        // BGMの停止
        bgmAudioSourceMain.Stop();
        bgmAudioSourceSub.Stop();
        // セットしたBGMをリセット
        bgmAudioSourceMain.clip = null;
        bgmAudioSourceSub.clip = null;
    }

    // SEの再生
    public void PlaySe(int index, GameObject objname)
    {
        //// indexを0～SE数の範囲内に収める
        //index = Mathf.Clamp(index, 0, se.Length);
        //// SEを一度だけ再生（音量も指定）
        //seAudioSource.PlayOneShot(se[index], SeVolume * Volume);

        // indexを0～SE数の範囲内に収める
        index = Mathf.Clamp(index, 0, se.Length);
        // SEを一度だけ再生（音量も指定）
        objname.GetComponent<AudioSource>().PlayOneShot(se[index], SeVolume * Volume);
        //Debug.Log("SE: " + se[index]);
    }
    // 見つけたSEを再生処理に飛ばす
    public void PlaySeByName(string name, GameObject objname)
    {
        if(objname.GetComponent<AudioSource>() == null)
        {
            objname.AddComponent<AudioSource>();
            if(objname.name == "Princess" || objname.name == "BigDoll(Clone)" || objname.name == "DragonFire(Clone)" || objname.name == "RedDragon")
            {
                objname.GetComponent<AudioSource>().spatialBlend = 1;
                objname.GetComponent<AudioSource>().rolloffMode = AudioRolloffMode.Linear;
                objname.GetComponent<AudioSource>().maxDistance = 45;
                if(objname.name == "RedDragon") objname.GetComponent<AudioSource>().maxDistance = 150;
                objname.GetComponent<AudioSource>().volume = 1;
            }
            else objname.GetComponent<AudioSource>().spatialBlend = 0;
        }
        PlaySe(GetSeIndex(name), objname);
    }
    // SEの停止処理
    public void StopSe()
    {
        // SEの停止
        seAudioSource.Stop();
        // セットしたSEをリセット
        seAudioSource.clip = null;
    }

    void Start()
    {
    }
    void Update()
    { 
    }
}
