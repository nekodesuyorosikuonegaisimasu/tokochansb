﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectItem : MonoBehaviour
{
    // スプライト
    public Sprite[] itemSprite;
    // オブジェクト
    public GameObject[] item;
    // 画像
    public Image[] itemImage;          
    // アイテムの種類
    public int itemType;
    // アイテムの数
    public int[] itemCount;             
    // 現在選択中のアイテム
    public int NowItem;                
    // アイテム効果処理
    UseItem useItem;
    // プレイヤー
    private GameObject player;
    // アイテムの効果説明テキスト
    private string[] ItemText;
    // テキストオブジェクト
    [SerializeField]
    private GameObject textBox;
    // マウスホイールの数値を取得
    private float mouseWheel;

    // Start is called before the first frame update
    void Start()
    {
        // アイテムUICanvasの子オブジェクトのImageに設定したImageを格納
        item = new GameObject[itemType];
        itemImage = new Image[itemType];
        for(int i = 0; i < itemType; i++)
        {
            item[i] = GameObject.Find("Item" + i);
            item[i].GetComponent<Image>().sprite = itemSprite[i];
            itemImage[i] = item[i].GetComponent<Image>();
            item[i].SetActive(false);
        }
        // 選択中のアイテムの初期値
        NowItem = 1;
        // 選択中のアイテムをもとにアイテムUIを描画
        for (int i = - 1; i < 2; i++)
        {
            item[NowItem + i].SetActive(true);
            itemImage[NowItem + i].GetComponent<Transform>().localScale = new Vector3(1.0f, 1.0f, 1.0f);
            if(i==0)itemImage[NowItem + i].GetComponent<Transform>().localScale = new Vector3(1.5f, 1.5f, 1.5f);
            item[NowItem + i].GetComponent<RectTransform>().anchoredPosition = new Vector3(650 + (i * 150), -420, 0);
        }
        item[NowItem - 1].GetComponent<RectTransform>().anchoredPosition = new Vector3(650 - 150, -445, 0);
        item[NowItem + 1].GetComponent<RectTransform>().anchoredPosition = new Vector3(650 + 150, -445, 0);
        // アイテム効果処理
        useItem = GetComponent<UseItem>();

        player = GameObject.Find("Player");// + PlayerChangeManager.ChangeCount

        //アイテムの効果テキストを取得し配列に変換する
        string LoadStr = (Resources.Load("ItemText", typeof(TextAsset)) as TextAsset).text;
        ItemText = LoadStr.Split(char.Parse("\n"));
        textBox.GetComponent<Text>().text = ItemText[NowItem];

        mouseWheel = 0;
    }

    // Update is called once per frame
    void Update()
    {
        // ポーズ中は操作を受け付けない
        if (Mathf.Approximately(Time.timeScale, 0f)) return;

        // プレイヤーがいなくなったときに探す処理
        if (player == null)
        {
            player = GameObject.Find("Player");
            useItem.coolTimeFlag = false;
        }

        // アイテム消費の処理
        if ((Input.GetButtonDown("Mouse_Square") || Input.GetButtonDown("Pad_Square")) && itemCount[NowItem] > 0 && !useItem.coolTimeFlag) 
        {
            SoundManager.Instance.PlaySeByName("UseItem", player);
            // アイテムの所持数を1減らす
            itemCount[NowItem] -= 1;
            // アイテム効果処理へ
            useItem.SelectedItem(NowItem);
            //アイテムがなくなったら薄黒くする
            if(itemCount[NowItem] == 0) itemImage[NowItem].color = new Color(35.0f/255.0f,35.0f / 255.0f, 35.0f / 255.0f);
        }
        // 選択中のアイテムを切り替える
        if ((Input.GetButtonDown("Pad_R1") || Input.GetAxis("Mouse_ScrollWheel") > 0) && NowItem < itemType - 1)
        {
            NowItem += 1;
            Draw();
        }
        if ((Input.GetButtonDown("Pad_L1") || Input.GetAxis("Mouse_ScrollWheel") < 0) && NowItem > 0)
        {
            NowItem -= 1;
            Draw();
        }
    }
    // 描画
    void Draw()
    {
        // 一度すべてのアイテムを非アクティブにする
        for (int i = 0; i < itemType; i++)
        {
            item[i].SetActive(false);
            itemImage[i].GetComponent<Transform>().localScale = new Vector3(1, 1, 1);
        }
        // 選択中のアイテムが両端でないなら
        if(NowItem != 0 && NowItem != itemType - 1)
        {
            // 選択中のアイテムとその左右のアイテムUIを表示
            for(int i = - 1; i < 2; i++)
            {
                item[NowItem + i].SetActive(true);
                itemImage[NowItem + i].GetComponent<Transform>().localScale = new Vector3(1.0f, 1.0f, 1.0f);
                if (i == 0) itemImage[NowItem + i].GetComponent<Transform>().localScale = new Vector3(1.5f, 1.5f, 1.5f);
                item[NowItem + i].GetComponent<RectTransform>().anchoredPosition = new Vector3(650 + (i * 150), -420, 0); 
            }
            item[NowItem - 1].GetComponent<RectTransform>().anchoredPosition = new Vector3(650 - 150, -445, 0);
            item[NowItem + 1].GetComponent<RectTransform>().anchoredPosition = new Vector3(650 + 150, -445, 0);
        }
        // 選択中のアイテムが0（左端）なら
        else if(NowItem == 0)
        {
            // 選択中のアイテムとその右のアイテムUIを表示
            for (int i = 0; i < 2; i++)
            {
                item[NowItem + i].SetActive(true);
                itemImage[NowItem + i].GetComponent<Transform>().localScale = new Vector3(1.0f, 1.0f, 1.0f);
                if (i == 0) itemImage[NowItem + i].GetComponent<Transform>().localScale = new Vector3(1.5f, 1.5f, 1.5f);
                item[NowItem + i].GetComponent<RectTransform>().anchoredPosition = new Vector3(650 + (i * 150), -420, 0);
            }
            item[NowItem + 1].GetComponent<RectTransform>().anchoredPosition = new Vector3(650 + 150, -445, 0);
        }
        // 選択中のアイテムが右端なら
        else if (NowItem == itemType - 1)
        {
            // 選択中のアイテムとその左のアイテムUIを表示
            for (int i = -1; i < 1; i++)
            {
                item[NowItem + i].SetActive(true);
                itemImage[NowItem + i].GetComponent<Transform>().localScale = new Vector3(1.0f, 1.0f, 1.0f);
                if (i == 0) itemImage[NowItem + i].GetComponent<Transform>().localScale = new Vector3(1.5f, 1.5f, 1.5f);
                item[NowItem + i].GetComponent<RectTransform>().anchoredPosition = new Vector3(650 + (i * 150), -420, 0);
            }
            item[NowItem - 1].GetComponent<RectTransform>().anchoredPosition = new Vector3(650 - 150, -445, 0);
        }
        textBox.GetComponent<Text>().text = ItemText[NowItem];
    }
}
