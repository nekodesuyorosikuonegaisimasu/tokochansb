﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverManager : MonoBehaviour
{
    public static int ReasonNum = 0;//死因記録用
    
    public GameObject GameoverText;//ゲームオーバーテキスト
    private string[] GameOverReason;//ゲームオーバーになった理由
    public int SelectNum;//項目選択用
    public GameObject SelectArrow;//項目選択用
    public int Shift;
    private GameObject[] TextList = new GameObject[3];//選択肢をまとめて管理する
    private float CoolTime;//選択肢が動きすぎないよう
    public GameObject Knight;//騎士
    public GameObject Angel;//プレイヤー昇天用
    public GameObject AngelLight;//昇天用パーティクル
    public GameObject RecoverLight;//復活用ライト
    
    public Animator animator;//騎士のアニメーション
    private float AnimeTimer;//アニメの挙動調整用
    // Start is called before the first frame update
    void Start()
    {
        //死因をまとめたテキストを取得し配列に変換する
        string LoadStr = (Resources.Load("DeadList", typeof(TextAsset)) as TextAsset).text;
        GameOverReason = LoadStr.Split(char.Parse("\n"));
        //とりあえず死因はランダムで表示
        //ReasonNum = Random.Range(0,3);
        //死因によって表示するテキストを変更
        GameoverText.GetComponent<Text>().text = GameOverReason[ReasonNum];
        //最初は一番上を選択
        SelectNum  =0;  
        //リストに各テキストを挿入する
        for(int i = 0;i<3;i++)TextList[i] = GameObject.Find("Text"+i);
        //所持PPが足りない時はリトライを灰色にする
        //if (PlayDataManager.PrincesPoint < PlayDataManager.CLERA_REWARD[PlayDataManager.StageNum-1] / 3) TextList[0].GetComponent<Text>().color = Color.gray;
        //正面を向いた状態にしておく
        AnimeTimer = 0;
        Shift = 0;

        //音楽を鳴らす
        SoundManager.Instance.PlayBgmByName("GameOverC","Main");
        SoundManager.Instance.PlayBgmByName("GameObird","Sub");

        //// マウスキーボード操作ならカーソルを表示する
        //if(!ControllerChecker.CheckController)Cursor.visible = true;
    }
    //操作関連のアップデート
    void Update()
    {
        switch (Shift)
        {
            case 1://項目を選択する
                GameOverSelect();
                break;
        }
    }

    //表示関連のアップデート
    void FixedUpdate()
    {
        switch (Shift)
        {
            case 0://画面に表示する内容の情報を取得
                GameOverStatus();
                Shift = 1;
                break;
            case 1://選択画面の動作
                GameOverSelectFix();
                break;
            case 2://リトライ選択時
                Retry();
                break;
            case 3://ステージセレクト選択時
                StageSelect();
                break;
            case 4://タイトル選択時
                Title();
                break;
        }
        AnimeTimer++;
    }
    void GameOverStatus()
    {
        //クールタイムを設定
        CoolTime = 20;
        //一度全てのテキストの大きさを元に戻す
        for (int i = 0; i < 3; i++) TextList[i].transform.localScale = new Vector3(1, 1, 1);
        //リトライ時の金額をPPUIに贈る
        //if(SelectNum == 0)PrincessPointUI.PPsub = PlayDataManager.CLERA_REWARD[PlayDataManager.StageNum-1]/3;
        //else PrincessPointUI.PPsub = 0;
          //選択中の項目のみ大きく表示する
        // TextList[SelectNum].transform.localScale = new Vector3(1.3f, 1.3f, 1.3f);
    }
    void GameOverSelect()
    {
        //選択中の項目のログを録る
        int SelectLog = SelectNum;
        //操作が無かったらクールタイムを解消
        if (Input.GetAxisRaw("Vertical") == 0 && Input.GetAxisRaw("Pad_D_V") == 0) CoolTime = 0;
        //上下操作で値を変更
        if ((Input.GetKeyDown(KeyCode.UpArrow) || Input.GetAxisRaw("Vertical") > 0 || Input.GetAxisRaw("Pad_D_V") > 0 || Input.GetAxis("Mouse_ScrollWheel") > 0) && CoolTime <= 0) SelectNum--;
        if ((Input.GetKeyDown(KeyCode.DownArrow) || 0 > Input.GetAxisRaw("Vertical") || Input.GetAxisRaw("Pad_D_V") < 0 || Input.GetAxis("Mouse_ScrollWheel") < 0) && CoolTime <= 0) SelectNum++;
        //値が上限値を上回らないようにする
        if (SelectNum < 0) SelectNum = 0;
        if (SelectNum > 2) SelectNum = 2;
        //エンター入力をしたら次のシーンへ
        if (Input.GetKeyDown(KeyCode.KeypadEnter) ||Input.GetButtonDown("Mouse_Fire1") || Input.GetButtonDown("Pad_Fire1"))
        {
            if (SelectNum == 0)
            {
                //SoundManager.Instance.PlaySeByName("CharaShopNG", gameObject);
                Shift = SelectNum + 2;
                AnimeTimer = 0;
            }
            else
            {
                //BGMを止め、効果音を鳴らす
                SoundManager.Instance.StopBgm();
                SoundManager.Instance.PlaySeByName("GameOverOK", gameObject);
                Shift = SelectNum + 2;
                AnimeTimer = 0;
            }
        }
        //選択中の項目がログと違っていたら情報を取得しなおす
        if (SelectNum != SelectLog)
        {
            SoundManager.Instance.PlaySeByName("GameOverSelect", gameObject);
            Shift = 0;
        }
    }
    void GameOverSelectFix()
    {
        //クールタイムの減少
        if (CoolTime-- > 0) { }
        //選択中の項目に沿って矢印が移動、ゆらゆらする       
        SelectArrow.GetComponent<RectTransform>().anchoredPosition = new Vector3(280 + SelectNum * 100 + 15 * Mathf.Sin(AnimeTimer / 50), -25 - SelectNum * 125, 0);

    }
    //リトライ選択時
    void Retry()
    {
        RecoverLight.SetActive(true);
        RecoverLight.GetComponent<Light>().intensity = 100-1.5f*AnimeTimer;
        //矢印を点滅させる
        if (AnimeTimer > 120 || AnimeTimer % 10 > 5) SelectArrow.SetActive(false);
        else SelectArrow.SetActive(true);
        //騎士を立たせる
        if (AnimeTimer == 0)animator.GetComponent<Animator>().SetBool("Recover", true);
        if (AnimeTimer >= 130)
        {
            //起き上がったプレイヤーを走らせる
            Knight.transform.position = new Vector3(Knight.transform.position.x+0.05f, Knight.transform.position.y, Knight.transform.position.z+0.15f);          
            //前回選択したステージへ移行     
            if (Knight.transform.position.z >= 11)if(SceneMove.Close()) SceneManager.LoadScene("Stage" + PlayDataManager.WorldStageNum); ;                               
        }       
    }
    //ステージセレクト選択時
    void StageSelect()
    {
        //矢印を点滅させる
        if (AnimeTimer > 120 || AnimeTimer % 10 > 5) SelectArrow.SetActive(false);
        else SelectArrow.SetActive(true);
        //騎士を立たせる
        if (AnimeTimer == 0) animator.GetComponent<Animator>().SetBool("Standup", true);
        if (AnimeTimer >= 240)
        {
            //起き上がったプレイヤーをゆっくり歩かせる
            Knight.transform.position = new Vector3(Knight.transform.position.x - 0.05f, Knight.transform.position.y, Knight.transform.position.z);            
            //前回選択したステージへ移行     
            if (Knight.transform.position.x <= -8)if(SceneMove.Back())SceneManager.LoadScene("StageSelect");
        }
    }
    //タイトル選択時
    void Title()
    {
        AngelLight.SetActive(true);
        //矢印を点滅させる
        if (AnimeTimer > 120 || AnimeTimer % 10 > 5) SelectArrow.SetActive(false);
        else SelectArrow.SetActive(true);
        //効果音を鳴らす
        if(AnimeTimer == 30)SoundManager.Instance.PlaySeByName("Angel",gameObject);
        //プレイヤーを上昇しながら大きくさせる
        if (AnimeTimer < 90) Angel.transform.localScale = new Vector3(Mathf.Pow(AnimeTimer/90,2), Mathf.Pow(AnimeTimer / 90, 2), Mathf.Pow(AnimeTimer / 90, 2));
        Angel.transform.position = new Vector3(0, Angel.transform.position.y + 0.05f, -1);
        Angel.transform.rotation = Quaternion.Euler(Knight.transform.localEulerAngles.x, 0, 0);
        AngelLight.transform.position = Angel.transform.position;
        if (Angel.transform.position.y >= 12)if(SceneMove.Close3())SceneManager.LoadScene("Title");
    }
}
