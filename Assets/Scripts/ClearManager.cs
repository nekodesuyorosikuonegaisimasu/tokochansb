﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.AI;
using UnityEngine.UI;

public class ClearManager : MonoBehaviour
{
    public static bool ClearFlag;//クリアフラグ(クリア条件を満たしたら立てる)
    public static int TreasureBonus;//宝箱を開けた際のボーナスポイント用

    //カメラ
    private GameObject PauseManager;
    //ClearUI
    private float TimerA;
    private float TimerB;
    private int ClearShift;
    public GameObject ClearCanvas;
    public GameObject ClearText;
    private GameObject[] ClearStatusList = new GameObject[3];
    private GameObject[] ClearCategoryList = new GameObject[3];
    public GameObject PPBounusText;
    public GameObject TreasureText;
    public GameObject TreasurePoint;
    public GameObject ClearNext;
    private int Timer = 150;//アイコンが移動するのに要する時間
    private int SliderX;//アイコン移動用

    private float Smoothing;

    void Start()
    {
        //カメラを探す
        PauseManager = GameObject.Find("PauseManager");
        //リザルト画面
        //クリアフラグを立てない、その他値のリセットをしておく
        ClearFlag = true;
        TreasureBonus = 0;
        if (SceneManager.GetActiveScene().name == "Stage4")
        { // hogehogeシーンでのみやりたい処理
            ClearShift = 0;
        }
        else
        { // それ以外のシーンでやりたい処理
            ClearShift = 51;
        }
        
        //各種テキストを探して、見つけ次第非表示にしておく
        for (int i = 0; i < 3; i++)
        {
            ClearCategoryList[i] = GameObject.Find("ClearCategory" + i);
            ClearCategoryList[i].SetActive(false);
            ClearStatusList[i] = GameObject.Find("ClearStatus" + i);
            ClearStatusList[i].SetActive(false);
        }
        ClearCanvas.SetActive(false);



    }
    void Update()
    {
        //update環境でfixedupdateと同じ動きにするために使用
        //TimeSyncを複数のスクリプトで同時に呼び出すと動作不良を起こす可能性がある為
        //ステージの進捗ゲージもまとめて管理します
        if (TimeSync())
        {
            if (ClearFlag)
            {
                switch (ClearShift)
                {
                    case 0://クリアキャンバスの準備＆セーブ
                        ClearPrepare();
                        break;
                    case 1://ゲームクリアと表示する
                        if (ClearTextApp(ClearText))
                        {
                            //表示が終わったら次に動かすテキストを表示し、次の手順へ移動
                            ClearCategoryList[0].SetActive(true);
                            ClearStatusList[0].SetActive(true);
                            ClearShift = 2;
                        }
                        break;
                    case 2://項目の書かれたテキストを表示する
                        if (ClearTextSlide(ClearCategoryList[0], ClearStatusList[0], 0))
                        {
                            //表示が終わったら次に動かすテキストを表示し、次の手順へ移動
                            ClearCategoryList[1].SetActive(true);
                            ClearStatusList[1].SetActive(true);
                            ClearShift = 3;
                        }
                        break;

                    case 3://項目の書かれたテキストを表示する
                        if (ClearTextSlide(ClearCategoryList[1], ClearStatusList[1], 1))
                        {
                            //表示が終わったら次に動かすテキストを表示し、次の手順へ移動
                            ClearCategoryList[2].SetActive(true);
                            ClearStatusList[2].SetActive(true);
                            ClearShift = 4;
                        }
                        break;
                    case 4://項目の書かれたテキストを表示する
                        if (ClearTextSlide(ClearCategoryList[2], ClearStatusList[2], 2))
                        {
                            //表示が終わったら次に動かすテキストを表示し、次の手順へ移動
                            TimerA = 0;
                            TimerB = 0;
                            ClearShift = 5;
                        }
                        break;
                    case 5://獲得PPにボーナスを加算する
                        if (TimerA >= 15 && !PPBounusText.activeSelf) PPBounusText.SetActive(true);
                        if (TimerA++ > 40) //if (GetPPAdd())
                        {
                            //次の手順へ移動
                            PPBounusText.SetActive(false);
                            TimerA = 0;
                            TimerB = 0;
                            ClearShift = 8;
                            if (TreasureBonus != 0)
                            {
                                TreasureText.SetActive(true);
                                ClearShift = 6;
                            }

                        }
                        break;
                    case 6:
                        if (ClearTextApp(TreasureText)) ClearShift = 7;
                        break;
                    case 7://獲得PPに宝箱分のボーナスを加算する
                        if (TimerA++ > 15) //if (GetTreasure())
                        {
                            TreasureText.SetActive(false);
                            TimerA = 0;
                            TimerB = 0;
                            ClearShift = 8;
                        }
                        break;

                    case 8://獲得PPを所持PPに加算する
                        if (TimerA++ > 50) //if (PPlogAdd())
                        {
                            //テキストを表示し、次の手順へ移動する
                            ClearNext.SetActive(true);
                            TimerA = 0;
                            TimerB = 0;
                            ClearShift = 50;
                        }
                        break;
                    case 50://次のシーンへ移動する
                        NextScene();
                        break;
                    case 51://シーンにやってきたときの動作
                        if (SceneMove.Open())
                        {
                            ClearFlag = false;
                            ClearShift = 0;
                        }
                        break;
                }
            }
        }
    }
    //クリアUIを動かすための準備をする
    void ClearPrepare()
    {
        //クリアしたことがないステージだったらクリアステージを更新
        if (PlayDataManager.ClearStage < PlayDataManager.StageNum) PlayDataManager.ClearStage = PlayDataManager.StageNum;
        //データを保存する
        PlayDataManager.DataSave();
        //クリア画面UIを表示する
        ClearCanvas.SetActive(true);
        //各種項目の値をセットしておく

        ClearShift = 1;
    }
    //文字が小さくなっていく演出用
    bool ClearTextApp(GameObject AppText)
    {
        TimerA += 0.05f;
        TimerB += TimerA;
        AppText.GetComponent<RectTransform>().localScale = new Vector2(6 - TimerB, 6 - TimerB);
        if (TimerB >= 5)
        {
            TimerA *= -0.6f;
            if (Mathf.Abs(TimerA) < 0.05f) TimerA = 0;
        }
        if (TimerA == 0.0f)
        {
            TimerA = 0;
            TimerB = 0;
            return true;
        }
        return false;
    }
    //値を横からスライドする
    bool ClearTextSlide(GameObject SlideCategory, GameObject SlideStatus, int PosY)
    {
        if (Input.anyKeyDown) TimerB++;
        if (TimerB != 0) TimerA += 50;
        TimerA += 50;
        //左右からテキストがスライドして出てくる
        SlideCategory.GetComponent<RectTransform>().anchoredPosition = new Vector3(-100 + TimerA, 50 - PosY * 100, 0);
        SlideStatus.GetComponent<RectTransform>().anchoredPosition = new Vector3(100 - TimerA, 50 - PosY * 100, 0);
        //画像が画面を覆う位置まで移動してきたら値を初期化してtrueを返す
        if (TimerA >= 800)
        {
            TimerA = 800;
            SlideCategory.GetComponent<RectTransform>().anchoredPosition = new Vector3(-100 + TimerA, 50 - PosY * 100, 0);
            SlideStatus.GetComponent<RectTransform>().anchoredPosition = new Vector3(100 - TimerA, 50 - PosY * 100, 0);
            TimerA = 0;
            return true;
        }
        return false;
    }


    //次のシーンへ移る動作
    void NextScene()
    {
        //ボタンを押したらタイマーを加算
        if (Input.anyKeyDown) TimerB++;
        //タイマーが0でなくなったら画面を閉じ、別のシーンへ移動する
        if (TimerB != 0.0f)
        {
            //クリアしたステージが最終ステージだった場合はエンドロールへ
            if (PlayDataManager.StageNum == PlayDataManager.StageMax)
            {
                if (SceneMove.Close3())
                {
                    //時を再び動かす
                    Time.timeScale = 1;
                    SceneManager.LoadScene("EndRoll");
                }
            }
            if (PlayDataManager.StageNum != PlayDataManager.StageMax)
            {
                if (SceneMove.Close())
                {
                    //クリアフラグを下げておく
                    ClearFlag = false;
                    //// マウスキーボード操作ならカーソルを表示する
                    //if(!ControllerChecker.CheckController)Cursor.visible = true;
                    //時を再び動かす
                    Time.timeScale = 1;
                    //ステージセレクトに移動
                    SceneManager.LoadScene("StageSelect");
                }
            }
        }
    }
    //fixedupdateでもtimescale = 0のupdateでも同じ動きが出来るようにする
    //TimeSyncとopen、closeを同時に使用すると動作がおかしくなるので注意
    bool TimeSync()
    {
        Smoothing += Time.unscaledDeltaTime;
        if (Smoothing >= Time.fixedUnscaledDeltaTime)
        {
            Smoothing = 0;
            return true;
        }
        return false;
    }
}
