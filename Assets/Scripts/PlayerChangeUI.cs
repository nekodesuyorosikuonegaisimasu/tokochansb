﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerChangeUI : MonoBehaviour
{
    // 近距離と遠距離のUI
    [SerializeField]
    private GameObject WeaponUI1, WeaponUI2;
    // プレイヤー切り替え中フラグ
    public bool IsChange;
    // 切り替えカウント
    private float Count;
    // 切り替え回数カウント
    public int CountTime;

    // Start is called before the first frame update
    void Start()
    {
        // 切り替えフラグの初期化
        IsChange = false;
        // 最初は近距離のUIを表示する
        WeaponUI1.GetComponent<Image>().fillAmount = 1;
        WeaponUI2.GetComponent<Image>().fillAmount = 1;
        // カウント初期化
        Count = 0;
        CountTime = 0;
    }

    // Update is called once per frame
    void Update()
    {
        // プレイヤー切り替えをしたらカウント処理へ
        if (IsChange) PlayerChange();

        if (IsChange)
        {
            Count += Time.deltaTime;
            // アニメーションの停止
            if (Count > 0.2)
            {
                WeaponUI1.GetComponent<Animator>().SetBool("MainToSub", false);
                WeaponUI2.GetComponent<Animator>().SetBool("MainToSub", false);
                WeaponUI1.GetComponent<Animator>().SetBool("SubToMain", false);
                WeaponUI2.GetComponent<Animator>().SetBool("SubToMain", false);
                // 切り替えボタンを押した回数を更新
                CountTime++;
                // その他初期化
                Count = 0;
                IsChange = false;
            }
        }
    }

    // プレイヤー切り替え時に大きさと位置を変える処理
    void PlayerChange()
    {
        if (CountTime % 2 == 0)
        {
            // 騎士UIから魔法使いUIに変えるアニメーション
            WeaponUI1.GetComponent<Animator>().SetBool("SubToMain", false);
            WeaponUI2.GetComponent<Animator>().SetBool("MainToSub", false);
            WeaponUI1.GetComponent<Animator>().SetBool("MainToSub", true);
            WeaponUI2.GetComponent<Animator>().SetBool("SubToMain", true);
            // 切り替え中フラグ
            IsChange = true;
            // 魔法使いUIを騎士UIより上に表示させる
            WeaponUI2.GetComponent<RectTransform>().SetAsLastSibling();
        }
        else
        {
            // 魔法使いUIから騎士UIに変えるアニメーション
            WeaponUI1.GetComponent<Animator>().SetBool("MainToSub", false);
            WeaponUI2.GetComponent<Animator>().SetBool("SubToMain", false);
            WeaponUI1.GetComponent<Animator>().SetBool("SubToMain", true);
            WeaponUI2.GetComponent<Animator>().SetBool("MainToSub", true);
            // 切り替え中フラグ
            IsChange = true;
            // 騎士UIを魔法使いUIより上に表示させる
            WeaponUI1.GetComponent<RectTransform>().SetAsLastSibling();
        }
    }
}
