﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// ドロップアイテムを扱うcs
public class DropItemManager : MonoBehaviour
{
    private float NowAxis; // アイテムがふわふわと上下するための変数
    public int itemType; // アイテムの種類
    public int[] itemCount; // アイテムの数
    private int ItemNumber; // ドロップアイテムのタイプナンバー
    //private GameObject DropItemBox; // ドロップアイテムの親の空オブジェクト
    //public GameObject dropItem; // プレハブのドロップアイテム

    GameObject ItemObj; // アイテム画像用オブジェクト

    private bool getItemNumber; // タイプを取る前にコライダーが反応しないためのbool
    private bool moveFlag; // 地面に落ちたらふわふわ動かすためのbool
    public bool choice; // 最初からアイテムを配置するときに特定のアイテムにするためのbool

    private GameObject player;

    void Start()
    {
        getItemNumber = false; // 初期化 
        moveFlag = false; // 初期化
        ItemObj = GameObject.Find("ItemUI"); // 画像用にItemUIを取得する
        if(!choice)itemType = Randomnumber(); // choiceされていないならアイテムタイプをランダムで入れる
        getItemNumber = true; // タイプを取った後なら取得できるようにする

        NowAxis = this.transform.position.y; // ふわふわ動く動きの場所を決める用
        
        

        // ItemTypeに対応した画像を描画させる
        this.gameObject.GetComponent<SpriteRenderer>().sprite = ItemObj.GetComponent<SelectItem>().itemSprite[itemType];

        player = GameObject.Find("Player");
    }

    void Update()
    {
        // プレイヤー切り替え時の処理
        if(player == null) player = GameObject.Find("Player");

        if (moveFlag)
        {
            // ふわふわ浮くような動きをさせる
            transform.position = new Vector3(transform.position.x, NowAxis + Mathf.PingPong(Time.time / 3, 0.5f), transform.position.z);
        }
        else
        {
            // moveFlagがonになるまでNowAxisの値は変え続ける(地面についたらonになるので)
            NowAxis = this.transform.position.y;
        }
        //ゆっくり回る動き(一秒で90度)
        transform.Rotate(new Vector3(0, 90, 0) * Time.deltaTime, Space.World);
    }
    // 触れたら処理する
    public void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.CompareTag("Player") && getItemNumber) // プレイヤーに触れたら
        {
            SoundManager.Instance.PlaySeByName("GetItem", player);
            Type();
        }
        else if (collider.gameObject.CompareTag("Ground")) // 地面に触れたら
        {
            moveFlag = true;
        }
    }

    public void Type() // タイプに応じてアイテム数を増やす
    {
        ItemObj.GetComponent<SelectItem>().itemCount[itemType] += 1;
        //アイテムの所持数が0から1になったときアイテムのアイコンを元に戻す(白くする)
        if (ItemObj.GetComponent<SelectItem>().itemCount[itemType] == 1)
            {ItemObj.GetComponent<SelectItem>().itemImage[itemType].color = new Color(1.0f, 1.0f, 1.0f); }
        Destroy(this.gameObject); // アイテムを消す
    }
    public int Randomnumber() // ランダムでドロップアイテムのタイプを取る
    {
        // 15からアイテムの種類+1の間の数字をランダムで決める
        ItemNumber = Random.Range(1, (ItemObj.GetComponent<SelectItem>().itemType - 1) * 15 + 1);
        ItemNumber = ItemNumber / 15; // 決めた数字を15で割る(これで7種類になる)
        return (ItemNumber); // 0から6の値を返す
    }
}
