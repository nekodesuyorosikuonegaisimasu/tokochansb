﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SceneMove : MonoBehaviour
{
    private static GameObject Screen;
    private static GameObject Fade;
    private static float SliderX;
    private static float Gravity;
    private static float Smoothing;
    private static bool SEFlag;
    public static string SceneName; //現在のシーン名を控える
    
    //static無しでも動かせるが手間が増えるのでstaticでやってます。
    public void Awake()
    {
        //スクリーンが空でない場合はオブジェクトを消す
        if (Screen != null) Destroy(gameObject);
        //スクリーンが空のときは画像を探す
        if (Screen == null) Screen = GameObject.Find("SliderPrincess");
        if (Fade == null) Fade = GameObject.Find("FadeBlack");
        //シーンを跨いでもオブジェクトが消えないようにする
        DontDestroyOnLoad(gameObject);
        //画像を動かすのに必要な値を初期化しておく
        Initialize();
    }
    //UpdateでもfixedUpdateでも同じような動きをするようにしたい

    //画面を開くときの動作１
    public static bool Open()
    {
        //スクリーンが空の場合はエラーの発生を防ぐためtrueを返す
        if (Screen == null) return true;
        SliderX += 50;
        //画像を左にスライドする
        Screen.GetComponent<RectTransform>().anchoredPosition = new Vector3(-SliderX, 0, 0);
        if (SliderX >= 150 && SEFlag == false)
        {
            SoundManager.Instance.PlaySeByName("SceneOpen1", Screen.gameObject);
            SEFlag = true;
        }
        //deltatimeをリセットしてカクツキ防止する用のフラグ
        //画面の外までスライドできたらtrueを返す
        if (SliderX > 2500)
        {
            Initialize();
            return true;
        }
        return false;
    }
    //画面を開くときの動作２
    public static bool Open2()
    {
        //スクリーンが空の場合はエラーの発生を防ぐためtrueを返す
        if (Screen == null) return true;
        Gravity += 4;
        SliderX += Gravity;
        //画像を加速させながら下にスライドする
        Screen.GetComponent<RectTransform>().anchoredPosition = new Vector3(0, -SliderX, 0);
        //画面の外までスライドできたらtrueを返す
        if (SliderX > 1080)
        {
            Initialize();
            return true;
        }
        return false;
    }
    //画面を開くときの動作３
    public static bool Open3()
    {
        //スクリーンが空の場合はエラーの発生を防ぐためtrueを返す
        if (Screen == null) return true;
        Fade.GetComponent<Image>().color = Fade.GetComponent<Image>().color - new Color(0, 0, 0, (float)2 / 255);
        if (Fade.GetComponent<Image>().color.a <= 0)
        {
            Initialize();
            return true;
        }
        return false;
    }
    //画面を閉じるときの動作１
    public static bool Close()
    {
        //スクリーンが空の場合はエラーの発生を防ぐためtrueを返す
        if (Screen == null) return true;
        //BGMの音を小さくしていく
        SoundManager.Instance.BgmVolume--;
        SliderX += 50;
        //画面右から画像がスライドして出てくる
        Screen.GetComponent<RectTransform>().anchoredPosition = new Vector3(2500 - SliderX, 0, 0);
        if (SliderX >= 150 && SEFlag == false)
        {
            SoundManager.Instance.PlaySeByName("SceneClose1", Screen.gameObject);
            SEFlag = true;
        }
        //画像が画面を覆う位置まで移動してきたら値を初期化してtrueを返す
        if (SliderX > 2500)
        {
            SceneName = SceneManager.GetActiveScene().name;
            SoundManager.Instance.StopBgm();
            Initialize();
            return true;
        }
        return false;
    }
    //画面を閉じるときの動作２
    public static bool Close2()
    {
        //スクリーンが空の場合はエラーの発生を防ぐためtrueを返す
        if (Screen == null) return true;
        //BGMの音を小さくしていく
        SoundManager.Instance.BgmVolume--;
        Gravity += 6;
        SliderX += Gravity;
        //画面上から画像が落ちてくる
        Screen.GetComponent<RectTransform>().anchoredPosition = new Vector3(0, 1080 - SliderX, 0);
        //画面下でバウンドする
        if (SliderX >= 1080)
        {
            SoundManager.Instance.PlaySeByName("SceneCloseBound", Screen.gameObject);
            Gravity *= -0.6f;
            //バウンドの値が小さくなってきたら値を0にする
            if (Mathf.Abs(Gravity) < 6) Gravity = 0;
        }
        if (SliderX >= 150 && SEFlag == false)
        {
            SoundManager.Instance.PlaySeByName("SceneClose2", Screen.gameObject);
            SEFlag = true;
        }
        //画像が落ちる速度が0になったら値を初期化してtrueを返す
        if (Gravity == 0)
        {
            Screen.GetComponent<RectTransform>().anchoredPosition = new Vector3(0, 0, 0);
            SceneName = SceneManager.GetActiveScene().name;
            SoundManager.Instance.StopBgm();
            Initialize();
            return true;
        }       
        return false;
    }
    //画面を開くときの動作３
    public static bool Close3()
    {
        //スクリーンが空の場合はエラーの発生を防ぐためtrueを返す
        if (Screen == null) return true;
        //BGMの音を小さくしていく
        SoundManager.Instance.BgmVolume--;
        Fade.GetComponent<Image>().color = Fade.GetComponent<Image>().color + new Color(0, 0, 0, (float)2 / 255);
        if (Fade.GetComponent<Image>().color.a >= 1)
        {
            SceneName = SceneManager.GetActiveScene().name;
            SoundManager.Instance.StopBgm();
            return true;
        }
        return false;
    }

    //前のシーンへ戻るときの動作
    public static bool Back()
    {
        //スクリーンが空の場合はエラーの発生を防ぐためtrueを返す
        if (Screen == null) return true;
        //BGMの音を小さくしていく
        SoundManager.Instance.BgmVolume--;
        SliderX += 50;
        //画面左から画像がスライドして出てくる
        Screen.GetComponent<RectTransform>().anchoredPosition = new Vector3(-2500 + SliderX, 0, 0);
        if (SliderX >= 150 && SEFlag == false)
        {
            SoundManager.Instance.PlaySeByName("SceneClose1", Screen.gameObject);
            SoundManager.Instance.StopBgm();
            SEFlag = true;
        }
        //画像が画面を覆う位置までスライドしてきたら値を初期化しtrueを返す
        if (SliderX > 2500)
        {
            SceneName = SceneManager.GetActiveScene().name;
            Initialize();
            return true;
        }
        return false;
    }
    
    //画像を動かすのに必要な値を初期化する(Openを使った後は必ず使っておくように)
    public static void Initialize()
    {
        SoundManager.Instance.BgmVolume = (float)PlayDataManager.VolumeList[1] / 100;
        SliderX = 0;
        Gravity = 0;
        Smoothing = 0;
        SEFlag = false;
    }
}
