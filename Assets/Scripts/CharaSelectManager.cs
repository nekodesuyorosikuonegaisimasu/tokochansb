﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using  UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CharaSelectManager : MonoBehaviour
{
    public GameObject SpotLight;//スポットライト
    public GameObject BigLight;//大きめのスポットライト
    private GameObject[] CharaList;//キャラクターをまとめて管理する
    private int CharaTotal;//キャラクターの総数
    private int CharaShift;//キャラクター選択のモード管理
    private int CharaTbl; //キャラクターテーブル
    private int CharaTblLog;//キャラクターテーブルの履歴
    private float CharaMove;//キャラ移動用
    private string[] FlavorStrins;//フレーバーテキスト表示用の文字列
    public GameObject FlavorCanvas;//フレーバーテキスト
    public GameObject FlavorHint;//フレーバーテキストの開き方
    private int FlavorFlug;//フレーバーテキスト表示を切り替えるフラグ
    private float MoveSecond;//キャラ移動にかかる時間
    public GameObject PriceText;//キャラクターの値段を表示
    public GameObject JobText;//職名を表示
    public GameObject StartText;//ステージ開始テキスト
    public GameObject StatusText;//ステータス表示
    private string StatusString;//ステータス表示に使用する文字列
    private bool SceneOpenFlag;//画面が開いたかどうか確認用
    public GameObject UI;
    public GameObject Back;
    // Start is called before the first frame update
    void Start()
    {
        //キャラクターの総数を入力
        CharaTotal = 5;
        //ステージ3が解放済みな場合キャラ追加
        if(PlayDataManager.OpenStage >=3)CharaTotal = PlayDataManager.CHARA_NAME.Length;
        //キャラが移動にかかる時間を設定
        MoveSecond = 60;
        //キャラクターテーブルに前回選択したキャラクターの値を挿入
        CharaTbl = PlayDataManager.CharaNum;
        //ジョブリストを作成
        CharaList = new GameObject[CharaTotal];
        //ジョブリストに各ジョブを登録
        for(int i = 0;i<CharaTotal;i++)
        {
            CharaList[i] = GameObject.Find("Job"+(i));
            //各ジョブに合わせた武器を装備する
            CharaList[i].GetComponent<MenuCharactor>().WeaponActive(i);
        }
        //キャラの回転速度を設定
        CharaMove = 100;
        //キャラクターの向きを整える
        CharaRotation();
        //フレーバーテキスト表示用の値
        FlavorFlug = -1;
        //フレーバーテキストリストを作成
        FlavorStrins = new string[CharaTotal];
        for (int i = 0;i< CharaTotal;i++)FlavorStrins[i] = (Resources.Load("JobStr"+i) as TextAsset).text;
        //画面をあける
        CharaShift = 4;
        //音楽を再生
        SoundManager.Instance.Volume = (float)PlayDataManager.VolumeList[0] / 100;
        SoundManager.Instance.PlayBgmByName("CharaSelectBGM","Main");
        SoundManager.Instance.PlayBgmByName("CharaSelectSub","Sub");
    }
    //操作関連のアップデート
    void Update()
    {
        switch (CharaShift)
        {
            case 1://テーブルを回転させる値を変更
                CharaTable();
                break;
        }
        //×入力時ステージセレクトへ戻る
        if (CharaShift != 4 && CharaShift != 5 && (Input.GetButtonDown("Mouse_StartButton") || Input.GetButtonDown("Pad_Jump"))) CharaShift = 6;

    }
    //表示関連のアップデート
    void FixedUpdate()
    {
        //もくじ
        switch (CharaShift)
        {           
            case 0://選択中のキャラクターに関する文字列のセット、ライトの点灯
                CharaStatus();
                //情報を取得したらテーブルの動作へ移行
                CharaShift = 1;
                break;          
            case 1:
                break;          
            case 2: //キャラクターの移動
                CharaPotion();
                break;            
            case 3://キャラクターの回転
                CharaRotation();
                break;              
            case 4: //シーン移動してきたときの動作
                SceneOpen();
                break;               
            case 5://シーン移動するときの動作
                NextScene();
                break;            
            case 6://前のシーンへ戻る
                PrevScene();
                break;
        }
    }
    //選択中のキャラクターの設定
    void CharaStatus()
    {
        //ステータスの値を文字列に変換
        for (int i = 0; i < 7; i++) StatusString += "\n" + PlayDataManager.CHARA_STATUS[CharaTbl % CharaTotal, i];
        //変換した文字列を表示する
        StatusText.GetComponent<Text>().text = StatusString;
        //テキストと画像を表示
        StartText.SetActive(true);
        //解放済みのキャラクターの場合
        if (PlayDataManager.CharaLock[CharaTbl % CharaTotal] == 1)
        {
            //テキストのフォントを変更
            StartText.GetComponentInChildren<Text>().font = Resources.Load<Font>("Fonts/JazzCreateBubble");
            //フォントのサイズを変更
            StartText.GetComponentInChildren<Text>().fontSize = 150;
            //テキストをGameStartに変更
            StartText.GetComponentInChildren<Text>().text = "GameStart";
            //職名を表示
            JobText.GetComponent<Text>().text = "~" + PlayDataManager.CHARA_NAME[CharaTbl % CharaTotal]+"~";
            //値段表示なし
            PriceText.GetComponent<Text>().text = "";
            //ライトを点灯する
            SpotLight.SetActive(true);
            //フレーバーテキストを表示できる用意
            FlavorHint.SetActive(true);
            //フレーバーテキストの表示関連
            FlavorText();
        }
        //未解放のキャラを選択していた場合
        if(PlayDataManager.CharaLock[CharaTbl%CharaTotal]==0)
        {
            //PrincessPointUI.PPsub = PlayDataManager.CHARA_PRICE[CharaTbl % CharaTotal];
            //テキストのフォントを変更
            StartText.GetComponentInChildren<Text>().font = Resources.Load<Font>("Fonts/gomarice_mukasi_mukasi");
            //フォントのサイズを変更
            StartText.GetComponentInChildren<Text>().fontSize = 100;
            /*
            //所持PPが解放に必要なPPに満たない場合
            if (PlayDataManager.PrincesPoint < PlayDataManager.CHARA_PRICE[CharaTbl % CharaTotal])
            {
                //テキストを未解放に変更
                StartText.GetComponentInChildren<Text>().text = "未解放";
                //値段表示を赤くする
                PriceText.GetComponent<Text>().color = Color.red;
            }//解放に必要なPPを所持している場合
            if (PlayDataManager.PrincesPoint >= PlayDataManager.CHARA_PRICE[CharaTbl % CharaTotal])
            {
                //テキストを解放可能に変更
                StartText.GetComponentInChildren<Text>().text = "解放可能";
                //値段表示を緑にする
                PriceText.GetComponent<Text>().color = Color.green;
            }
            //解放に必要な値段を表示
            PriceText.GetComponent<Text>().text = "" + PlayDataManager.CHARA_PRICE[CharaTbl % CharaTotal];
            */
            //ライトを消灯する
            SpotLight.SetActive(false);
            //フレーバーテキストを開くヒントを非表示
            FlavorHint.SetActive(false);
        }
    }
    //フレーバーテキストの表示
    void FlavorText()
    {

        if (FlavorFlug == 1)
        {
            SoundManager.Instance.PlaySeByName("PaperOpen", gameObject);
            FlavorHint.GetComponentInChildren<Text>().text = "詳細情報を閉じる";
            if (PlayDataManager.CharaLock[CharaTbl % CharaTotal] == 1)
            {
                FlavorCanvas.SetActive(true);
                FlavorCanvas.GetComponent<Text>().text = FlavorStrins[CharaTbl % CharaTotal];
            }
        }
        if (FlavorFlug == -1)
        {
            if(CharaShift != 0)SoundManager.Instance.PlaySeByName("PaperClose", gameObject);
            FlavorCanvas.SetActive(false);
            FlavorHint.GetComponentInChildren<Text>().text = "詳細情報を開く";
        }
    }
    //テーブルの動作
    void CharaTable()
    {
        //エンターを押すことで選択したステージへ移動           
        if ((Input.GetKeyDown(KeyCode.KeypadEnter) ||Input.GetButtonDown("Mouse_Fire1") || Input.GetButtonDown("Pad_Fire1")) && PlayDataManager.CharaLock[CharaTbl%CharaTotal] == 1)
        {
            SoundManager.Instance.PlaySeByName("StageSelectOK",gameObject);
            CharaShift = 5;
        }

　　　 //解放されていない場合はエンター入力でショップへ移動
        if(PlayDataManager.CharaLock[CharaTbl%CharaTotal] != 1)Store();

        //Xを押したときフレーバーテキストの表示非表示を切り替え
        if (PlayDataManager.CharaLock[CharaTbl % CharaTotal] == 1 && (Input.GetButtonDown("Mouse_Fire2") || Input.GetButtonDown("Pad_Fire2")))
            {
            FlavorFlug *=-1;
            FlavorText();
        }

            //キャラテーブルの値のログを録っておく
            CharaTblLog = CharaTbl;
        // 左右に入力でテーブルを回す
        if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetAxisRaw("Horizontal") < 0 || Input.GetAxisRaw("Pad_D_H") < 0 || Input.GetAxis("Mouse_ScrollWheel") > 0) CharaTbl-= 1;
        if (Input.GetKeyDown(KeyCode.RightArrow) || 0 < Input.GetAxisRaw("Horizontal") || Input.GetAxisRaw("Pad_D_H") > 0 || Input.GetAxis("Mouse_ScrollWheel") < 0) CharaTbl+= 1;
        //テーブルの値が0より小さくならないようにする
        if (CharaTbl < 0)
        {
            CharaTbl = CharaTotal-1;
            //バグ防止のためログの値も変更
            CharaTblLog = CharaTotal;
        }
        //キャラテーブルの値が変わっていた場合
        if (CharaTbl != CharaTblLog)
        {
            //効果音を鳴らす
            SoundManager.Instance.PlaySeByName("CharaSelect",gameObject);
            //ステータス情報をリセット
            StatusString = "";
            //スポットライトを消灯する
            SpotLight.SetActive(false);
            //テキストの表示を消す
            StatusText.GetComponent<Text>().text = "";
            //StartText.SetActive(false);
            PriceText.GetComponent<Text>().text = "";
            //PrincessPointUI.PPsub = 0;
            JobText.GetComponent<Text>().text = "";  
            FlavorCanvas.SetActive(false);
            FlavorHint.SetActive(false);
            //キャラクターの移動に使用する値をセット
            CharaMove = MoveSecond;
            //キャラの移動へシフト
            CharaShift = 2;
        }
    }
    //ショップの動作
    void Store()
    {
       /*
        //解放に必要なPPを所持している場合
        if (PlayDataManager.PrincesPoint >= PlayDataManager.CHARA_PRICE[CharaTbl % CharaTotal])
        {
           
            //PPを消費してキャラクターを解放
            if (Input.GetKeyDown(KeyCode.KeypadEnter) ||Input.GetButtonDown("Mouse_Fire1") || Input.GetButtonDown("Pad_Fire1"))
            {
                //効果音を鳴らす
                SoundManager.Instance.PlaySeByName("CharaShopOK",gameObject);         
                //キャラクターのロックを解除
                PlayDataManager.CharaLock[CharaTbl % CharaTotal] = 1;
                //キャラクターのステータス情報をリセット
                StatusString = "";
                //キャラクターのデータを取得
               CharaShift = 0;
            }
        }
        //解放に必要なPPを所持していない場合
        if (PlayDataManager.PrincesPoint < PlayDataManager.CHARA_PRICE[CharaTbl % CharaTotal])
        {
            if (Input.GetKeyDown(KeyCode.KeypadEnter) ||Input.GetButtonDown("Mouse_Fire1") || Input.GetButtonDown("Pad_Fire1"))
                //効果音を鳴らす
                SoundManager.Instance.PlaySeByName("CharaShopNG", gameObject);
        }
        */
    }
   
    //キャラクターの配置関連
    void CharaPotion()
    {
        int PosiNega = 1;  
        //キャラクターの位置をテーブルによって移動させる  
        if (CharaTbl - CharaTblLog < 0)PosiNega = 1;
        //キャラクターの位置をテーブルによって移動させる(逆回転)
        if (CharaTbl - CharaTblLog > 0) PosiNega = -1;

        if (CharaMove-- < 0) CharaMove = 0;
        //移動を行う。
        for (int i = 0; i < CharaTotal; i++) CharaList[i].transform.position = new Vector3
            (6 * Mathf.Cos(2 * Mathf.PI * (i - CharaTblLog + (float)CharaTotal * 5 / 4 + PosiNega * ((MoveSecond - CharaMove) / MoveSecond)) / CharaTotal),//X
            0,//Y
            6 * Mathf.Sin(2 * Mathf.PI * (i - CharaTblLog + (float)CharaTotal * 5 / 4 + PosiNega * ((MoveSecond - CharaMove) / MoveSecond)) / CharaTotal));//Z
        //プレイヤーの移動後と移動前の座標から角度を算出
        float Angle = Mathf.Atan2(CharaList[0].transform.position.z, CharaList[0].transform.position.x) * Mathf.Rad2Deg;

        for (int i = 0; i < CharaTotal; i++) CharaList[i].transform.rotation = Quaternion.Euler(0, -Angle + 90 - (360 / CharaTotal) * i, 0);
        //移動が完了していたらキャラクターの向きを調整する
        if (CharaMove == 0) CharaShift = 0;
    }
    //キャラクターの向き関連
    void CharaRotation()
    {
        //テーブルの位置によってキャラクターの向きを変更
        for (int i = 0; i < CharaTotal; i++) CharaList[i].transform.rotation = Quaternion.Euler(0, (CharaTbl - i) * (360 / CharaTotal), 0);
        //選択中のキャラクターのステータス取得へ移動
        CharaShift = 0;
    }
    //キャラクターセレクトにやってきたときの動作
    void SceneOpen()
    {
        //スクリーンを開ける
        if(SceneOpenFlag == false)if(SceneMove.Open())SceneOpenFlag = true;
        //キャラクターテーブルをしばらく回転させる
        if (CharaMove-- <= 0) CharaMove = 0;
        //各キャラを円状に配置
        for (int i = 0; i < CharaTotal; i++) CharaList[i].transform.position = new Vector3
                (6 * Mathf.Cos(2 * Mathf.PI * (i - CharaTbl + (float)CharaTotal * 5 / 4 + CharaMove * CharaMove / 1800) / CharaTotal),
                0,
                6 * Mathf.Sin(2 * Mathf.PI * (i - CharaTbl + (float)CharaTotal * 5 / 4 + CharaMove * CharaMove / 1800) / CharaTotal));
        //プレイヤーの移動後と移動前の座標から角度を算出
        float Angle = Mathf.Atan2(CharaList[0].transform.position.z, CharaList[0].transform.position.x) * Mathf.Rad2Deg;
        //徐々に暗くする
        BigLight.GetComponent<Light>().intensity = CharaMove/200.0f;

        for (int i = 0; i < CharaTotal; i++) CharaList[i].transform.rotation = Quaternion.Euler(0, -Angle + 90 - (360 / CharaTotal) * i, 0);
        //全ての演出が終わったらキャラクターを選べるようにする
        if (CharaMove == 0 && SceneOpenFlag == true)CharaShift = 0;
    }
    //ステージに入るときの動作
    void NextScene()
    {   
        if (SceneMove.Close())
        {
            //キャラクターテーブルで選択したキャラをプレイするキャラクターにセット
            PlayDataManager.CharaNum = CharaTbl % CharaTotal;
            //データを保存する
            PlayDataManager.DataSave();
            //ステージセレクトで選択したステージへ移動
            SceneManager.LoadScene("Stage" + PlayDataManager.StageNum);
        }
    }
    //ステージセレクトに戻るときの動作
    void PrevScene()
    {
        if(SceneMove.Back())
        {
            SceneManager.LoadScene("StageSelect");
        }
    }
}
