﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// チェストを扱うcs
public class ChestManager : MonoBehaviour
{
    private new Animation animation;// アニメーション用

    private GameObject DropItemBox; // ドロップアイテムの親の空オブジェクト
    public GameObject dropItem; // ドロップアイテム
    public GameObject Player;   // プレイヤー
    
    Attack attack; // Attack.cs

    private int Count; // チェストが開いて消えるときに使う
    public float YRota; // 開く時にY軸のRotationが0に戻るのでそれを阻止するための変数

    bool OpenFlag; // 開いたかどうかのbool
    public Vector3 Pos; // Unity上で設置した場所に固定するため

    void Start()
    {
        Player = GameObject.Find("Player");// Playerを探す
        attack = Player.GetComponent<Attack>();// アタックの取得
        animation = GetComponent<Animation>(); // アニメーションの取得
        Count = 0; // カウントを0にしておく
        OpenFlag = false; // 初期化
        Pos = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z); // 場所を固定する
    }
    void Update()
    {
        transform.position = Pos; // 場所は変えず
        transform.localScale = new Vector3(3, 3, 3); // 大きさは3倍(ちょうどいい大きさに)
        transform.rotation = Quaternion.Euler(0.0f, YRota, 0.0f); // Y軸だけは固定回転
    }

    void FixedUpdate()
    {
        //もし、Playerがいなかったら探す
        if (Player == null)Player = GameObject.Find("Player" + (PlayDataManager.CharaNum));

        if (OpenFlag) Open();// チェストが開いていたらあけっぱなしにする
    }

    //攻撃に触れたらチェストを開ける
    public void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.CompareTag("NormalWeapon") && attack.NormalCoolTime != 0 || collision.gameObject.CompareTag("DollNormalWeapon"))
        {
            Open();
            OpenFlag = true;
        }
        if (collision.gameObject.CompareTag("SkillWeapon") && attack.SPCoolTime != 0 || collision.gameObject.CompareTag("DollSkillWeapon"))
        {
            Open();
            OpenFlag = true;
        }
    }
    public void Open()
    {
        if(Count == 20)
        {
            ClearManager.TreasureBonus += 500;
            // アイテム出現
            DropItemBox = GameObject.Find("DropItemBox"); // 親オブジェクト用に取得する

            GameObject CloneDropItem = Instantiate(dropItem, // 出現させるプレハブ名
                new Vector3(this.transform.position.x, this.transform.position.y + 1.0f, this.transform.position.z), // 少し高い位置にしたいので
                this.transform.rotation); // 角度は回転するので適当

            CloneDropItem.name = "DropItem"; // クローンしたオブジェクトの名前を変更
            CloneDropItem.transform.SetParent(DropItemBox.transform); // DropItemBoxを親に指定
        }
        if(Count < 50)animation.Play("OPEN"); // 開けるアニメーションを50フレーム分
        else animation.Play("OPEN_IDLE"); // そのあとは開けっ放しにする
        Count++;

        Destroy(this.gameObject,5f);// アニメーションを再生してから消えるように5秒に設定
    }
}
