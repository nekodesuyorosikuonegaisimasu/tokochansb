﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SeedUI : MonoBehaviour
{
    // 通常攻撃UI
    [SerializeField]
    private GameObject NormalAttackUI;
    // 種を持つUI
    [SerializeField]
    private GameObject CatchSeedUI;
    // 種を植えるUI
    [SerializeField]
    private GameObject PlantSeedUI;
    // スキル攻撃UI
    [SerializeField]
    private GameObject SkillAttackUI;
    // 種を手放すUI
    [SerializeField]
    private GameObject ThrowSeedUI;

    // プレイヤー
    private GameObject Player;

    // Start is called before the first frame update
    void Start()
    {
        // ループオブジェクト（種）関連のUIを非表示にする
        CatchSeedUI.SetActive(false);
        PlantSeedUI.SetActive(false);
        ThrowSeedUI.SetActive(false);
        // プレイヤーを探す
        Player = GameObject.Find("Player");
    }

    // Update is called once per frame
    void Update()
    {
        // プレイヤーがいないなら探す処理へ
        if(Player == null) Player = GameObject.Find("Player");

        // 種に触れているかつ種を持っていないなら
        if(Player.GetComponent<Player>().IsTouchSeed && !Player.GetComponent<Attack>().IsHoldingSeed)
        {
            // 持つUIとスキル攻撃UIのみ表示
            NormalAttackUI.SetActive(false);
            CatchSeedUI.SetActive(true);
            PlantSeedUI.SetActive(false);
            SkillAttackUI.SetActive(true);
            ThrowSeedUI.SetActive(false);
        }
        // 種所持中なら
        else if(Player.GetComponent<Attack>().IsHoldingSeed)
        {
            // 植えるUIと手放すUIのみ表示
            NormalAttackUI.SetActive(false);
            CatchSeedUI.SetActive(false);
            PlantSeedUI.SetActive(true);
            SkillAttackUI.SetActive(false);
            ThrowSeedUI.SetActive(true);

            // 植えられる場所なら植えるUIを明るく、植えられない場所なら植えるUIを暗くする
            if(Player.GetComponent<Player>().IsStickGround) PlantSeedUI.GetComponent<Image>().color = new Color(1, 1, 1, 1);
            else PlantSeedUI.GetComponent<Image>().color = new Color(39.0f / 255.0f, 39.0f / 255.0f, 39.0f / 255.0f, 0.5f);
        }
        // 種に触れていないなら
        else
        {
            // 通常攻撃UIとスキル攻撃UIのみ表示
            NormalAttackUI.SetActive(true);
            CatchSeedUI.SetActive(false);
            PlantSeedUI.SetActive(false);
            SkillAttackUI.SetActive(true);
            ThrowSeedUI.SetActive(false);
        }
    }
}
