﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player6 : MonoBehaviour
{
    /*
    // プレイヤーを探す処理（プレイヤー6を探す処理）
    private GameObject player6;
    // 召喚物を探す処理
    private GameObject summonPlayer;
    // アニメーション
    private Animator animator;
    // Rigidbody
    private Rigidbody m_rigidbody;
    // 攻撃したかの情報
    private Attack6 attack;
    // 移動
    private Vector3 input;
    // 移動に必要な力
    private float moveForceMultiplier;
    // 接地判定
    private bool ground;
    // ジャンプ判定
    private bool jumpFlag;
    // 足を止める攻撃
    public bool stopAttackFlag;
    // 召喚中か否か（召喚物消滅時にオンになる）
    public bool endFlag;
    // 召喚物の番号
    public int Number;
    // 敵
    GameObject enemy;
    // 体力のキャンバス
    private GameObject GetHpCanvas;
    [SerializeField]
    private GameObject HpCanvas;
    

    // ステータス情報
    // 自分で変えることはないので最後に全部privateにする予定
    public float charaSpeed;
    private float jumpPower;
    public float NattackPower;
    public float Hp;

    // Start is called before the first frame update
    void Start()
    {
        // プレイヤーの取得
        player6 = GameObject.Find("Player" + PlayDataManager.CharaNum);
        // 召喚物の取得
        summonPlayer = GameObject.Find("Player6-" + Number);
        // アニメーションの取得
        animator = this.GetComponent<Animator>();
        // RigidBodyの取得
        m_rigidbody = this.GetComponent<Rigidbody>();
        // Attack6の情報を持つ
        attack = summonPlayer.GetComponent<Attack6>();
        // 初期化
        jumpFlag = false;
        stopAttackFlag = false;
        endFlag = false;
        moveForceMultiplier = 10.0f;

        // 召喚したキャラクターに応じてステータスを変更
        NattackPower = PlayDataManager.SUMMON_STATUS[Number - 1, 0];
        Hp = PlayDataManager.SUMMON_STATUS[Number - 1, 2];
        charaSpeed = PlayDataManager.SUMMON_STATUS[Number - 1, 3];
        jumpPower = PlayDataManager.SUMMON_STATUS[Number - 1, 4]*10;

        // HPのCanvas配置
        GetHpCanvas = Instantiate(HpCanvas, new Vector3(0, 0, 0), transform.rotation);
        // 召喚時効果音の再生
        SoundManager.Instance.PlaySeByName("Summon Appear" + Number, this.gameObject);
        Summon();
    }

    // Update is called once per frame
    void Update()
    {
        // ポーズ中は操作を受け付けない
        if (Mathf.Approximately(Time.timeScale, 0f)) return;
        if (!stopAttackFlag)
        {
            // 左スティックで動くようにする
            input.x = Input.GetAxisRaw("Horizontal");
            input.y = 0;
            input.z = Input.GetAxisRaw("Vertical");
            // ジャンプをしていないかつ接地しているなら
            if (!jumpFlag && ground)
            {
                if(ControllerChecker.CheckController) jumpFlag = Input.GetButtonDown("Pad_Jump");
                else jumpFlag = Input.GetButtonDown("Mouse_Jump");
            } 
        }
        // 体力が0なら消滅フラグをオンにする
        if(Hp <= 0) endFlag = true;
        // 召喚処理へ
        Summon();
    }

    void FixedUpdate()
    {
        // 移動処理
        Vector3 forward = Vector3.Scale(UnityEngine.Camera.main.transform.forward, new Vector3(1, 0, 1)).normalized;
        Vector3 direction = forward * input.z + UnityEngine.Camera.main.transform.right * input.x;  //  テンキーや3Dスティックの入力（GetAxis）があるとdirectionに値を返す
        Vector3 move = direction * charaSpeed;
        Vector3 vel = new Vector3(m_rigidbody.velocity.x, 0, m_rigidbody.velocity.z);

        // 足を止める攻撃中でないなら
        if (!stopAttackFlag)
        {
            // 接地しているなら
            if (ground)
            {
                // キャラクターに力を加える
                Vector3 mult = moveForceMultiplier * (move - vel);
                if (mult.magnitude > 0.001f)
                {
                    m_rigidbody.AddForce(moveForceMultiplier * (move - vel));
                }

                // 移動した場合方向転換（ベクトルの大きさが少しでも変わったなら）
                if (direction.magnitude > 0.01f)
                {
                    // directoinのxとz方向にキャラクターを回転させる
                    transform.rotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
                    // 走るアニメーションの再生
                    animator.SetBool("Running", true);
                }
                // 移動していない場合
                else if (input == Vector3.zero)
                {
                    animator.SetBool("Running", false);
                }
                // ジャンプ処理
                if (jumpFlag)
                {
                    m_rigidbody.AddForce(transform.up * jumpPower);
                    jumpFlag = false;
                }
            }
            else
            {
                // キャラクターに力を加える
                m_rigidbody.AddForce(move - vel);

                // 移動した場合方向転換（ベクトルの大きさが少しでも変わったなら）
                if (direction.magnitude > 0.01f)
                {
                    // directoinのxとz方向にキャラクターを回転させる
                    transform.rotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
                    // 走るアニメーションの再生
                    animator.SetBool("Running", true);
                }
                // 移動していない場合
                else if (input == Vector3.zero)
                {
                    animator.SetBool("Running", false);
                }
            }
        }
        else
        {
            m_rigidbody.constraints = RigidbodyConstraints.FreezePositionX;
            m_rigidbody.constraints = RigidbodyConstraints.FreezePositionZ;
            if(attack.NormalCoolTime == 0.0f)
            {
                stopAttackFlag = false;
                m_rigidbody.constraints = RigidbodyConstraints.FreezeRotation;
            }
        }
    }

    void OnCollisionStay(Collision collision)
    {
        // 地面との当たり判定
        if (collision.gameObject.CompareTag("Ground"))
        {
            ground = true;
            // ジャンプのアニメーションをオフにする
            animator.SetBool("Jumping", false);
        }
    }
    // 接地していないなら（ジャンプ中なら）
    void OnCollisionExit(Collision collision)
    {
        ground = false;
        // ジャンプのアニメーションをオフにする
        animator.SetBool("Jumping", true);
    }
    // 敵との当たり判定
    void OnTriggerEnter(Collider other)
    {
        // 敵との当たり判定
        if (other.gameObject.CompareTag("Enemy") && attack.NormalCoolTime == 0.0f)
        {
            // 敵を登録
            enemy = other.transform.root.gameObject; 
            //敵の名前を照合してhpを減らす
            for (int i = 0; i < PlayDataManager.ENEMY_NAME.Length; i++)
            {
                if (enemy.name == PlayDataManager.ENEMY_NAME[i])
                {
                    Hp -= PlayDataManager.ENEMY_STATUS[i, 1];
                }
            }
        }

        if(other.gameObject.CompareTag("Enemy") && attack.NormalCoolTime != 0.0f && PlayDataManager.SUMMON_STATUS[Number - 1, 5] == 0)
        {
            // 効果音再生
            SoundManager.Instance.PlaySeByName(attack.seName[Number - 1], this.gameObject);
        }
    }

    void Summon()
    {
        if(!endFlag)
        {
            player6.GetComponent<Player>().enabled = false;
            player6.GetComponent<Attack>().enabled = false;
        }
        else
        {
            // 召喚中にオフにしたものをオンにする
            player6.GetComponent<Player>().enabled = true;
            player6.GetComponent<Attack>().enabled = true;
            // プレイヤー6の通常攻撃クールタイムを0にする
            player6.GetComponent<Attack>().NormalCoolTime = 0;
            // 破壊時に通常攻撃のクールタイムがあるなら破壊する
            Destroy(attack.instance);
            // 召喚中フラグをオフにする
            //player6.GetComponent<Attack>().summonFlag = false;
            // オブジェクトの破壊
            Destroy(this.gameObject);
        }
    }
    */
}
