﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeteoriteOfEarth : MonoBehaviour
{
    //ゲームオブジェクト
    GameObject EarthObj; //地球

    //値
    float MeteoriteSpeed; //隕石の落ちる速度
    Vector3 direction; //距離

    // Start is called before the first frame update
    void Start()
    {
        MeteoriteSpeed = 6.6f; //速度
        EarthObj = GameObject.Find("Earth"); //壊れる地球を検索
    }
    
    void FixedUpdate()
    {
        //対象あり
        if (EarthObj != null)
        { 
            // 地球に向かう向きの取得
            direction = EarthObj.transform.position - transform.position;

            // 加速度与える
            GetComponent<Rigidbody>().AddForce(MeteoriteSpeed * direction.normalized * (EarthObj.GetComponent<Rigidbody>().mass * GetComponent<Rigidbody>().mass) / (direction.sqrMagnitude));
        }


    }
    //何かに当たった
    void OnCollisionEnter(Collision collision)
    {
        //当たったのが地球だった
        if(collision.transform.name == "Earth")
        {
            Destroy(this.gameObject); //オブジェクトを消す
        }
    }
}
