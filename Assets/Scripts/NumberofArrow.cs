﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NumberofArrow : MonoBehaviour
{
    /*
    // 数えるもののテキスト格納用オブジェクト
    public GameObject numItem;
    // フレーム格納用オブジェクト
    public GameObject frame;
    // プレイヤー
    private GameObject player;
    // 召喚物
    private GameObject summonPlayer;
    // 弓矢の数
    private int arrow;
    // パワー
    private int levelCount;
    // 召喚物の体力
    private float Hp;
    // 召喚物の番号
    private int summonNumber;
    // 召喚物判定
    private bool judgeSummonFlag;

    // プレイヤー5の弓矢のイメージと「×」
    [SerializeField]
    private GameObject arrowImage;
    // プレイヤー6のパワーのイメージと「×」
    [SerializeField]
    private GameObject starImage;
    // 召喚物の体力のイメージと「×」
    [SerializeField]
    private GameObject HpImage;

    // Start is called before the first frame update
    void Start()
    {
        // プレイヤーを探す
        player = GameObject.Find("Player" + PlayDataManager.CharaNum);
        // プレイヤーが「召喚士」なら召喚物の番号をとる
        if(PlayDataManager.CharaNum == 6)summonNumber = player.GetComponent<Attack>().player6Number;
        // 召喚物のHPキャンバスの生成なら
        if(player.GetComponent<Attack>().summonFlag)
        {
            judgeSummonFlag = true;
            SetTransform();
        }
        else judgeSummonFlag = false;
    }
    // Update is called once per frame
    void Update()
    {
        // プレイヤー「弓使い」の場合
        if(PlayDataManager.CharaNum == 5)
        {
            // 数値をみる
            arrow = player.GetComponent<Attack>().arrowCount;
            // 数値を文字にする
            numItem.GetComponent<Text>().text = arrow.ToString();
            // 弓矢の数のイメージ表示
            arrowImage.SetActive(true);
            starImage.SetActive(false);
            HpImage.SetActive(false);
        }
        // プレイヤー「召喚士」が召喚中の場合
        else if(judgeSummonFlag)
        {
            summonPlayer = GameObject.Find("Player6-" + summonNumber);
            // 数値をみる
            Hp = summonPlayer.GetComponent<Player6>().Hp;
            // 数値を文字にする
            numItem.GetComponent<Text>().text = Hp.ToString();
            // 体力がなくなったらフラグを降ろす
            if(Hp <= 0 || summonPlayer.GetComponent<Player6>().endFlag)
            {
                //judgeSummonFlag = false;
                Destroy(this.gameObject);
            }
            // 召喚物の体力のイメージ表示
            arrowImage.SetActive(false);
            starImage.SetActive(true);
            HpImage.SetActive(true);
        }
        // プレイヤー「召喚士」が召喚していない場合
        else if(PlayDataManager.CharaNum == 6 && !player.GetComponent<Attack>().summonFlag)
        {
            // 数値をみる
            levelCount = player.GetComponent<Attack>().levelCount;
            // 数値を文字にする
            numItem.GetComponent<Text>().text = levelCount.ToString();
            // 18(最大値)以上の場合
            if(levelCount >= 18) numItem.GetComponent<Text>().text = "MAX";
            // パワーのイメージ表示
            arrowImage.SetActive(false);
            starImage.SetActive(true);
            HpImage.SetActive(false);
        }
    }

    private void SetTransform()
    {
        numItem.GetComponent<RectTransform>().anchoredPosition = new Vector3(175, 20, 0);
        frame.GetComponent<RectTransform>().anchoredPosition = new Vector3(150, 20, 0);
    }
    */
}
