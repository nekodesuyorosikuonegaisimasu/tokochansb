﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// ボスの攻撃を扱うcs
public class BossAttack : MonoBehaviour
{
    public bool Type; // ボスの攻撃タイプ(炎か落石の2種類なのでbool)
    public float Length; // 射程(攻撃が消えるまでの距離)
    public float DesCount; // クリアカウント
    private float Count; // 秒数をカウント
    public int RandomCount; // 落石の落ちる順番

    void Start()
    {
        if(this.gameObject.name == "BossFire" + "(Clone)")Type = true; // 出された攻撃がどちらかを判別する 
        else Type = false;

        //射程を設定する
        Length = 1000f;
        if(!Type)Length += 2000f;

        DesCount = 0.0f;

        if(!Type)RandomCount = Random.Range(0, 15); // 落石用にランダムな数値をもらう
        if(this.gameObject.transform.localScale.x == 5)RandomCount = 0; // サイズが5倍のものは数値を0(直ぐに落ちてくる)にする。

    }
    
    void Update()
    {
        if(Type) // Boss Fire
        {
            // 打ち出した前方にとんでいく
            GetComponent<Rigidbody>().velocity = transform.forward * 1000 * Time.fixedDeltaTime;
        }
        else // Store Fall
        {
            if(Count > RandomCount) // 毎秒ごとに落石が始まる
            {
                // 打ち出した前方にとんでいく
                GetComponent<Rigidbody>().velocity = -transform.up * 2000 * Time.fixedDeltaTime;
            } 
        }
        if (DesCount++ > Length) // DesCountが射程の数値を超えたら消す
        {
            // 消す
            Destroy(this.gameObject);
        }
        Count += Time.deltaTime; // 秒数をカウント
    }
}
