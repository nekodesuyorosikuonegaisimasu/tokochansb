﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackFire : MonoBehaviour
{
    GameObject player;
    // 飛んでいく速さ
    public float speed;
    // 球が消えるかどうか
    private bool clearFlag;
    // 攻撃が通常攻撃かどうか
    public bool normalAttackFlag;
    // クリアカウント
    public float clearCount;
    // 射程（攻撃が消えるまでの距離）
    public float NLength;
    public float SLength;
    // 攻撃力
    public float Power;
    // プレイヤー「弓使い」
    public GameObject dropArrow;
    public GameObject shotArrow;
    private Vector3 arrow;
    private bool onceFlag;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player");// + (PlayerChangeManager.ChangeCount)
        // 飛び道具を消す処理
        clearCount = 0.0f;
        // 射程
        NLength = PlayDataManager.CHARA_STATUS[PlayDataManager.CharaNum, 1];
        SLength = PlayDataManager.CHARA_STATUS[PlayDataManager.CharaNum, 3];
        onceFlag = false;
    }

    // Update is called once per frame
    void Update()
    {
        // ポーズ中は操作を受け付けない
        if (Mathf.Approximately(Time.timeScale, 0f)) return;

        if(clearFlag && dropArrow)
        {
            Instantiate(dropArrow, new Vector3(this.transform.position.x, this.transform.position.y + 1.0f, this.transform.position.z),this.transform.rotation);
        }

        // 通常攻撃を消す処理
        if (clearCount++ > 500 * NLength || clearFlag) Destroy(this.gameObject);
        // 必殺技を消す処理
        if(clearCount > 500 * SLength) Destroy(this.gameObject);

        if(player == null) Destroy(this.gameObject);
        else Power = Power * player.GetComponent<Attack>().Item;
    }

    void FixedUpdate()
    {
        // 「弓使い」のアローレインの降る速さの決定
        if(PlayDataManager.CharaNum == 5) RandomSpeed();

        // 打ち出した前方にとんでいく
        this.GetComponent<Rigidbody>().velocity = transform.forward * speed * Time.fixedDeltaTime;

        // 「弓使い」の矢の方向を決める処理
        if(shotArrow)
        {
            if(normalAttackFlag && !onceFlag)
            {
                arrow = player.transform.eulerAngles;
                arrow.x = player.transform.eulerAngles.x;
                arrow.y = player.transform.eulerAngles.y + 270.0f;
                arrow.z = player.transform.eulerAngles.z;
                shotArrow.transform.eulerAngles = arrow;
                onceFlag = true;
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        // 通常攻撃が敵に当たった場合
        if(normalAttackFlag && other.gameObject.CompareTag("Enemy"))
        {
            clearFlag = true;
        }
    }

    void RandomSpeed()
    {
        speed = Random.Range(500, 3500);
    }
}
