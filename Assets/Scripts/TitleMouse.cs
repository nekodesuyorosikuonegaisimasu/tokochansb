﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TitleMouse : MonoBehaviour
{
    [SerializeField]
    private int selectNum;
    private GameObject ManagerName;
    private string thisSceneName;

    // Start is called before the first frame update
    void Start()
    {
        if(SceneManager.GetActiveScene().name.ToString().Contains("Stage") && SceneManager.GetActiveScene().name != "StageSelect")
        {
            ManagerName = GameObject.Find("PauseManager");
        }
        else
        { 
            ManagerName = GameObject.Find(SceneManager.GetActiveScene().name +"Manager");
        }
        if(SceneManager.sceneCount == 2)
        {
            ManagerName = GameObject.Find("OptionManager");
        }
        thisSceneName = SceneManager.GetActiveScene().name;
    }

    private void OnMouseOver()
    {
        // マウス操作かつ開いてるシーンが一つだけのとき
        if (!ControllerChecker.CheckController && SceneManager.sceneCount == 1)
        {
            // タイトルなら
            if (thisSceneName == "Title")
            {
                if (ManagerName.GetComponent<TitleManager>().SelectNum != selectNum && ManagerName.GetComponent<TitleManager>().Shift == 1)
                {
                    // TitleManager.csのSelectNumの数値を変える
                    ManagerName.GetComponent<TitleManager>().SelectNum = selectNum;
                    ManagerName.GetComponent<TitleManager>().Shift = 0;
                }
            }

            // ステージセレクトなら
            if (thisSceneName == "StageSelect")
            {
                // ワールドセレクト処理
                if (Input.GetButtonDown("Mouse_Fire1") && ManagerName.GetComponent<StageSelectManager>().Shift == 1 && !ManagerName.GetComponent<StageSelectManager>().IsSelectedWorld)
                {
                    // TitleManager.csのSelectNumの数値を変える
                    ManagerName.GetComponent<StageSelectManager>().WorldSelectNum = selectNum;
                    ManagerName.GetComponent<StageSelectManager>().StageSelectNum = 1 + (ManagerName.GetComponent<StageSelectManager>().WorldSelectNum - 1) * 3;
                }
                // ステージセレクト処理
                else if(ManagerName.GetComponent<StageSelectManager>().Shift == 1 && ManagerName.GetComponent<StageSelectManager>().IsSelectedWorld && selectNum + ((ManagerName.GetComponent<StageSelectManager>().WorldSelectNum - 1) * 3) <= PlayDataManager.OpenStage && ManagerName.GetComponent<StageSelectManager>().StageSelectNum != selectNum + ((ManagerName.GetComponent<StageSelectManager>().WorldSelectNum - 1) * 3))
                {
                    ManagerName.GetComponent<StageSelectManager>().StageSelectNum = selectNum + ((ManagerName.GetComponent<StageSelectManager>().WorldSelectNum - 1) * 3);
                    SoundManager.Instance.PlaySeByName("StageSelectKey", gameObject);
                    ManagerName.GetComponent<StageSelectManager>().PrincessIcon.transform.localPosition = new Vector3(-1352+(1200*(selectNum-1)), 466, 2474);
                    ManagerName.GetComponent<StageSelectManager>().Shift = 2;
                }
                // バックタイトル処理
                if (ManagerName.GetComponent<StageSelectManager>().BackSelect != selectNum && ManagerName.GetComponent<StageSelectManager>().Shift == 7)
                {
                    // TitleManager.csのSelectNumの数値を変える
                    ManagerName.GetComponent<StageSelectManager>().BackSelect = selectNum;
                    ManagerName.GetComponent<StageSelectManager>().Shift = 6;
                }
            }

            // ゲームオーバーなら
            if (thisSceneName == "GameOver")
            {
                if (ManagerName.GetComponent<GameOverManager>().SelectNum != selectNum && ManagerName.GetComponent<GameOverManager>().Shift == 1)
                {
                    // TitleManager.csのSelectNumの数値を変える
                    ManagerName.GetComponent<GameOverManager>().SelectNum = selectNum;
                    ManagerName.GetComponent<GameOverManager>().Shift = 0;
                }
            }

            // ポーズなら
            if (thisSceneName.Contains("Stage") && thisSceneName != "StageSelect")
            {
                if (ManagerName.GetComponent<Pause>().SelectNum != selectNum  && ManagerName.GetComponent<Pause>().Shift == 1 && !ManagerName.GetComponent<Pause>().operationFlag)
                {
                    ManagerName.GetComponent<Pause>().SelectNum = selectNum;
                    ManagerName.GetComponent<Pause>().Shift = 0;
                }
            }
        }

        /*//オプションなら
        if (SceneManager.sceneCount == 2 && ManagerName.name != "PauseManager")
        {
            if(ManagerName.name != "OptionManager") return;
            if (ManagerName.GetComponent<OptionManager>().SelectNum != selectNum && ManagerName.GetComponent<OptionManager>().Shift == 0)
            {
                ManagerName.GetComponent<OptionManager>().SelectNum = selectNum;
            }
        }*/
        
    }
}
