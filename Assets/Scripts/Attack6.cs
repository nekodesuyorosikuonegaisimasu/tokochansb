﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack6 : MonoBehaviour
{
    /*
    // 召喚物の種類
    const int SUMMON_PLAYER = 10;

    // アニメーター
    private Animator animator;
    //　Resourcesフォルダからテキストを読み込む
    private string loadStr;
    private int Count;
    // 効果音
    public string[] seName = new string[SUMMON_PLAYER];
    private string[] seStr;
    // プレイヤー
    private Player6 player;
    // クールタイム
    private GameObject coolTime;
    public GameObject instance;
    // 近接武器
    [SerializeField]
    private GameObject NormalWeapon;
    // 飛び道具を使うプレイヤー用
    [SerializeField]
    private GameObject Normalmagic;
    // クールタイム関係
    public float NCoolTime;
    public float NormalCoolTime = 0;
    // 召喚番号
    private int summonNumber;
    // 攻撃中にアニメーションを止める
    [SerializeField]
    private bool stopAttackAnimationFlag;
    // プレイヤー6-6用
    private Transform attackTransform;
    // プレイヤー6-10用
    private GameObject attackBall;


    // Start is called before the first frame update
    void Start()
    {
        // アニメーション
        animator = this.GetComponent<Animator>();
        // SEの登録
        LoadSE();
        for (int i = 0; i < SUMMON_PLAYER; i++)
        {
            // 通常攻撃効果音と必殺技効果音をキャラクターごとに登録していく
            seName[i] = seStr[Count];
            Count++;
        }
        // 攻撃のクールタイム
        NCoolTime = PlayDataManager.SUMMON_STATUS[this.gameObject.GetComponent<Player6>().Number - 1 , 1];
        // 召喚物のPlayer6スクリプト
        player = this.GetComponent<Player6>();
        // クールタイマーをResourcesファイルからロード
        coolTime = (GameObject)Resources.Load("CoolTimeUI");
        if (NormalWeapon) NormalWeapon.GetComponent<Collider>().enabled = false;
        // 召喚物の番号を記録
        summonNumber = this.gameObject.GetComponent<Player6>().Number;
    }

    // Update is called once per frame
    void Update()
    {
        // ポーズ中は操作を受け付けない
        if (Mathf.Approximately(Time.timeScale, 0f)) return;

        // 通常攻撃
        if ((Input.GetButtonDown("Mouse_Fire1") || Input.GetButtonDown("Pad_Fire1")) && NormalCoolTime == 0.0f && !player.stopAttackFlag)
        {
            // 攻撃時に効果音が鳴る召喚物の処理
            if(PlayDataManager.SUMMON_STATUS[this.gameObject.GetComponent<Player6>().Number - 1, 5] == 1)
            {
                SoundManager.Instance.PlaySeByName(seName[this.gameObject.GetComponent<Player6>().Number - 1], this.gameObject);
            }
            // 武器のコライダーをオンにする
            if (NormalWeapon) NormalWeapon.GetComponent<Collider>().enabled = true;
            // クールタイム処理
            NormalCoolTime = NCoolTime;
            // アニメーションの再生
            animator.SetBool("NormalAttack", true);
            // クールタイマー生成処理
            instance = (GameObject)Instantiate(coolTime, new Vector3(0, 0, 0), Quaternion.identity);
            instance.GetComponent<CoolTimeUI>().Count(NCoolTime, 150, -420);
            // 攻撃の種類
            StopAttack();
            // 飛び道具を使うプレイヤーの場合
            if (Normalmagic)
            {
                // 飛び道具の生成
                if(summonNumber == 5)
                {
                    StartCoroutine("SummonPlayer5");
                }
                else if(summonNumber == 6)
                {
                    attackTransform = this.transform;
                    StartCoroutine("SummonPlayer6");
                }
                else if(summonNumber == 7)
                {
                    StartCoroutine("SummonPlayer7");
                }
                else if (summonNumber == 10)
                {
                    attackBall = Instantiate(Normalmagic, transform.position + transform.forward * 5 + transform.up * 2, transform.rotation);
                    attackBall.GetComponent<Rigidbody>().AddForce(transform.forward * 40);
                }
                else
                {
                    Instantiate(Normalmagic, transform.position + transform.forward + transform.up, transform.rotation);
                }
            }
        }
        // クールタイム中の処理
        else
        {
            if (NormalCoolTime > 0.0f)
            {
                NormalCoolTime -= Time.deltaTime;
                animator.SetBool("NormalAttack", false);
            }
            else if (NormalCoolTime < 0.0f)
            {
                if (NormalWeapon) NormalWeapon.GetComponent<Collider>().enabled = false;
                animator.speed = 1.0f;
                NormalCoolTime = 0.0f;
            }

            // 攻撃時に動きを止めるプレイヤーの処理
            if(stopAttackAnimationFlag && 0.0f < NormalCoolTime && NormalCoolTime < 9.3f)
            {
                animator.speed = 0;
            }
        }
        // 消滅させる
        if ((Input.GetButtonDown("Mouse_Fire2") || Input.GetButtonDown("Pad_Fire2")))
        {
            player.endFlag = true;
        }
    }
    void StopAttack()
    {
        // 攻撃時にプレイヤーの動きを止める
        if(summonNumber == 5 || summonNumber == 6 || summonNumber == 10)
        {
            player.stopAttackFlag = true;
        }
    }
    // プレイヤー6-5の処理
    IEnumerator SummonPlayer5()
    {
        // アニメーション再生分止まる
        yield return new WaitForSeconds(0.2f);
        // 爆破
        Instantiate(Normalmagic, transform.position + transform.up * 2, transform.rotation);
        // アニメーション終了まで止まる
        yield return new WaitForSeconds(0.1f);
        // 召喚物を消滅させる
        player.endFlag = true;

        yield break;
    }
    // プレイヤー6-6の処理
    IEnumerator SummonPlayer6()
    {
        // アニメーション再生分止まる
        yield return new WaitForSeconds(0.8f);
        for (int i = 0; i < 50; i++)
        {
            Instantiate(Normalmagic, attackTransform.position - attackTransform.up + attackTransform.forward * 3 + (attackTransform.forward * 2) * i, attackTransform.rotation * Quaternion.Euler(-90, 0, 0));
            yield return new WaitForSeconds(0.01f);
        }
        yield break;
    }
    // プレイヤー6-7の処理
    IEnumerator SummonPlayer7()
    {
        // アニメーション再生分止まる
        yield return new WaitForSeconds(1.0f);
        // 岩生成
        Instantiate(Normalmagic, transform.position + transform.up * 2 + transform.forward * 4 + transform.right * 2, transform.rotation);
        yield break;
    }
    // 効果音の読み込み
    public void LoadSE()
    {
        // SEの文字を読み込む
        loadStr = (Resources.Load("SummonSeStr", typeof(TextAsset)) as TextAsset).text;
        seStr = loadStr.Split(char.Parse(","));
    }
    */
}
