﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//岩に関する処理
public class RockEfect : MonoBehaviour
{
    // ★岩の移動関係の変数★

    // 物理挙動
    private Rigidbody m_rigidbody;
    //移動するかどうか
    public bool MoveFlg;

    // ★岩の破砕関係の変数★

    //壊れるかどうか
    public bool BrokenFlg;
    // 岩の移動値
    //private Vector3[] Num_Force = { new Vector3(0, 0, 10), new Vector3(10, 0, 0), new Vector3(0, 0, -10), new Vector3(-10, 0, 0) };
    // 破砕演出
    public GameObject RuptureEfect;
    //プレイヤー(岩の移動に使う)
    private GameObject player;
    //プレイヤーの方向
    private Vector3 Ply_dir;

    // ★岩のTransformの変更★

    //倍率
    public float magnification;

    
    void Start()
    {        
        // 岩の挙動の初期設定
        Init_RRB();
        // 岩の大きさの変更
        Init_RS();
        // 岩の破砕演出の初期設定
        Init_RE();
    }
    

    // 岩の大きさ(RockSize)の変更
    void Init_RS()
    {
        // 岩の大きさの変更
        if(magnification != 0)transform.localScale = transform.localScale * magnification;
         else magnification = 1;

    }

    // 岩の挙動(RockRigidBody)の初期設定
    void Init_RRB()
    {
        //プレイヤを取得
        player = GameObject.Find("Player0");
        // 物理挙動の取得
        m_rigidbody = this.GetComponent<Rigidbody>();
        // 移動する(岩を移動させる際にスムーズでないため回転はさせない)
        if (MoveFlg == true){
            m_rigidbody.constraints = RigidbodyConstraints.FreezeRotation;}
        // 移動しない(座標と角度を固定)
        else m_rigidbody.constraints = RigidbodyConstraints.FreezeAll;
        

    }

    // 岩の破砕演出(RockEfect)の初期設定
    void Init_RE()
    {
        // 親
        var parent = this.transform;
        // 破砕演出の生成（動的に生成すると重いため先に生成しておく）
        RuptureEfect = Instantiate(RuptureEfect, transform.position, RuptureEfect.transform.localRotation, parent);
        // 破砕演出開始
        RuptureEfect.GetComponent<Efect>().EffectStart(magnification);
        // 非アクティブにしておく
        RuptureEfect.SetActive(false);

    }

    // 岩の破砕演出(RockEfect)の呼び出し
    void Call_RE()
    {
        // 生成しておいた破砕演出をアクティブにする
        RuptureEfect.SetActive(true);
        // 親子関係の解除（破砕演出の終了前に自身が消されないようにするため）
        RuptureEfect.transform.parent = null;
    }

    // 岩が壊れる
    void Break_Rock(Collider collision)
    {
        //壊れる
        if (BrokenFlg)
        {
            //通常攻撃
            if (collision.gameObject.CompareTag("NormalWeapon") || collision.gameObject.CompareTag("DollNormalWeapon"))
            {
                Call_RE();// 破砕演出
                Destroy(this.gameObject);// 削除               
            }
            //スキル攻撃
            if (collision.gameObject.CompareTag("SkillWeapon") || collision.gameObject.CompareTag("DollSkillWeapon"))
            {
                Call_RE();// 破砕演出
                Destroy(this.gameObject);// 削除
            }
        }
    }
    /*
    // 岩の移動
    void Move_Rock(Collision collision)
    {
        // プレイヤと当たったなら移動判定
        if (collision.gameObject.name == "Player0")
        {
            // プレイヤーの方向を取得
            Ply_dir = player.transform.localEulerAngles;
            // rigidbodyをAddForce関数で移動させる。
            for (int i = 0; i < 4; i++)//右下上をオイラー角で取得　＊コントローラーに依存しないため＊
            { //角度に応じて移動。
                if (Ply_dir.y == 90 * i) m_rigidbody.AddForce(Num_Force[i], ForceMode.Force);
            }
            //0度が取得できなかったため4次元数を条件に移動
            if (player.transform.rotation.w == 1.0f) m_rigidbody.AddForce(Num_Force[0], ForceMode.Force);
        }
    }
    */
    // 当たり判定(当たる相手のコライダーがすり抜けないなら)
    private void OnCollisionStay(Collision collision)
    {
        // 岩が移動する。
        //Move_Rock(collision);
    }

    // 当たり判定(当たる相手の当たる相手のコライダーがすり抜けるなら)
    private void OnTriggerEnter(Collider collision)
    {
        // 岩が壊れる
        Break_Rock(collision);
    }

}
