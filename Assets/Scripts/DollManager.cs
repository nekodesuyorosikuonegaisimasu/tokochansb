﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class DollManager : MonoBehaviour
{

    private GameObject Player;//本体
    private float PlayerDistance;//本体との距離を測る
    public GameObject Weapon;//武器

    private float Length;//射程
    private float Speed;//移動速度

    private GameObject Target;//狙う敵
    private GameObject Enemy;//敵
    private float TargetDistance = 100;//初期値のままだと動かなくなるため適当な値を入れる
    private float EnemyDistance;

    private float PT_Distance;
    
    private int StatusShift;

    private int AnimationTime;
    private int LifeTimer;

    private float Angle;
    private float AngleReal;
    private Vector3 Poslog;

    // Use this for initialization
    void Start()
    {
        Player = GameObject.Find("Player" + (PlayDataManager.CharaNum));
        Length = 20;
        Speed = 6.0f;
        GetComponent<NavMeshAgent>().speed = Speed;
        //消えるまでの時間をセット
        LifeTimer =　540;
        //最も近かったオブジェクトを取得
        TargetSerch();
        AngleReal = -transform.localEulerAngles.y+90.0f;
        Poslog = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        switch(StatusShift)
        {
            case 0:Move();
                break;
          
        }

    }
    void FixedUpdate()
    {
        //ライフタイマーを徐々に減らす
        LifeTimer--;
        switch (StatusShift)
        {
            case 1:
                Attack();
                break;
            case 2:
                Death();
                break;
        }
    }
    void Move()
    {
        //ターゲットの親オブジェクトのHPが0以下になっていたらターゲットを空にする
        if (Target != null && Target != Player)
        {
            TargetDistance = Length;
            Target = null;
        }
        //ターゲットがプレイヤーのときは射程を広げておく
        if (Target == Player) TargetDistance = Length;
        //ターゲットがいないかプレイヤーを狙っている場合はターゲットを探す
        if (Target == null || Target == Player) TargetSerch();
        //ターゲットがいる場合プレイヤーとターゲット間の距離を計算
        if (Target != null) PT_Distance = Vector3.Distance(Target.transform.position, Player.transform.position);
        //ターゲットがいないか狙っている敵か人形の位置がプレイヤーの射程外のときは近くに戻ってくるように
        if (Target == null || PlayerDistance > Length || PT_Distance > Length)Target = Player;
        //人形とターゲットの距離を計算する
        if(Target != null)TargetDistance = Vector3.Distance(Target.transform.position, transform.position);
        //Debug.Log(TargetDistance);
        //ターゲットが近くにいたら攻撃する
        if (Target != Player && TargetDistance <1.3f)
        {
            StatusShift = 1;
            AnimationTime = 30;
            //武器のあたり判定をオンにする
            Weapon.GetComponent<Collider>().enabled = true;
            //攻撃中は動かないようにする
            GetComponent<NavMeshAgent>().speed = 0;
        }
        //ターゲットの方向に移動する
        if(Target != null)GetComponent<NavMeshAgent>().destination = Target.transform.position;

        //プレイヤーの移動後と移動前の座標から角度を算出
        Angle = Mathf.Atan2(transform.position.z - Poslog.z, transform.position.x - Poslog.x) * Mathf.Rad2Deg;
        //振り向く角度が180度を上回っていた場合
        if (Angle - AngleReal > 180) AngleReal = 360 + AngleReal;
        if (Angle - AngleReal < -180) AngleReal = -360 + AngleReal;
        //滑らかに方向転換できる関数を用意
        AngleReal += (Angle - AngleReal) / 20;
        //プレイヤーが移動していた場合向きを変更する
        if (transform.position.x != Poslog.x || transform.position.z != Poslog.z)
            transform.rotation = Quaternion.Euler(0, -AngleReal + 90.0f, 0);
        //プレイヤーの座標を更新
        Poslog = transform.position;

        //ライフタイマーが切れていたら死ぬ
        if (LifeTimer <= 0)
        {
            StatusShift = 2;
            AnimationTime = 100;
            //動かないようにする
            GetComponent<NavMeshAgent>().speed = 0;
        }


    }
    //最寄りの敵を検索する
    void TargetSerch()
    {
        //エネミータグのついたオブジェクトを検索
        foreach (GameObject Enemy in GameObject.FindGameObjectsWithTag(("Enemy")))
        {
            //エネミーと人形の距離を計算
            EnemyDistance = Vector3.Distance(Enemy.transform.position, transform.position);
            //エネミーがターゲットよりも近くにいたらターゲットをエネミーに切り替える
            if (TargetDistance > EnemyDistance)
            {
                TargetDistance = EnemyDistance;
                Target = Enemy;
            }
        }
        //ギミックタグのついたオブジェクトを検索
        foreach (GameObject Enemy in GameObject.FindGameObjectsWithTag("Gimmick"))
        {
            //ギミックと人形の距離を計算
            EnemyDistance = Vector3.Distance(Enemy.transform.position, transform.position);
            //ギミックがターゲットよりも近くにいたらターゲットをギミックに切り替える
            if (TargetDistance > EnemyDistance)
            {
                TargetDistance = EnemyDistance;
                Target = Enemy;
            }
        }



        //プレイヤーと人形間の距離を計算
        PlayerDistance = Vector3.Distance(Player.transform.position, transform.position);
    }
    //攻撃時の動作
    void Attack()
    {
        //攻撃のアニメーションを再生
        GetComponent<Animator>().SetBool("Attack", true);
        //再生が終わったら元の動作に戻る
        if (AnimationTime-- < 0)
        {
            GetComponent<Animator>().SetBool("Attack", false);
            StatusShift = 0;
            Weapon.GetComponent<Collider>().enabled = false;
            GetComponent<NavMeshAgent>().speed = Speed;
        }
        //人形と敵の座標から向きたい角度を計算
        if (Target != null)
        {
            Angle = Mathf.Atan2(transform.position.z - Target.transform.position.z, transform.position.x - Target.transform.position.x) * Mathf.Rad2Deg;
            //敵の方向を向く
            transform.rotation = Quaternion.Euler(0, -Angle - 90, 0);
        }
    }
    //死亡時の動作
    void Death()
    {
        GetComponent<Animator>().SetBool("Death", true);
        if (AnimationTime-- < 0)
        {
            Destroy(this.gameObject);
        }
    }
}
