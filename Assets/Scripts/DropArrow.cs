﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropArrow : MonoBehaviour
{
    /*
    // アイテムがふわふわと上下するための変数
    private float nowAxis;
    // アイテム画像用オブジェクト
    GameObject arrowObj;
    // 地面に落ちたらふわふわ動かすためのbool
    private bool moveFlag;

    void Start()
    {
        moveFlag = false;
        // ふわふわ動く動きの場所を決める用
        nowAxis = this.transform.position.y; 
        // プレイヤーの取得
        arrowObj = GameObject.Find("Player" + PlayDataManager.CharaNum);
    }

    void Update()
    {
        if (moveFlag)
        {
            // ふわふわ浮くような動きをさせる
            transform.position = new Vector3(transform.position.x, nowAxis + Mathf.PingPong(Time.time / 3, 0.5f), transform.position.z);
        }
        else
        {
            // moveFlagがonになるまでNowAxisの値は変え続ける(地面についたらonになるので)
            nowAxis = this.transform.position.y;
        }
        //ゆっくり回る動き(一秒で90度)
        transform.Rotate(new Vector3(0, 90, 0) * Time.deltaTime, Space.World);
    }
    // 触れたら処理する
    public void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.CompareTag("Player")) // プレイヤーに触れたら
        {
            GetArrow();
        }
        else if (collider.gameObject.CompareTag("Ground")) // 地面に触れたら
        {
            moveFlag = true;
        }
    }

    public void GetArrow()
    {
        // 弓矢アイテムの数を増やす
        arrowObj.GetComponent<Attack>().arrowCount += 1;
        // 弓矢アイテムを消す
        Destroy(this.gameObject);
    }
    */
}
