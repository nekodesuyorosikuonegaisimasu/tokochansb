﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageWall : MonoBehaviour
{
    // 時間を計る
    private float count;
    // 壁が上昇する時間
    [SerializeField]
    private float upTime;
    // 飛ぶ方向の指定
    [SerializeField]
    private float moveX, moveY, moveZ;
    // 上昇しきったフラグ
    public bool upFalg;
    // スイッチ
    [SerializeField]
    private GameObject[] wallSwitch;
    // 動きのタイプ
    public int actionType;

    // Start is called before the first frame update
    void Start()
    {
        // 初期化
        count = 0;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // 壁上昇処理
    public void WallUp()
    {
        if (count < upTime)
        {
            this.gameObject.GetComponent<Transform>().transform.position += new Vector3(moveX, moveY, moveZ);
            count++;
        }
        else
        {
            // 上昇しきったので動かなくする
            actionType = 0;
            // 上昇しきったフラグをたてる
            upFalg = true;
            // countがupTimeを超えないようにする
            count = upTime;
        }
    }
    // 壁下降処理
    public void WallDown()
    {
        if (count > 0)
        {
            this.gameObject.GetComponent<Transform>().transform.position += new Vector3(-moveX, -moveY, -moveZ);
            count--;
        }
        else
        {
            // 下降しきったので動かなくする
            actionType = 0;
            // 下降しきったのでフラグを下す
            upFalg = false;
            // countが0を下回らないようにする
            count = 0;
        }
    }
}
