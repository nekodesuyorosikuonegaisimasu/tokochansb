﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlantSeed : MonoBehaviour
{
    // 植えられる場所のUI
    private GameObject PlantUI;
    // プレイヤー
    private GameObject Player;
    // 植えられる場所のオブジェクト
    private GameObject[] PlantPositionObj;
    // 種のオブジェクト
    private GameObject SeedObj;
    // 植えられる場所に触れているフラグ
    public bool IsRidePlantPosition;


    // Start is called before the first frame update
    void Start()
    {
        // 植えれらる場所のオブジェクトをすべて探す
        PlantPositionObj = GameObject.FindGameObjectsWithTag("Seed");
        // プレイヤー
        Player = GameObject.Find("Player");

        // 植えれらる場所のオブジェクトを非表示にする
        for (int i = 0; i < PlantPositionObj.Length; i++)
        {
            PlantPositionObj[i].SetActive(false);
        }
        // 植えれらる場所に乗っているフラグを初期化
        IsRidePlantPosition = false;
    }

    // Update is called once per frame
    void Update()
    {
        // ポーズ中は操作を受け付けない
        if (Mathf.Approximately(Time.timeScale, 0f)) return;
        // プレイヤーがいなくなったら探す処理
        if (Player == null) Player = GameObject.Find("Player");
        // 種を植える場所を表示/非表示にする処理
        PlayerCatchSeed();
    }

    // プレイヤーと種の関係
    void PlayerCatchSeed()
    {
        // もしプレイヤーが種を持っていたら
        if (Player.GetComponent<Attack>().IsHoldingSeed)
        {
            // 植えれらる場所のオブジェクトを表示する
            for (int i = 0; i < PlantPositionObj.Length; i++)
            {
                PlantPositionObj[i].SetActive(true);
            }
        }
        else
        {
            // 植えれらる場所のオブジェクトを非表示にする
            for (int i = 0; i < PlantPositionObj.Length; i++)
            {
                PlantPositionObj[i].SetActive(false);
            }
        }
    }
    // 植えた種を登録する処理
    public void GetPlantSeed(GameObject Seed)
    {
        SeedObj = Seed;
    }
    // 植えられた種の位置を調整する
    public void SetPlantSeed()
    {
        // 種に一番近いオブジェクトを検索するための変数
        float Dis = 0;
        float NearDis = 100;
        GameObject NearObj = null;

        foreach(GameObject Obj in GameObject.FindGameObjectsWithTag("Seed"))
        {
            // 種との距離を計算
            Dis = Vector3.Distance(Obj.transform.position, SeedObj.transform.position);
            // 直前に計算下オブジェクトのほうが近かったら更新
            if(NearDis > Dis || Dis == 0)
            {
                // 距離を保存
                NearDis = Dis;
                // 近いオブジェクトを更新
                NearObj = Obj;
                Debug.Log(NearObj.name);
            }
        }
        // 一番近かったオブジェクトの位置に種を合わせる
        SeedObj.transform.position = NearObj.transform.position;
    }
        
    
}
