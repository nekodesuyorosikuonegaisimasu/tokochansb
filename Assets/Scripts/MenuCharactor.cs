﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuCharactor : MonoBehaviour
{   
    private static string[] WeaponStr;
    private GameObject[] Weapon = new GameObject[10];
    private int Count ;
    private GameObject Shield;

    private Vector3 PlayerPos;//騎士の座標の記録を録る
    private float Angle;//騎士を向かせたい方向を入れる
    private float AngleReal;//騎士が急に向きを変えないようにする

    //タイトル、ステージセレクト、キャラクターセレクト、ゲームオーバー画面での騎士の武器装備、向きの調整を行う
    void Awake()
    {       
        Count = 0;
        //キャラの装備する武器のリストを作成する(プレイヤークラスを参考)
        //武器が登録されていなかったら
        if (WeaponStr == null)
        {        
            //武器の一覧が記載された文字列を取得
            string　LoadStr = (Resources.Load("WeaponStr", typeof(TextAsset)) as TextAsset).text;
            //文字列を","ごとに分別された配列に変換
            WeaponStr = LoadStr.Split(char.Parse(","));
        }
        //配列の名前をしたオブジェクトを検索する
        for (int i = 0; i < PlayDataManager.CHARA_NAME.Length; i++)
        {
            Weapon[i] = transform.Find("root/pelvis/Weapon/"+WeaponStr[Count]).gameObject;
            //登録された武器を非表示にしていく
            Weapon[i].SetActive(false);
            Count += 2;
        }
        //シールドを検索する
        Shield = transform.Find("root/pelvis/Shield").gameObject;
        Shield.SetActive(false);

        //プレイヤーの初期配置、方向を取得しておく
        PlayerPos = transform.position;
        Angle = transform.eulerAngles.y+ 90.0f;
        AngleReal = transform.eulerAngles.y+90.0f;

        //タイトル画面、ステージ選択画面なら前回選択したキャラクターの武器を自動で装備する
        if(SceneManager.GetActiveScene().name == "Title" || SceneManager.GetActiveScene().name == "StageSelect")
        {
            //選択中のキャラの武装のみ表示する
            Weapon[PlayDataManager.CharaNum].SetActive(true);
            if (PlayDataManager.CharaNum == 0) Shield.SetActive(true);
        }
    }
    //CharaNumの職に対応した武器を装備させる(キャラクターセレクトでのみ使用)
    public void WeaponActive(int CharaNum)
    {
        //選択中のキャラの武装のみ表示する
        Weapon[CharaNum].SetActive(true);                   
        if (CharaNum == 0) Shield.SetActive(true);
    }
    //キャラクターの向きを調整する
    void FixedUpdate()
    {
        //キャラクターセレクト画面以外のとき
        if (SceneManager.GetActiveScene().name != "CharaSelect")
        {
            //プレイヤーが移動をしていた場合角度を変更していく
            if (transform.position.x != PlayerPos.x || transform.position.z != PlayerPos.z)
            {
                //プレイヤーの移動後と移動前の座標から角度を算出
                Angle = Mathf.Atan2(transform.position.z - PlayerPos.z, transform.position.x - PlayerPos.x) * Mathf.Rad2Deg;
                //振り向く角度が180度を上回っていた場合
                if (Angle - AngleReal > 180) AngleReal = 360 + AngleReal;
                if (Angle - AngleReal < -180) AngleReal = -360 + AngleReal;
            }
            //滑らかに方向転換していくようにする
            AngleReal += (Angle - AngleReal) / 20;
            //プレイヤーが移動していた場合向きを変更する       
            transform.rotation = Quaternion.Euler(0, -AngleReal + 90.0f, 0);

            //プレイヤーの座標を更新
            PlayerPos = transform.position;
        }
    }
}
