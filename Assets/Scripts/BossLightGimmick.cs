﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// ボス戦のギミックの子オブジェクトを扱うcs
public class BossLightGimmick : MonoBehaviour
{
    GameObject GemObject; // 孫オブジェクトのジェム
    GameObject LightObject; // ひ孫オブジェクトのライト

    public GameObject Player;   // プレイヤー
    public GameObject Dragon;   // ドラゴン

    Attack attack; // Attack.cs
    
    public bool OperateFlag; // 作動したかどうかのbool

    private void Awake()
    {
        GemObject = transform.Find("castle_tower_round/SphereGemLarge").gameObject; // 子オブジェクト取得(ジェム)
        LightObject = transform.Find("castle_tower_round/SphereGemLarge/Point Light").gameObject; // 子オブジェクト取得(ライト)
    }

    void Start()
    {
        Player = GameObject.Find("Player"); // Playerを探す
        Dragon = GameObject.Find("RedDragon"); // Dragonを探す
        attack = Player.GetComponent<Attack>(); // アタックの取得
        
        OperateFlag = false; // 初期化
    }

    void Update()
    {
        //if (OperateFlag) GemMove(); // 作動時の移動処理
        Operate(); // 作動処理
    }

    //攻撃に触れたらチェストを開ける
    public void OnTriggerEnter(Collider collision)
    {
        Debug.Log(collision.gameObject.name);
        if ((collision.gameObject.CompareTag("NormalWeapon") && attack.NormalCoolTime != 0) || collision.gameObject.CompareTag("DollNormalWeapon")
            || (collision.gameObject.CompareTag("SkillWeapon") && attack.SPCoolTime != 0) || collision.gameObject.CompareTag("DollSkillWeapon"))
        { // 攻撃全般、当たると作動させる。
            OperateFlag = true;
        }
    }

    public void Operate()
    {
        if (OperateFlag)
        {
            LightObject.SetActive(true);// ギミックのライトをつける
            // 上下にゆらゆらと移動をさせたかったが固定にした
            //GemObject.transform.position = new Vector3(GemObject.transform.position.x, CentralPosition + Mathf.PingPong(1.0f/*Time.time*/, 2.5f), GemObject.transform.position.z);
            // 少しずつ回転をする
            GemObject.transform.Rotate(new Vector3(0, 30, 0) * Time.deltaTime, Space.World);
        }
        else
        {
            LightObject.SetActive(false);// ギミックのライトを消す
        }
    }

    //public void GemMove()
    //{
    //    // 上下に移動を続ける
    //    GemObject.transform.position = new Vector3(GemObject.transform.position.x, CentralPosition + Mathf.PingPong(Time.time, 2.5f), GemObject.transform.position.z);
    //    // 少しずつ回転をする
    //    GemObject.transform.Rotate(new Vector3(0, 30, 0) * Time.deltaTime, Space.World);
    //}
}
