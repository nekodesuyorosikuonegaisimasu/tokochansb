﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageSwitch : MonoBehaviour
{
    // 姫を邪魔する壁
    [SerializeField]
    private GameObject[] wall;
    // 壁をどかすスイッチ
    [SerializeField]
    private GameObject wallSwitch;
    //// 動きのタイプ
    //public int actionType;
    // 一回しか当たらないようにする
    private bool onceFlag;

    // Start is called before the first frame update
    void Start()
    {
        // 初期化
        onceFlag = false;
    }

    // Update is called once per frame
    private void FixedUpdate()
    {
        for(int i = 0; i < wall.Length; i++)
        {
            switch (wall[i].GetComponent<StageWall>().actionType)
            {
                // 何もしない
                case 0:
                    break;
                // 上昇中
                case 1:
                    wall[i].GetComponent<StageWall>().WallUp();
                    break;
                // 下降中
                case 2:
                    wall[i].GetComponent<StageWall>().WallDown();
                    break;
            }
        }
    }

    private void OnTriggerEnter(Collider collider)
    {
        // プレイヤーの攻撃が当たったら
        if ((collider.gameObject.CompareTag("NormalWeapon")
        || collider.gameObject.CompareTag("DollNormalWeapon")
        || collider.gameObject.CompareTag("SkillWeapon")
        || collider.gameObject.CompareTag("DollSkillWeapon")
        || collider.gameObject.CompareTag("SummonWeapon"))
        && !onceFlag)
        {
            onceFlag = true;
            Debug.Log("当たりました");
            for(int i = 0; i < wall.Length; i++)
            {
                // 壁が下りている状態で起動
                if (wall[i].GetComponent<StageWall>().actionType == 0 && !wall[i].GetComponent<StageWall>().upFalg)
                {
                    Debug.Log("止まるから上がる");
                    wall[i].GetComponent<StageWall>().actionType = 1;
                }
                // 壁が上がっている状態で起動
                else if (wall[i].GetComponent<StageWall>().actionType == 0 && wall[i].GetComponent<StageWall>().upFalg)
                {
                    Debug.Log("止まるから下がる");
                    wall[i].GetComponent<StageWall>().actionType = 2;
                }
                // 壁が上昇中に起動
                else if (wall[i].GetComponent<StageWall>().actionType == 1)
                {
                    Debug.Log("上昇中に起動");
                    wall[i].GetComponent<StageWall>().actionType = 2;
                }
                // 壁が下降中に起動
                else if (wall[i].GetComponent<StageWall>().actionType == 2)
                {
                    Debug.Log("下降中に起動");
                    wall[i].GetComponent<StageWall>().actionType = 1;
                }
                Debug.Log(wall[i].GetComponent<StageWall>().actionType);
            }
        }
        else if ((collider.gameObject.CompareTag("NormalWeapon")
             || collider.gameObject.CompareTag("DollNormalWeapon")
             || collider.gameObject.CompareTag("SkillWeapon")
             || collider.gameObject.CompareTag("DollSkillWeapon")
             || collider.gameObject.CompareTag("SummonWeapon"))
             && onceFlag)
        {
            onceFlag = false;
        }
    }
}
