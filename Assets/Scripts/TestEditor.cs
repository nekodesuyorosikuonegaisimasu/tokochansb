﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;

//()の中にクラス名を入れて、エディタ拡張をするクラスを指定
[CustomEditor(typeof(TestLoopObj))]

//エディタ拡張
public class TestEditor : Editor
{
    private int listSize = 0;//配列の長さを一時的に保存するための変数

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        var gameobjects = serializedObject.FindProperty("gameObjects");     //LoopObjクラスの配列gameObjectsを取得
        TestLoopObj loopObj = target as TestLoopObj;                        //LoopObjクラスのインスタンスを取得
        listSize = gameobjects.arraySize;                                   //配列の長さを取得
        listSize = EditorGUILayout.IntField("Size", listSize);              //一時的に保存した配列の長さをカスタムインスペクタに描画（ここで書き換えも可能）
        EditorGUILayout.Space();                                            //スペースを描画

        //一時的に保存した配列の長さと、本来の配列の長さが同じかチェックする
        if (listSize != gameobjects.arraySize)
        {
            //長さが違う場合は


            gameobjects.arraySize = listSize;     //長さの変更を適用

            //ここでserializedObjectへの変更を適用し、再び更新する
            serializedObject.ApplyModifiedProperties();
            serializedObject.Update();
        }

        else
        {
            //一時的に保存した配列の長さと、本来の配列の長さが同じ場合は　配列の要素を描画する                 
            for (int i = 0; i < gameobjects.arraySize; ++i)
            {
                loopObj.gameObjects[i].name = EditorGUILayout.TextField("name:", loopObj.gameObjects[i].name);
                //04/07　マテリアル設定にアクセスできる用にしたい。
                //loopObj.gameObjects[i].material = EditorGUILayout.ObjectField("materialname:", loopObj.gameObjects[i].material,typeof(Renderer),false);

                //loopObj.gameObjects[i].accessory.name = EditorGUILayout.TextField("accessory name:", player.weapons[i].accessory.name);


                EditorGUILayout.Space();  //スペースを描画
            }
        }


        base.OnInspectorGUI();      //正しく動作しているか確認にするために、元のインスペクターを表示
        serializedObject.ApplyModifiedProperties();    
    }
}

#endif