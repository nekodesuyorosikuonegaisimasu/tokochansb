﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

//障害物に当たった姫を止める
public class StopPrincess : MonoBehaviour
{
    //ゲームオブジェクト
    private GameObject Princess; //姫の控え
    

    private void Start()
    {
        Princess = GameObject.Find("Princess");
    }
    
    //送られてきた場所に位置を調整
    public void PositionHoming(Transform ObstaclesObj)
    {this.gameObject.transform.position = ObstaclesObj.position;}

    //障害物が消えた
    public void GoPrinces()
    {
        Princess = GameObject.Find("Princess");
        if (Princess != null)
        {
            //障害物にぶつかっているを落とす
            Princess.GetComponent<PrincessManager>().ObstaclesStopFlag = false;
            //動き再開
            Princess.GetComponent<PrincessManager>().MoveFlag = true;
            //止める指示をなくす
            Princess.GetComponent<NavMeshAgent>().isStopped = false; 
            //非表示
            this.gameObject.SetActive(false);
        }
    }

    //障害物が現れた
    public void StopPrinces()
    {
        Princess = GameObject.Find("Princess");
        if (Princess != null)
        {
            //表示
            this.gameObject.SetActive(true);
        }
    }

    //当たり判定
    void OnTriggerStay(Collider other)
    {
        //姫に当たった
        if (other.name == "Princess")
        {
            //障害物にぶつかっているフラグを立てる
            other.GetComponent<PrincessManager>().ObstaclesStopFlag = true;
            //動きを止める
            other.GetComponent<PrincessManager>().MoveFlag = false;
            //止める指示
            other.GetComponent<NavMeshAgent>().isStopped = true;
            //姫の存在を記録しておく
            Princess = other.gameObject;
        }
    }
    //当たりから外れる
    void OnTriggerExit(Collider other)
    {
        //姫に当たった
        if (other.name == "Princess")
        {
            //障害物にぶつかっているを落とす
            other.GetComponent<PrincessManager>().ObstaclesStopFlag = false;
            //動き再開
            other.GetComponent<PrincessManager>().MoveFlag = true;
            //止める指示をなくす
            other.GetComponent<NavMeshAgent>().isStopped = false;
        }
    }
}
