﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;


public class TimeText : MonoBehaviour
{
    //オブジェクト
    public GameObject BlackImage; //黒背景
    public GameObject CameraObj; //カメラ
    public GameObject CanvasObj; //Textの入っているキャンバス
    
    //時間
    DateTime Today; //今の時間取得

    //値
    float[] DayFlo = new float[6]; //時間を値に変換用の箱
    float[] UpTimeSpeed = new float[6]; //時間の増加速度
    float CountTime; //タイミングカウント用
    Vector3 DuplicateCameraRot; //カメラ角度控え
    Vector3 DuplicateCanvasPos; //キャンバスの位置控え
    Vector3 FirstCameraRot; //初めのカメラ角度控え
    Vector3 FirstCanvasPos; //初めのキャンバス角度控え

    //色
    Color TimeTextColor; //時間テキストの色

    //文
    Text DayText; //時間表示テキスト
    public String[] DuplicateText = new String[6]; //時間を文に整える用

    //判定
    bool ReplayedFlag; //再構成開始か判定
    public bool EndFlag; //終了

    // Start is called before the first frame update
    void Start()
    {
        BlackImage.SetActive(false); //黒背景非表示

        //テキスト取得
        DayText = this.gameObject.GetComponent<Text>();

        //時間
        Today = DateTime.Now; //現在の日付・時刻
        //テキストに入力
        DayText.text = Today.Year.ToString() + "/ " + Today.Month.ToString()
            + "/" + Today.Day.ToString() + "  " + DateTime.Now.ToLongTimeString();

        //数値に変換
        DayFlo[0] = Today.Year; //年
        DayFlo[1] = Today.Month; //月
        DayFlo[2] = Today.Day; //日
        DayFlo[3] = Today.Hour; //時
        DayFlo[4] = Today.Minute; //分
        DayFlo[5] = Today.Second; //秒

        //文字列と数列を空にしておく
        for (int i = 0; i < 6; i++) { DuplicateText[i] = ""; UpTimeSpeed[i] = 0;}

        CountTime = 0; //カウント初期化

        DuplicateCameraRot = CameraObj.transform.eulerAngles; //カメラ角度控え
        DuplicateCanvasPos = CanvasObj.transform.GetChild(0).transform.position; //キャンバス位置の控え
        FirstCameraRot = CameraObj.transform.eulerAngles; //カメラ角度控え
        FirstCanvasPos = CanvasObj.transform.GetChild(0).transform.position; //キャンバス位置の控え

        TimeTextColor = DayText.color; //色取得

        ReplayedFlag = false; //再構成を開始しない
        EndFlag = false;
    }

    // Update is called once per frame
    void Update()
    {
        CountTime += Time.deltaTime; //カウント
        if(CountTime < 20) {UpTime(); CameraRote();} //一定の時間加算
        else if (!ReplayedFlag) { ResetTime();} //再構成時1度だけ実行
        else if(TimeTextColor.a != 1.0f) { if(CountTime > 22) FadeInText();} //Textをフェードイン
        else if(DayFlo[0] < Today.Year){ UpTime();}  //現代まで時間を加算
        else { NowTimeUpdateText(); } //時間を現在で固定
        if (CountTime > 40) { EndFlag = true;}

    }

    //時間加算処理
    void UpTime()
    {
        //徐々に時間経過が加速
        if (UpTimeSpeed[5] < 240) { UpTimeSpeed[5] += Time.deltaTime * 50; }
        if (UpTimeSpeed[4] < 240 && UpTimeSpeed[5] >= 240) { UpTimeSpeed[4] += Time.deltaTime * 200; }
        if (UpTimeSpeed[3] < 240 && UpTimeSpeed[4] >= 240) { UpTimeSpeed[3] += Time.deltaTime * 200; }
        if (UpTimeSpeed[3] >= 240) { UpTimeSpeed[2] = 20; }
        if (UpTimeSpeed[2] >= 20) { UpTimeSpeed[1] = 20; }
        if (UpTimeSpeed[0] < 240 && UpTimeSpeed[1] >= 20) { UpTimeSpeed[0] += Time.deltaTime * 50; }

        for (int i = 0; i < 6; i++) { DayFlo[i] += (int)(UpTimeSpeed[i] / 20); } //時間加算

        //時間の繰り上がり
        if (DayFlo[5] >= 60) { DayFlo[5] = 0; DayFlo[4]++; } //60秒を超えたら1分追加
        if (DayFlo[4] >= 60) { DayFlo[4] = 0; DayFlo[3]++; } //60分超えたら1時間追加
        if (DayFlo[3] >= 24) { DayFlo[3] = 0; DayFlo[2]++; } //24時間超えたら1日追加
        if (DayFlo[2] > 31) { DayFlo[2] = 1; DayFlo[1]++; } // 31日超えたら1カ月追加
        if (DayFlo[1] > 12) { DayFlo[1] = 1; DayFlo[0]++; } //12カ月超えたら1年追加

        //すべての文字列に反映
        for (int i = 0; i < 6; i++)
        {
            DuplicateText[i] = DayFlo[i].ToString(); //数値を文字列に変換
            if (DayFlo[i] < 10) { DuplicateText[i] = "0" + DayFlo[i]; } //桁を揃える
        }

        //年数は、4桁に合わせる
        if (DayFlo[0] < 10) { DuplicateText[0] = "   " + DayFlo[0]; } //1桁の時
        if (DayFlo[0] < 100) { DuplicateText[0] = "  " + DayFlo[0]; } //2桁の時
        if (DayFlo[0] < 100) { DuplicateText[0] = " " + DayFlo[0]; } //3桁の時

        //テキストに反映
        DayText.text = DuplicateText[0] + "/ " + DuplicateText[1] + "/" + DuplicateText[2]
            + "  " + DuplicateText[3] + ":" + DuplicateText[4] + ":" + DuplicateText[5];
    }

    //Textをフェードイン
    void FadeInText()
    {
        //テキストカラー
        TimeTextColor.a += Time.deltaTime/2.0f; //少しづつ透明度を下げる
        if(TimeTextColor.a > 1.0f) { TimeTextColor.a =1.0f;} //透明度は最高1.0ｆ
        DayText.color = TimeTextColor; //色を実装
    }

    //加算処理リセットと時間の最初期化
    void ResetTime()
    {
        //惑星の処理の方を綺麗にしておく
        GameObject.Find("EarthManage").GetComponent<EarthChange>().GetLoopFlag(false);

        BlackImage.SetActive(true); //黒背景表示

        //時間の再取得
        Today = DateTime.Now; //現在の日付・時刻

        //テキスト
        //すべてに反映
        for (int i = 0; i < 6; i++) 
        {
            DayFlo[i] = 0;//時間の最初期化
            DuplicateText[i] = "00"; //0に揃える
            UpTimeSpeed[i] = 0; //数列を空にしておく
            
            
        }

        //年数は、4桁に合わせる
        if (DayFlo[0] < 10) { DuplicateText[0] = "   " + DayFlo[0]; } //1桁の時

        //テキストに反映
        DayText.text = DuplicateText[0] + "/ " + DuplicateText[1] + "/" + DuplicateText[2]
            + "  " + DuplicateText[3] + ":" + DuplicateText[4] + ":" + DuplicateText[5];

        ReplayedFlag = true; //再構成を開始

        //テキストカラー
        TimeTextColor.a = 0.0f; //透明に
        DayText.color = TimeTextColor; //色を実装
    }

    //時間を現在に
    void NowTimeUpdateText()
    {
        //テキストに入力
        DayText.text = Today.Year.ToString() + "/ " + Today.Month.ToString()
            + "/" + Today.Day.ToString() + "  " + DateTime.Now.ToLongTimeString();
    }

    //すべてリセット処理
    public void StartTime()
    {
        BlackImage.SetActive(false); //黒背景非表示

        //テキスト取得
        DayText = this.gameObject.GetComponent<Text>();

        //時間の再取得
        Today = DateTime.Now; //現在の日付・時刻
        //テキストに入力
        DayText.text = Today.Year.ToString() + "/ " + Today.Month.ToString()
            + "/" + Today.Day.ToString() + "  " + DateTime.Now.ToLongTimeString();

        //数値に変換
        DayFlo[0] = Today.Year; //年
        DayFlo[1] = Today.Month; //月
        DayFlo[2] = Today.Day; //日
        DayFlo[3] = Today.Hour; //時
        DayFlo[4] = Today.Minute; //分
        DayFlo[5] = Today.Second; //秒

        //文字列と数列を空にしておく
        for (int i = 0; i < 6; i++) { DuplicateText[i] = ""; UpTimeSpeed[i] = 0; }

        TimeTextColor = DayText.color; //一応保存
        TimeTextColor.a = 1.0f; //透明度は最高1.0ｆ
        DayText.color = TimeTextColor; //色を実装

        CameraObj.transform.eulerAngles = FirstCameraRot; //位置を戻す
        DuplicateCameraRot = FirstCameraRot; //控えも戻す
        CanvasObj.transform.position = FirstCanvasPos; //位置を戻す
        DuplicateCanvasPos = FirstCanvasPos; //控えも戻す
        CanvasObj.transform.eulerAngles = CameraObj.transform.GetChild(0).transform.eulerAngles; //角度実装

        ReplayedFlag = false; //再構成を開始しない
        EndFlag = false;

        CountTime = 0; //リセット
    }

    //カメラ回転
    void CameraRote()
    {
        //カメラ
        DuplicateCameraRot.y -= 0.01f; //加算
        CameraObj.transform.eulerAngles = DuplicateCameraRot; //実装

        //キャンバス
        DuplicateCanvasPos = CameraObj.transform.GetChild(0).transform.position; //位置取得
        DuplicateCanvasPos /= 1.34f; //カメラ範囲に位置を調整
        CanvasObj.transform.position = DuplicateCanvasPos; //位置実装
        CanvasObj.transform.eulerAngles = CameraObj.transform.GetChild(0).transform.eulerAngles; //角度実装
    }
}
