﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackDragon : MonoBehaviour
{
    GameObject player;
    // 飛んでいく速さ
    public float speed;
    // 球が消えるかどうか
    public bool clearFlag;
    // 攻撃が通常攻撃かどうか
    public bool normalAttackFlag;
    // クリアカウント
    public float clearCount;
    // 射程（攻撃が消えるまでの距離）
    public float NLength;
    public float SLength;
    // 攻撃力
    public float Power;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player" + (PlayDataManager.CharaNum));
        // 飛び道具を消す処理
        clearCount = 0.0f;
        // 射程
        //NLength = PlayDataManager.CHARA_STATUS[PlayDataManager.CharaNum, 1];
        SLength = PlayDataManager.CHARA_STATUS[PlayDataManager.CharaNum, 3];
    }

    // Update is called once per frame
    void Update()
    {
        // ポーズ中は操作を受け付けない
        if (Mathf.Approximately(Time.timeScale, 0f)) return;

        // 必殺技
        if (clearCount++ > SLength * 10)
        {
            // 消す
            Destroy(this.gameObject);
        }

        //Power = Power * player.GetComponent<Attack>().Item;
    }

    void FixedUpdate()
    {
        // 打ち出した前方にとんでいく
        GetComponent<Rigidbody>().velocity = transform.forward * 5000 * Time.fixedDeltaTime;
    }
}
