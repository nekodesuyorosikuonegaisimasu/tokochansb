﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : MonoBehaviour
{
    // キャラクターの攻撃の種類
    const int CHARA_ATTACK = 2;
    // アニメーター
    private Animator animator;
    //　Resourcesフォルダからテキストを読み込む
    private string loadStr;
    private int Count;
    // 武器
    private GameObject[,] weaponObj = new GameObject[PlayDataManager.CHARA_NAME.Length, CHARA_ATTACK];
    private string[] weaponStr;
    // 効果音
    private string[,] seName = new string[PlayDataManager.CHARA_NAME.Length, CHARA_ATTACK];
    private string[] seStr;

    // プレイヤー
    private Player player;
    // クールタイム
    private GameObject coolTime;
    private GameObject Ninstance;   // 通常攻撃用
    private GameObject Sinstance;   // 必殺技用
    // 近接武器
    [SerializeField]
    private GameObject NormalWeapon;
    [SerializeField]
    private GameObject SkillWeapon;
    // 飛び道具を使うプレイヤー用
    public GameObject Normalmagic;
    [SerializeField]
    private GameObject SPmagic;
    // クールタイム関係
    private float NCoolTime;
    public float NormalCoolTime = 0.0f;
    private float SCoolTime;
    public float SPCoolTime = 0.0f;
    public float itemCoolTime;
    // 攻撃UI
    private GameObject AttackUI;
    // 種
    public bool IsHoldingSeed;
    private GameObject PlantSeed;
    // アイテム
    public float Item;

    // 現在のプレイヤーの番号をとる
    [SerializeField]
    private int PlayerNum;

    //ポーズの管理
    Pause PauseManager;

    // Start is called before the first frame update
    void Start()
    {
        // 生成時のプレイヤー切り替え番号
        PlayerNum = PlayerChangeManager.ChangeCount;

        // アニメーション
        animator = this.GetComponent<Animator>();
        // 武器の登録
        LoadweponObj();
        // SEの登録
        LoadSE();
        for (int i = 0; i < PlayDataManager.CHARA_NAME.Length; i++)
        {
            for (int j = 0; j < CHARA_ATTACK; j++)
            {
                // 通常攻撃武器と必殺技武器をキャラクターごとに登録していく
                weaponObj[i, j] = GameObject.Find(weaponStr[Count]);
                // 通常攻撃効果音と必殺技効果音をキャラクターごとに登録していく
                seName[i, j] = seStr[Count];
                Count++;
            }
        }
        // 必殺技を非表示にするにする
        weaponObj[PlayerNum, 1].SetActive(false);
        // 攻撃のクールタイム
        NCoolTime = PlayDataManager.CHARA_STATUS[PlayerNum, 1];
        SCoolTime = PlayDataManager.CHARA_STATUS[PlayerNum, 3];
        // プレイヤー
        player = this.GetComponent<Player>();
        // クールタイマーをResourcesファイルからロード
        coolTime = (GameObject)Resources.Load("CoolTimeUI");
        // AttackUIを探す
        AttackUI = GameObject.Find("AttackUI");
        // 種保持フラグの初期化
        IsHoldingSeed = false;
        // 種を植える場所のオブジェクト
        PlantSeed = GameObject.Find("PlantSeedManager");
        // 選択中のアイテムの初期化
        Item = 1;
        // クールタイム減少アイテムの初期値
        itemCoolTime = 1.0f;

        //スクリプト取得
        PauseManager = GameObject.Find("PauseManager").GetComponent<Pause>();
    }

    // Update is called once per frame
    void Update()
    {
        // ポーズ中は操作を受け付けない
        if (Mathf.Approximately(Time.timeScale, 0f)) return;

        // 種保持中なら
        if (IsHoldingSeed)
        {
            // 種保持中の処理へ
            HoldingSeed();
            return;
        }

        // プレイヤーがスタン中でないなら
        if (!player.stunFlag)
        {
            // 種との当たり判定中かつ種が持てる状態で攻撃ボタンを押したなら
            if ((Input.GetButtonDown("Mouse_Fire1") || Input.GetButtonDown("Pad_Fire1")) && player.IsTouchSeed)
            {
                // 保持中フラグをtrueにする
                IsHoldingSeed = true;
                // 剣を非表示にする処理
                weaponObj[PlayerNum, 0].SetActive(false);
                weaponObj[PlayerNum, 1].SetActive(false);
                return;
            }

            // 通常攻撃
            if ((Input.GetButtonDown("Mouse_Fire1") || Input.GetButtonDown("Pad_Fire1")) && NormalCoolTime == 0.0f)
            {
                // 効果音再生
                SoundManager.Instance.PlaySeByName(seName[PlayerNum, 0], this.gameObject);
                // 通常攻撃
                weaponObj[PlayerNum, 0].SetActive(true);
                weaponObj[PlayerNum, 1].SetActive(false);
                // 武器のコライダーをオンにする
                if (NormalWeapon) NormalWeapon.GetComponent<Collider>().enabled = true;
                // クールタイム処理
                NormalCoolTime = NCoolTime;
                // アニメーションの再生
                animator.SetBool("NormalAttack", true);
                // クールタイマー生成処理
                Ninstance = (GameObject)Instantiate(coolTime, new Vector3(0, 0, 0), Quaternion.identity);
                //クールタイムが発生
                PauseManager.PlayUI[4] = Ninstance;
                Ninstance.GetComponent<CoolTimeUI>().Count(NCoolTime, 150, -420);

                // 飛び道具を使うプレイヤーの場合
                if (Normalmagic)
                {
                    // 飛び道具の生成
                    Instantiate(Normalmagic, transform.position + transform.forward + transform.up, transform.rotation);
                }
            }
            // クールタイム中の処理
            else
            {
                if (NormalCoolTime > 0.0f)
                {
                    NormalCoolTime -= itemCoolTime * Time.deltaTime;
                    animator.SetBool("NormalAttack", false);
                }
                else if (NormalCoolTime < 0.0f)
                {
                    if (NormalWeapon) NormalWeapon.GetComponent<Collider>().enabled = false;
                    NormalCoolTime = 0.0f;
                }
            }
            // 必殺技
            if ((Input.GetButtonDown("Mouse_Fire2") || Input.GetButtonDown("Pad_Fire2")) && SPCoolTime == 0.0f)
            {
                //敵にヒットしたとき一度だけヒットストップを行うためフラグを立てる
                HitStop.AttackFlag = true;
                // スキル音を鳴らす
                SoundManager.Instance.PlaySeByName(seName[PlayerNum, 1], this.gameObject);
                // 必殺技
                weaponObj[PlayerNum, 0].SetActive(false);
                weaponObj[PlayerNum, 1].SetActive(true);
                // 武器のコライダーをオンにする
                if (SkillWeapon) SkillWeapon.GetComponent<Collider>().enabled = true;
                SPCoolTime = SCoolTime;
                // アニメーションの再生
                animator.SetBool("SPAttack", true);
                // クールタイマー生成処理
                Sinstance = (GameObject)Instantiate(coolTime, new Vector3(0, 0, 0), Quaternion.identity);
                //クールタイムが発生
                PauseManager.PlayUI[5] = Sinstance;
                Sinstance.GetComponent<CoolTimeUI>().Count(SCoolTime, 320, -420);
                // 飛び道具を使うプレイヤーの場合
                if (SPmagic)
                {
                    // 飛び道具の生成
                    if (PlayerNum != 5 && PlayerNum != 6) Instantiate(SPmagic, transform.position + transform.forward + transform.up, transform.rotation);
                }
            }
            // クールタイム中の処理
            else
            {
                if (SPCoolTime > 0.0f)
                {
                    SPCoolTime -= itemCoolTime * Time.deltaTime;
                    animator.SetBool("SPAttack", false);
                }
                else if (SPCoolTime < 0.0f)
                {
                    SPCoolTime = 0.0f;
                }
                // 必殺技のモーションを終えた後武器を非表示にする
                if (SPCoolTime < 2.0f / 3.0f * SCoolTime)
                {
                    if (SkillWeapon) SkillWeapon.GetComponent<Collider>().enabled = false;
                    weaponObj[PlayerNum, 0].SetActive(true);
                    weaponObj[PlayerNum, 1].SetActive(false);
                }
            }
        }
    }
    // 種保持中の処理
    private void HoldingSeed()
    {
        // 植える処理
        if ((Input.GetButtonDown("Mouse_Fire1") || Input.GetButtonDown("Pad_Fire1")) && player.IsStickGround)
        {
            // 植えた判定にする
            player.Seed.GetComponent<LoopObj>().IsPlant = true;
            // ループで育つ判定
            player.Seed.GetComponent<LoopObj>().IsGrow = true;
            // 種保持フラグをfalseにする
            IsHoldingSeed = false;
            // 種に触れていない
            player.IsTouchSeed = false;
            // 種オブジェクトのタグをデフォルトに変える
            player.Seed.tag = ("Untagged");
            // 種オブジェクトのRigidbodyを取り除く
            Destroy(player.Seed.GetComponent<Rigidbody>());
            // 植えられた位置を特定の場所までずらす
            PlantSeed.GetComponent<PlantSeed>().GetPlantSeed(player.Seed);
            // 植えた時の種の位置を統一
            PlantSeed.GetComponent<PlantSeed>().SetPlantSeed();
            // 格納していた種を空にする
            player.Seed = null;
        }
        // 種保持中にスキル攻撃ボタンまたはスタンで種を手放す処理
        if (Input.GetButton("Mouse_Fire2") || Input.GetButton("Pad_Fire2") || player.stunFlag)
        {
            // 種保持フラグをfalseにする
            IsHoldingSeed = false;
            // 格納していた種を空にする
            player.Seed = null;
        }

        // 通常攻撃のクールタイム処理
        if (NormalCoolTime > 0.0f) NormalCoolTime -= itemCoolTime * Time.deltaTime;
        else if (NormalCoolTime < 0.0f) NormalCoolTime = 0.0f;
        // スキルのクールタイム処理
        if (SPCoolTime > 0.0f)　SPCoolTime -= itemCoolTime * Time.deltaTime;
        else if (SPCoolTime < 0.0f) SPCoolTime = 0.0f;
    }

    // テキストの読み込み
    public void LoadweponObj()
    {
        // 武器の文字を読み込む
        loadStr = (Resources.Load("WeaponStr", typeof(TextAsset)) as TextAsset).text;
        weaponStr = loadStr.Split(char.Parse(","));
    }
    // 効果音の読み込み
    public void LoadSE()
    {
        // SEの文字を読み込む
        loadStr = (Resources.Load("SeStr", typeof(TextAsset)) as TextAsset).text;
        seStr = loadStr.Split(char.Parse(","));
    }
}


