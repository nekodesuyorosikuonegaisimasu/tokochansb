﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class OptionManager : MonoBehaviour
{
    private GameObject[] BackScreen = new GameObject[2];
    private GameObject[] Select = new GameObject[6];
    private GameObject[] Slider = new GameObject[4];

    private float ScrollX;

    private int[] VolumeCustom = new int[4];

    private GameObject Bgm;

    public int SelectNum;
    private int SelectLog;
    private int CoolTime;
    public int Shift;

    private float Smoothing;
    void Start()
    {
        for(int i = 0;i < 3;i++)VolumeCustom[i] = PlayDataManager.VolumeList[i];
        VolumeCustom[3] = (PlayDataManager.CameraSensitive-100)/14;
        //BGMのオブジェクトを探す
        Bgm = GameObject.Find("BGM");
        for (int i = 0; i < 2; i++) BackScreen[i] = GameObject.Find("Scroll" + i);
        for (int i = 0; i < VolumeCustom.Length; i++)
        {
            Slider[i] = GameObject.Find("Slider" + i);
            Slider[i].GetComponent<RectTransform>().anchoredPosition = new Vector2(VolumeCustom[i] * 8 - 200, 150 - i * 120);
        }
        for (int i = 0; i < Select.Length; i++)
        {
            Select[i] = GameObject.Find("Select" + i);
            Select[i].GetComponent<Collider>().enabled = true;
        }
        Shift = 2;

        //音楽を鳴らす
        SoundManager.Instance.PlayBgmByName("BGM2", "Main");
    }

    //TimeScale = 0の環境でも操作できるようにする為全てupdateで行う
    void Update()
    {
        //操作があるか確認する為にクールタイムを設ける
        if (Input.GetAxisRaw("Vertical") == 0 && Input.GetAxisRaw("Horizontal") == 0 && Input.GetAxisRaw("Pad_D_V") == 0 && Input.GetAxisRaw("Pad_D_H") == 0 && Input.GetAxis("Mouse_ScrollWheel") == 0) CoolTime = 0;
        //メニューの動作
        switch (Shift)
        {
            case 0://変更する項目を選択する
                OptionSelect();
                break;
            case 1://音量を変更する
                SetVolume();
                break;
            case 2://シーンを開いた時の動き
                if (TimeSync()) if (SceneMove.Open()) Shift = 0;
                break;
            case 3://シーンを閉じるときの動き
                if (TimeSync()) if (SceneMove.Close2()) SceneManager.UnloadSceneAsync("Option");
                break;
        }
        //背景の動作TimeSyncで動かしたかったものの動作が重くなるため個別で動かします
        if ((ScrollX += Time.unscaledDeltaTime / Time.fixedUnscaledDeltaTime) >= 1920) ScrollX -= 1920;
        for (int i = 0; i < 2; i++) BackScreen[i].GetComponent<RectTransform>().anchoredPosition = new Vector2(1920 * i - ScrollX, 0);


    }

    //変更したい項目を選択する
    void OptionSelect()
    {
        

        if (SelectNum < 4)
        {
            //上下入力で選択した値の変更
            if ((Input.GetKeyDown(KeyCode.UpArrow) || Input.GetAxisRaw("Vertical") > 0 || Input.GetAxisRaw("Pad_D_V") > 0 || Input.GetAxis("Mouse_ScrollWheel") > 0) && CoolTime <= 0) SelectNum--;
            if ((Input.GetKeyDown(KeyCode.DownArrow) || Input.GetAxisRaw("Vertical") < 0 || Input.GetAxisRaw("Pad_D_V") < 0 || Input.GetAxis("Mouse_ScrollWheel") < 0) && CoolTime <= 0) SelectNum++;

            //上限を超えないようにする
            if (SelectNum > 4) SelectNum = 4;
            if (SelectNum < 0) SelectNum = 0;
            //Aボタンを押したら音量調節に移行する
            if (Input.GetKeyDown(KeyCode.KeypadEnter) ||Input.GetButtonDown("Mouse_Fire1") || Input.GetButtonDown("Pad_Fire1"))
            {
                //スライターのアウトラインの色を変更する
                Slider[SelectNum].GetComponent<Outline>().effectColor = new Color(145, 255, 100, 255) / 255;
                SoundManager.Instance.PlaySeByName("OptionSelect", gameObject);
                Shift = 1;
            }
            //B入力時反映せずに戻るへ移動する
            if (Input.GetButtonDown("Mouse_Jump") || Input.GetButtonDown("Pad_Jump")) SelectNum = 4;
        }
        if (SelectNum >= 4)
        {
            if (Input.GetKey(KeyCode.RightArrow) || 0 < Input.GetAxisRaw("Horizontal") || Input.GetAxisRaw("Pad_D_H") > 0 || Input.GetAxis("Mouse_ScrollWheel") < 0 && CoolTime == 0) SelectNum++;
            if (Input.GetKey(KeyCode.LeftArrow) || Input.GetAxisRaw("Horizontal") < 0 || Input.GetAxisRaw("Pad_D_H") < 0  || Input.GetAxis("Mouse_ScrollWheel") > 0 && CoolTime == 0) SelectNum--;

            if (SelectNum < 4 && Input.GetAxis("Mouse_ScrollWheel") == 0) SelectNum = 4;
            if (SelectNum > 5) SelectNum = 5;
            if ((Input.GetAxisRaw("Vertical") > 0 || Input.GetAxisRaw("Pad_D_V") > 0 ) && CoolTime == 0) SelectNum = 3;
            if (Input.GetKeyDown(KeyCode.KeypadEnter) ||Input.GetButtonDown("Mouse_Fire1") || Input.GetButtonDown("Pad_Fire1"))
            {
                switch (SelectNum)
                {
                    case 4:
                        SoundManager.Instance.PlaySeByName("OptionNG", gameObject);
                        //音量を変更する
                        PlayDataManager.BGM.GetComponent<SoundManager>().Volume = (float)PlayDataManager.VolumeList[0] / 100;
                        PlayDataManager.BGM.GetComponent<SoundManager>().BgmVolume = (float)PlayDataManager.VolumeList[1] / 100;
                        PlayDataManager.BGM.GetComponent<SoundManager>().SeVolume = (float)PlayDataManager.VolumeList[2] / 100;
                        break;
                    case 5:
                        SoundManager.Instance.PlaySeByName("OptionOK", gameObject);
                        for(int i = 0;i < 3;i++) PlayDataManager.VolumeList[i] = VolumeCustom[i];
                        PlayDataManager.CameraSensitive = VolumeCustom[3]*14+100;
                        PlayDataManager.DataSave();
                        break;
                }
                Shift = 3;
            }
        }
        if (SelectNum != SelectLog)
        {
            SelectLog = SelectNum;
            CoolTime = 20;
            SoundManager.Instance.PlaySeByName("OptionCursor", gameObject);
            //選択中の文字に色を付ける
            for (int i = 0; i < Select.Length; i++) Select[i].GetComponent<Outline>().enabled = false;
            Select[SelectNum].GetComponent<Outline>().enabled = true;
        } 
    }
    void SetVolume()
    {
        //変更前の値をとっておく
        int VolumeLog = VolumeCustom[SelectNum];
        //マウスホイールで音量を変更する
        if (Input.GetAxis("Mouse_ScrollWheel") > 0) VolumeCustom[SelectNum]++;
        if (Input.GetAxis("Mouse_ScrollWheel") < 0) VolumeCustom[SelectNum]--;
        //fixedupdateと同じような動作をさせたいところ
        if (TimeSync())
        {
            if (CoolTime-- < 0) CoolTime = 0;
            //左右入力で音量を変更する
            if (Input.GetKey(KeyCode.RightArrow) || Input.GetAxisRaw("Horizontal") > 0 || Input.GetAxisRaw("Pad_D_H") > 0) VolumeCustom[SelectNum]++;
            if (Input.GetKey(KeyCode.LeftArrow) || Input.GetAxisRaw("Horizontal") < 0 || Input.GetAxisRaw("Pad_D_H") < 0) VolumeCustom[SelectNum]--;
        }
        //音量が制限を超えないようにする    
        if (VolumeCustom[SelectNum] < 0) VolumeCustom[SelectNum] = 0;
        if (VolumeCustom[SelectNum] > 100) VolumeCustom[SelectNum] = 100;
        if (VolumeLog != VolumeCustom[SelectNum])
        {
            //音量を変更する
            PlayDataManager.BGM.GetComponent<SoundManager>().Volume = (float)VolumeCustom[0] / 100;
            PlayDataManager.BGM.GetComponent<SoundManager>().BgmVolume = (float)VolumeCustom[1] / 100;
            PlayDataManager.BGM.GetComponent<SoundManager>().SeVolume = (float)VolumeCustom[2] / 100;
            //一定間隔で効果音を鳴らす
            if (VolumeCustom[SelectNum] % 5 == 4) SoundManager.Instance.PlaySeByName("VolumeTs", gameObject);
            //音量に応じてスライダーを移動させる
            Slider[SelectNum].GetComponent<RectTransform>().anchoredPosition = new Vector2(VolumeCustom[SelectNum] * 8 - 200, 150 - SelectNum * 120);
        }
        //AかBボタンで音量を決定する
        if (Input.GetKeyDown(KeyCode.KeypadEnter) || Input.GetButtonDown("Mouse_Fire1") || Input.GetButtonDown("Pad_Fire1"))
        {
            //アウトラインの色を戻す
            Slider[SelectNum].GetComponent<Outline>().effectColor = new Color(137, 137, 137, 255) / 255;
            SoundManager.Instance.PlaySeByName("OptionSelect", gameObject);
            Shift = 0;
        }
    }
    //fixedupdateでもtimescale = 0のupdateでも同じ動きが出来るようにする
    bool TimeSync()
    {
        Smoothing += Time.unscaledDeltaTime;
        if (Smoothing >= Time.fixedUnscaledDeltaTime)
        {
            Smoothing = 0;
            return true;
        }
        return false;
    }
}
