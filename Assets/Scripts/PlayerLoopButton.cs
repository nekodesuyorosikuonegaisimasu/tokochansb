﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLoopButton : MonoBehaviour
{
    // ループオブジェクトを取得
    [SerializeField]
    private GameObject[] LoopObj;
    // ループ演出オブジェクトを取得
    [SerializeField]
    private GameObject LoopEffectObj;
    // R2を押したクールタイム
    private float R2CoolTime;
    // ループ可能不可能のフラグ
    public static bool IsLoop;
    // ループUI
    [SerializeField]
    private GameObject LoopUI;

    // Start is called before the first frame update
    void Start()
    {
        LoopObj = GameObject.FindGameObjectsWithTag("LoopObject");
        // R2を押したフラグの初期化
        IsLoop = true;
        R2CoolTime = 0;
        LoopUI.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        // ループ演出中に演出をスキップ
        if ((Input.anyKeyDown || LoopEffectManage.EndFlag) && LoopEffectManage.LoopEffectFlag)
        {
            LoopEffectObj.GetComponent<LoopEffectManage>().LoopSkip();
            IsLoop = true;
        }

        // ポーズ中は操作を受け付けない
        if (Mathf.Approximately(Time.timeScale, 0f)) return;

        // ループ可能な時にR2を押したら
        if ((Input.GetKeyDown(KeyCode.RightShift)/*デバッグ用*/|| Input.GetAxis("Pad_R2") > 0) && !LoopEffectManage.LoopEffectFlag && IsLoop)
        {
            IsLoop = false;
            for (int i = 0; i < LoopObj.Length; i++)
            {
                if (LoopObj[i] != null)
                {
                    // ループ時のオブジェクト変更処理
                    LoopObj[i].GetComponent<LoopObj>().PushButton();
                }
            }
            R2CoolTime += 1;
            // ループ演出
            LoopEffectObj.GetComponent<LoopEffectManage>().LoopStart();
        }

        // ループクールタイム処理
        if (R2CoolTime != 0)
        {
            if (!LoopUI.activeSelf) LoopUI.SetActive(true);
            LoopUI.GetComponent<Animator>().SetBool("Loop", true);
            R2CoolTime = 0;
        }
    }
}
