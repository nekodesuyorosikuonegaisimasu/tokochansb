﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigDollManager : MonoBehaviour
{
    private int Timer;

    public GameObject Body;
    public GameObject Bomb;

    // Start is called before the first frame update
    void Start()
    {
        Timer = 1000;
    }

    // Update is called once per frame
    void Update()
    {
        if (Timer == 120)
        {
            Bomb.SetActive(true);
            Bomb.transform.position = new Vector3(transform.position.x, transform.position.y + 3, transform.position.z);
        }

        if (Timer == 70)
        {
            SoundManager.Instance.PlaySeByName("BigDoll",gameObject);
            Bomb.GetComponent<Collider>().enabled = true;
            Body.SetActive(false);
        }
        if (Timer <= 0) Destroy(this.gameObject);
    }
    void FixedUpdate()
    {
        Timer--;
    }
    public void OnTriggerEnter(Collider collider)
    {

        if ((collider.gameObject.CompareTag("Enemy") || collider.gameObject.CompareTag("DollSkillWeapon") || collider.gameObject.CompareTag("Gimmick")) && Timer > 120)
        {
            Timer = 120;
        }

    }
}
