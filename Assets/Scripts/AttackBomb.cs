﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackBomb : MonoBehaviour
{
    // プレイヤーを探す
    private GameObject player;
    // プレイヤー6の召喚物を探す
    private GameObject summonPlayer;
    // 爆発までのカウント
    [SerializeField]
    private float destroyTime;
    // カウント
    private float count;

    // Start is called before the first frame update
    void Start()
    {
        // プレイヤーを探す
        player = GameObject.Find("Player" + PlayDataManager.CharaNum);
        // プレイヤー6なら
        if(PlayDataManager.CharaNum == 6)
        {
            // 召喚物5を探す
            summonPlayer = GameObject.Find("Player6-5");
        }
        count = 0;
    }

    // Update is called once per frame
    void Update()
    {
        count += Time.deltaTime;

        if(count >= destroyTime)
        {
            Destroy(this.gameObject);
        }
    }
}
