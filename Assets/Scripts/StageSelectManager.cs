﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StageSelectManager : MonoBehaviour
{
    [SerializeField]
    private GameObject StageEnter, StageName, StageCancel; // ステージの入り方・ステージの名前表示・ステージキャンセルの文字表示
    //public GameObject StageName;//ステージの名前を表示する
    //public GameObject Stagecancel; // ステージキャンセルの文字表示
    private string[] WorldNameList;//ステージの名前のリスト
    private GameObject[] WorldList = new GameObject[PlayDataManager.WorldMax]; // ワールドをまとめて管理する
    public int WorldSelectNum;//選択中のワールドの値
    private int WorldSelectNumLog;//選択中のワールドのログ
    private GameObject[] StageList = new GameObject[PlayDataManager.StageMax]; // ステージをまとめて管理する
    public int StageSelectNum;//選択中のステージの値
    private int StageSelectNumLog;//選択中のワールドのログ
    public bool IsSelectedWorld;// ワールドを選んだか判定
    private GameObject[] WorldStageList = new GameObject[PlayDataManager.WorldMax]; // ワールド選択後のステージ背景

    //タイトル画面移行用
    public GameObject BackHint;//タイトル画面に戻るボタンを表示しておく
    public GameObject BackCanvas;//タイトル画面に戻るか確認する画像
    public GameObject[] SelectButton = new GameObject[2];//選択肢を入れる 
    public int BackSelect;//選択中の項目の値取得用
    private bool BackFlag;//ボタンを押したときのフラグ取得用

    //姫と騎士関連
    public GameObject Knight;//騎士
    private float KnightSpeed;//騎士の移動速度
    public float X;//騎士を周回させるのに使用
    public GameObject Princess;//姫
    private Vector3 PrincessPos;//姫の移動履歴
    private int FootTimer = 90;//足音を一定間隔で鳴らすためのタイマー
    private float PrincessAngleReal;//滑らかに方向転換させるために使用
    private float Speed = 0.1f;//プレイヤーの移動速度
    private float PrincessAngle;//騎士の向きたい方向計算用

    // ステージ選択関連
    private int IconTimer; // 連続して動かせないようにするタイマー
    [SerializeField]
    public GameObject PrincessIcon; // 姫のアイコン
    private Color PrincessIconColor; //姫のアイコン点滅用
    private float IconColorSpeed; //姫のアイコンの点滅速度
    private float IconMoveCount; // ステージセレクトのUI表示のカウント

    public Animator KnightAnimator;//騎士のアニメーション
    public GameObject Camera;//カメラ

    private int StoryShift;//ストーリー再生の段階
    public GameObject StoryText;//ストーリー表示用
    public GameObject Clouds;//雲を纏めて管理する
    private GameObject[] Cloud = new GameObject[12];//雲
    [SerializeField]
    private GameObject StorySkipText; // ストーリースキップテキスト

    public int Shift;//ステージ選択のモード管理


    enum ShiftDef
    {
        Shift_Non = 0,
    };
    //MenuCharactorのスタート時の値がおかしくなるため、先に姫と騎士の座標を変更しておく
    void Awake()
    {
        //最後に選択したワールドを最初に選択するようにする
        WorldSelectNum = PlayDataManager.WorldStageNum;
        WorldSelectNumLog = WorldSelectNum;
        // 最後に選択したステージを最初に選択するようにする
        StageSelectNum = 1 + (WorldSelectNum - 1) * 3;
        StageSelectNumLog = StageSelectNum;

        //ステージリストに各ステージオブジェクトを登録する
        for (int i = 0; i < PlayDataManager.StageMax; i++)
        {
            StageList[i] = GameObject.Find("Stage" + (i + 1));
            //一度全てのステージを非表示にする(登録する際表示しておく必要がある為)
            //StageList[i].SetActive(false);
        }
        for (int i = 0; i < PlayDataManager.WorldMax; i++)
        {
            WorldList[i] = GameObject.Find("World" + (i + 1));
            WorldList[i].GetComponentInChildren<Renderer>().material.color = Color.red;
            //一度全てのステージを非表示にする(登録する際表示しておく必要がある為)
            WorldList[i].SetActive(false);

            WorldStageList[i] = GameObject.Find("WorldStage" + (i + 1));
            WorldStageList[i].SetActive(false);
        }
        GetComponent<LineRenderer>().positionCount = PlayDataManager.OpenWorldStage;
        //解放済みのステージを表示し、線で結ぶ
        for (int i = 0; i < PlayDataManager.OpenWorldStage; i++)
        {
            WorldList[i].SetActive(true);
            GetComponent<LineRenderer>().SetPosition(i, WorldList[i].transform.position);
        }
        //クリア済みのステージを緑色にする
        for (int i = 0; i < PlayDataManager.ClearStage / 3; i++) WorldList[i].GetComponentInChildren<Renderer>().material.color = Color.green;
        //解放済みでクリアしていないステージを赤くする
        if (PlayDataManager.OpenWorldStage != PlayDataManager.ClearStage) for (int i = PlayDataManager.ClearStage; i < PlayDataManager.OpenWorldStage; i++)
                WorldList[i].GetComponentInChildren<Renderer>().material.color = Color.red;
        //最後に選択したステージの位置に姫を移動
        Princess.transform.position = WorldList[WorldSelectNum - 1].transform.position;
        //ついでに姫の向きを整える
        PrincessAngle = Princess.transform.eulerAngles.y + 90;
        PrincessAngleReal = Princess.transform.eulerAngles.y + 90;
        //騎士を姫の隣に配置する
        Knight.transform.position = new Vector3(Princess.transform.position.x - 1.5f * Mathf.Cos(X / 75), Princess.transform.position.y, Princess.transform.position.z - 1.5f * Mathf.Sin(X / 75));
        // ステージ1～3を選んでいない状態にする
        IsSelectedWorld = false;
        // プリセスアイコンを非表示にする
        PrincessIcon.SetActive(false);
        //姫のアイコンのイメージカラーを保存
        PrincessIconColor = PrincessIcon.GetComponent<Image>().color;
        //空にしておく
        IconColorSpeed = 0;
    }

    // Start is called before the first frame update
    void Start()
    {
        //ステージの名前を取得して配列に変換する
        string LoadStr = (Resources.Load("StageName", typeof(TextAsset)) as TextAsset).text;
        //文字列を","ごとに分別された配列に変換
        WorldNameList = LoadStr.Split(char.Parse("\n"));
        //タイトルへ戻る用のボタン取得
        for (int i = 0; i < 2; i++) SelectButton[i] = GameObject.Find("SelectButton" + i);
        BackCanvas.SetActive(false);
        //雲を見つける
        for (int i = 0; i < 12; i++) Cloud[i] = GameObject.Find("Cloud" + i);
        //ステージの名前、大きさを調整する
        WorldStatus();
        //スクリーンを開く動作を最初に行う
        Shift = 4;
        //初回起動時
        if (PlayDataManager.OpenWorldStage == 0)
        {
            StageName.SetActive(false);
            StageEnter.SetActive(false);
            StageCancel.SetActive(false);
            BackHint.SetActive(false);
            StoryText.GetComponent<Text>().text = (Resources.Load("Story") as TextAsset).text;
            Camera.transform.position = new Vector3(0, 50, 0);
            Camera.transform.rotation = Quaternion.Euler(0, 0, 0);
            StoryShift = 0;
        }
        //その他
        else
        {
            StoryText.SetActive(false);
            StorySkipText.SetActive(false);
            Clouds.SetActive(false);
            Camera.transform.position = new Vector3(0, 12, 0);
            Camera.transform.rotation = Quaternion.Euler(90, 0, 0);
        }
        //音楽を流す
        SoundManager.Instance.PlayBgmByName("StageSelectBGM", "Main");
        SoundManager.Instance.PlayBgmByName("StageSelectBird", "Sub");
    }
    //操作関連のアップデート
    void Update()
    {
        if (ControllerChecker.CheckFlag == true)
        {
            switch (Shift)
            {
                case 1:// ワールド・ステージの選択
                    Stage();
                    break;
                case 7://タイトル画面に戻る選択
                    TitleBack();
                    break;
            }
        }
    }
    //表示関連のアップデート
    void FixedUpdate()
    {
        // ワールド選択
        if (!IsSelectedWorld)
        {
            //騎士を動かす
            KnightMove();

            //もくじ
            switch (Shift)
            {
                case 0:// 情報の更新＆取得
                    WorldStatus();
                    Shift = 1;
                    break;
                case 1:// 姫の向きの調整              
                    PrincessRotate();
                    break;
                case 2: // 姫の移動
                    PrincessMove();
                    break;
                case 3:// ステージの解放
                    WorldOpen();
                    break;
                case 4: // シーン移動してきたときの動作
                    SceneOpen();
                    break;
                case 5:// シーン移動するときの動作
                    NextScene();
                    break;
                case 6:// タイトルへ戻る用の情報取得
                    TitleBackStatus();
                    break;
                case 7:// タイトル画面へ戻るときの動作
                    TitleBackFix();
                    break;
                case 8:// 初回起動時のストーリー閲覧用
                    Story();
                    break;
            }
        }
        // ワールド選択後のステージ選択
        else
        {
            // もくじ
            switch (Shift)
            {
                case 0: // 情報の更新・取得
                    StageStatus();
                    Shift = 1;
                    break;
                case 2: // アイコンの移動
                    IconMove();
                    break;
                case 3: // ステージの解放
                    StageOpen();
                    break;
                case 4: // ステージ表示
                    SceneOpen();
                    break;
            }

            //姫のアイコンを点滅させる
            IconVisual();
        }
    }
    //ステージに関する情報を取得
    void WorldStatus()
    {
        // ステージ名表示
        StageName.SetActive(true);
        //ステージ名を取得
        StageName.GetComponent<Text>().text = "ワールド " + WorldSelectNum + " \n" + WorldNameList[WorldSelectNum - 1];
        // ゲーム開始テキスト表示
        StageEnter.SetActive(true);
        StageEnter.GetComponentInChildren<Text>().text = "　ステージ選択";
        // キャンセルテキスト表示
        StageCancel.SetActive(true);
        StageCancel.GetComponentInChildren<Text>().text = "　タイトル画面に戻る";
        //全ての項目の大きさを元に戻す
        for (int i = 0; i < PlayDataManager.WorldMax; i++)
            WorldList[i].transform.localScale = new Vector3(1, 0.1f, 1);
        //選択中の項目のみ大きくする
        WorldList[WorldSelectNum - 1].transform.localScale = new Vector3(1.3f, 0.1f, 1.3f);
    }
    void StageStatus()
    {
        // ステージ名表示
        StageName.SetActive(true);
        // ステージ名を取得
        StageName.GetComponent<Text>().text = "ステージ" + WorldSelectNum + " - " + (StageSelectNum - (3 * (WorldSelectNum - 1)));
        // ゲーム開始テキスト表示
        StageEnter.SetActive(true);
        StageEnter.GetComponentInChildren<Text>().text = "　　ゲーム開始";
        // キャンセルテキスト表示
        StageCancel.SetActive(true);
        StageCancel.GetComponentInChildren<Text>().text = "　ワールド選択に戻る";
        // 全ての項目の大きさを元に戻す
        for (int i = 0; i < PlayDataManager.WorldMax; i++)
            WorldList[i].transform.localScale = new Vector3(1, 0.1f, 1);
        //選択中の項目のみ大きくする
        WorldList[WorldSelectNum - 1].transform.localScale = new Vector3(1.3f, 0.1f, 1.3f);
    }
    // ワールド・ステージの選択、入るときの動作
    void Stage()
    {
        // ワールドを選んでいないなら
        if (!IsSelectedWorld)
        {
            //選択しているステージのログを録る
            //左右キーでワールド選択
            if (Input.GetKey(KeyCode.RightArrow) || Input.GetAxisRaw("Horizontal") > 0 || Input.GetAxisRaw("Pad_D_H") > 0 || Input.GetAxis("Mouse_ScrollWheel") < 0)
            {
                WorldSelectNum++;
                StageSelectNum += 3;
            }
            if (Input.GetKey(KeyCode.LeftArrow) || Input.GetAxisRaw("Horizontal") < 0 || Input.GetAxisRaw("Pad_D_H") < 0 || Input.GetAxis("Mouse_ScrollWheel") > 0)
            {
                WorldSelectNum--;
                StageSelectNum -= 3;
            }
            //ワールド１より下には行けないようにする
            if (WorldSelectNum <= 0)
            {
                WorldSelectNum = 1;
                StageSelectNum += 3;
            }
            //クリアしていないワールドより先には進めないように
            if (WorldSelectNum > PlayDataManager.OpenWorldStage)
            {
                WorldSelectNum = PlayDataManager.OpenWorldStage;
                StageSelectNum -= 3;
            }
            // 戻る入力時タイトルへ戻る
            if (Input.GetButtonDown("Pad_Jump") || Input.GetButtonDown("Mouse_StartButton"))
            {
                SoundManager.Instance.PlaySeByName("PaperOpen", gameObject);
                Shift = 6;
            }
            // 別なステージを選択していた場合騎士を移動させる
            if (WorldSelectNum != WorldSelectNumLog)
            {
                // テキストの表示を消す
                StageEnter.SetActive(false);
                StageName.SetActive(false);
                // 全ての項目の大きさを元に戻す
                for (int i = 0; i < PlayDataManager.WorldMax; i++)
                    WorldList[i].transform.localScale = new Vector3(1, 0.1f, 1);
                Shift = 2;
            }
            // エンター入力時（ワールド選択時）ステージ選択処理へ
            if ((Input.GetKeyDown(KeyCode.KeypadEnter) || Input.GetButtonDown("Mouse_Fire1") || Input.GetButtonDown("Pad_Fire1")))
            {
                if (WorldSelectNum == WorldSelectNumLog)
                {
                    // ステージ選択処理へ移行
                    SoundManager.Instance.PlaySeByName("PaperOpen", gameObject);
                    IsSelectedWorld = true;
                    // ステージセレクト表示
                    Shift = 4;
                }
            }
        }
        // ワールドを選んでいるならステージ選択処理
        else
        {
            StageSelectNumLog = StageSelectNum;

            //左右キーでステージ選択
            if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetAxisRaw("Horizontal") > 0 || Input.GetAxisRaw("Pad_D_H") > 0 || Input.GetAxis("Mouse_ScrollWheel") < 0)
            {
                // 値を足してアイコン移動処理へ
                StageSelectNum++;
                SoundManager.Instance.PlaySeByName("StageSelectKey", gameObject);
                if (StageSelectNum <= PlayDataManager.OpenStage) PrincessIcon.transform.localPosition += new Vector3(1200, 0, 0);
                Shift = 2;
            }
            if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetAxisRaw("Horizontal") < 0 || Input.GetAxisRaw("Pad_D_H") < 0 || Input.GetAxis("Mouse_ScrollWheel") > 0)
            {
                // 値を引いてアイコン移動処理へ
                StageSelectNum--;
                SoundManager.Instance.PlaySeByName("StageSelectKey", gameObject);
                PrincessIcon.transform.localPosition += new Vector3(-1200, 0, 0);
                Shift = 2;
            }
            //クリアしていないステージより先には進めないように
            if (StageSelectNum > PlayDataManager.OpenStage) StageSelectNum = PlayDataManager.OpenStage;

            // ステージ〇-1～〇-3より上下に数値がいかないようにする
            if (StageSelectNum <= 3 * (WorldSelectNum - 1))
            {
                StageSelectNum = (3 * (WorldSelectNum - 1)) + 1;
                PrincessIcon.transform.localPosition += new Vector3(1200, 0, 0);
            }
            if (StageSelectNum >= 3 * WorldSelectNum + 1)
            {
                StageSelectNum = 3 * WorldSelectNum;
                PrincessIcon.transform.localPosition += new Vector3(-1200, 0, 0);
            }

            // 戻るボタン入力時タイトルへ戻る
            if (Input.GetButtonDown("Pad_Jump") || Input.GetButtonDown("Mouse_StartButton"))
            {
                SoundManager.Instance.PlaySeByName("PaperOpen", gameObject);
                // ステージ番号をリセット(各ワールド-1に設定する)
                StageSelectNum = 1 + (3 * (WorldSelectNum - 1));
                StageSelectNumLog = 1 + (3 * (WorldSelectNumLog - 1));
                // ステージを選んでいない状態に戻す
                IsSelectedWorld = false;
                // ステージセレクトの背景を非表示にする
                WorldStageList[WorldSelectNum - 1].SetActive(false);

                // 姫のアイコンを表示前の状態に戻す
                PrincessIcon.SetActive(false);
                IconTimer = 0;

                Shift = 0;
            }

            //エンター入力時次のシーンへ移動する準備を行う
            if ((Input.GetKeyDown(KeyCode.KeypadEnter) || Input.GetButtonDown("Mouse_Fire1") || Input.GetButtonDown("Pad_Fire1")))
            {
                if (StageSelectNum == StageSelectNumLog)
                {
                    SoundManager.Instance.PlaySeByName("StageSelectOK", gameObject);
                    IsSelectedWorld = false;
                    Shift = 5;
                }
            }
        }
    }
    // 姫をステージマスまで移動させる
    void PrincessMove()
    {
        // 一定間隔で足音を鳴らす
        if (FootTimer++ > 80)
        {
            SoundManager.Instance.PlaySeByName("footsteps4", gameObject);
            FootTimer = 0;
        }
        // 姫を一定の速度でゴールまで移動させる
        Princess.transform.position = Vector3.MoveTowards(Princess.transform.position, WorldList[WorldSelectNumLog - 1].transform.position, Speed);

        // 姫が移動していた場合向きを変更する
        if (Princess.transform.position.x != PrincessPos.x || Princess.transform.position.z != PrincessPos.z)
        {
            // 姫の移動後と移動前の座標から角度を算出
            PrincessAngle = Mathf.Atan2(Princess.transform.position.z - PrincessPos.z, Princess.transform.position.x - PrincessPos.x) * Mathf.Rad2Deg;
            // 振り向く角度が180度を上回っていた場合
            if (PrincessAngle - PrincessAngleReal > 180) PrincessAngleReal = 360 + PrincessAngleReal;
            if (PrincessAngle - PrincessAngleReal < -180) PrincessAngleReal = -360 + PrincessAngleReal;
            // 滑らかに方向転換できる関数を用意
            PrincessAngleReal += (PrincessAngle - PrincessAngleReal) / 15;
            // 向きを更新する
            Princess.transform.rotation = Quaternion.Euler(0, -PrincessAngleReal + 90.0f, 0);
            // プレイヤーの座標を更新
            PrincessPos = Princess.transform.position;
        }
        // 姫がマスまで移動し終えたら次のマスに向けて移動するようにする
        if (PrincessPos == WorldList[WorldSelectNumLog - 1].transform.position)
        {
            if (WorldSelectNum - WorldSelectNumLog < 0) WorldSelectNumLog--;
            if (WorldSelectNum - WorldSelectNumLog > 0) WorldSelectNumLog++;
        }
        // 目標地点まで移動していたらステージ選択に戻る
        if (PrincessPos == WorldList[WorldSelectNum - 1].transform.position)
        {
            FootTimer = 80;
            SoundManager.Instance.StopSe();
            Shift = 0;
        }

    }
    //ステージマスにいるときの姫
    void PrincessRotate()
    {
        //姫に正面を向かせる
        PrincessAngle = -90;
        //振り向く角度が180度を上回っていた場合
        if (PrincessAngle - PrincessAngleReal > 180) PrincessAngleReal = 360 + PrincessAngleReal;
        if (PrincessAngle - PrincessAngleReal < -180) PrincessAngleReal = -360 + PrincessAngleReal;
        //滑らかに方向転換できる関数を用意
        if (PrincessAngle - PrincessAngleReal > 0) PrincessAngleReal += 3;
        if (PrincessAngle - PrincessAngleReal <= 0) PrincessAngleReal -= 3;
        Princess.transform.rotation = Quaternion.Euler(0, -PrincessAngleReal + 90.0f, 0);
        //向いている方向が正面に近くなったら正面で固定
        if ((-PrincessAngleReal + 90) > 178.5f && (-PrincessAngleReal + 90) <= 181.5f)
        {
            Princess.transform.rotation = Quaternion.Euler(0, 180, 0);
        }
    }
    //騎士の動き
    void KnightMove()
    {
        //ステージ解放中。姫の移動中は騎士を周回させない
        if (Shift == 1 || Shift == 5 || Shift == 7) X++;
        //姫が移動していない時は歩かせる
        if (Shift != 2) KnightAnimator.GetComponent<Animator>().SetBool("Run", false);
        //姫の移動中は走って追いかける
        if (Shift == 2) KnightAnimator.GetComponent<Animator>().SetBool("Run", true);
        //ステージ解放中は座標の変更なし　姫を中心に円周上に歩く
        if (Shift != 3 && Shift != 8) Knight.transform.position = new Vector3(Princess.transform.position.x - 1.5f * Mathf.Cos(X / 75), Princess.transform.position.y, Princess.transform.position.z - 1.5f * Mathf.Sin(X / 75));
    }
    // アイコンの動き
    void IconMove()
    {
        if (IconTimer == 0)
        {
            // ステージセレクトを開いたときのみアイコン表示・位置を〇-1の場所に設定
            PrincessIcon.SetActive(true);
            PrincessIcon.transform.localPosition = new Vector3(-1352, 466, 2474);
        }
        if (IconTimer++ > 10)
        {
            IconTimer = 1;
            Shift = 0;
        }
    }

    //アイコンの点滅
    void IconVisual()
    {
        //姫のアイコンのイメージカラーを保存
        PrincessIconColor = PrincessIcon.GetComponent<Image>().color;
        //スピード加算
        IconColorSpeed += Time.deltaTime * 1.4f;
        //透明度を変える
        PrincessIconColor.a = Mathf.Sin(IconColorSpeed);
        //マイナスはなし
        if (PrincessIconColor.a < 0) PrincessIconColor.a *= -1;
        //色の更新
        PrincessIcon.GetComponent<Image>().color = PrincessIconColor;
    }

    // ステージの開放
    void StageOpen()
    {
        IconMoveCount++;

        //一定時間後カメラの位置を戻し、新しいワールドマスを徐々に大きくしながら表示
        if (IconMoveCount > 50)
        {
            StageList[PlayDataManager.OpenStage].GetComponentInChildren<Image>().color = new Color((IconMoveCount - 50) / 100, (IconMoveCount - 50) / 100, (IconMoveCount - 50) / 100, 255);

            // 黒塗りが色付きに変わり切ったら
            if (IconMoveCount > 150)
            {
                // 開放済みステージを更新してセーブ
                IconMoveCount = 0;
                PlayDataManager.OpenStage += 1;
                PlayDataManager.DataSave();
                Shift = 2;
            }
        }
    }
    // ワールドの開放
    void WorldOpen()
    {
        //カメラの位置を固定、解放するワールドのオブジェクトを表示する
        if (X == 50)
        {
            WorldList[PlayDataManager.OpenWorldStage].SetActive(true);
            WorldList[PlayDataManager.OpenWorldStage].GetComponentInChildren<Image>().color = new Color(1, 1, 1, 0);
            Camera.transform.position = new Vector3(0, 12, 0);
            Camera.transform.rotation = Quaternion.Euler(90, 0, 0);
        }

        X++;
        //一定時間が経過するまでカメラを揺らす
        if (X < 50) Camera.transform.position = new Vector3(Random.Range(-0.30f, 0.30f), 12, Random.Range(-0.30f, 0.30f));
        //一定時間後カメラの位置を戻し、新しいワールドマスを徐々に大きくしながら表示
        if (X > 50)
        {

            WorldList[PlayDataManager.OpenWorldStage].transform.localScale = new Vector3((X - 50) / 100, 0.1f, (X - 50) / 100);
            //GetComponent<LineRenderer>().endColor = new Color(1, 1, 1, X/150);
            WorldList[PlayDataManager.OpenWorldStage].GetComponentInChildren<Image>().color = new Color(1, 1, 1, (X - 50) / 100);
            //マスが大きくなりきったら色を変更、解放済みワールドを更新しデータセーブ
            if (X > 150)
            {
                X = 0;
                Shift = 0;
                WorldList[PlayDataManager.OpenWorldStage].transform.localScale = new Vector3(1, 1, 1);
                //ステージ同士を線でつなぐ
                GetComponent<LineRenderer>().positionCount = PlayDataManager.OpenWorldStage + 1;
                GetComponent<LineRenderer>().SetPosition(PlayDataManager.OpenWorldStage, new Vector3(WorldList[PlayDataManager.OpenWorldStage].transform.position.x, 0, WorldList[PlayDataManager.OpenWorldStage].transform.position.z));
                PlayDataManager.OpenWorldStage += 1;
                PlayDataManager.DataSave();
                StageName.SetActive(true);
                StageEnter.SetActive(true);
                BackHint.SetActive(true);
                //騎士に歩かせる
                KnightAnimator.GetComponent<Animator>().SetBool("Walk", true);
            }
        }
    }
    //ステージセレクトに移動してきたときの動作
    void SceneOpen()
    {
        // ワールドを選んでいないなら
        if (!IsSelectedWorld)
        {
            if (SceneMove.Open())
            {
                //初回起動時はストーリー再生
                if (PlayDataManager.OpenWorldStage == 0) Shift = 8;
                //解放済みステージとクリア済みステージの値が一致していた場合新しいステージを解放(&&に入れる値は最大ステージ数)
                else if (PlayDataManager.ClearStage == PlayDataManager.OpenStage && PlayDataManager.ClearStage / 3 != PlayDataManager.WorldMax)
                {
                    // クリアステージが〇-3なら
                    if (PlayDataManager.ClearStage == 3 || PlayDataManager.ClearStage == 6) Shift = 3;
                    else Shift = 0;
                }
                else
                {
                    //解放済みでクリアしていないステージを赤くする
                    if (PlayDataManager.OpenWorldStage != PlayDataManager.ClearStage) for (int i = PlayDataManager.ClearStage / 3; i < PlayDataManager.OpenWorldStage; i++)
                            WorldList[i].GetComponentInChildren<Renderer>().material.color = Color.red;
                    //騎士に歩かせる
                    KnightAnimator.GetComponent<Animator>().SetBool("Walk", true);
                    Shift = 0;
                }
            }
        }
        // ワールドを選んでいるなら
        else
        {
            // Xが50以上の時に一回だけステージセレクトの背景オブジェクトをアクティブにする処理
            if (IconMoveCount > 50 && !WorldStageList[WorldSelectNum - 1].gameObject.activeSelf)
            {
                // ステージセレクトの背景を表示する
                WorldStageList[WorldSelectNum - 1].SetActive(true);
                // ステージの設定
                for (int i = 0; i < 3; i++)
                {
                    // 開放していないステージなら
                    if (i + 3 * (WorldSelectNum - 1) >= PlayDataManager.OpenStage)
                    {
                        // ステージの画像を黒塗りにする
                        StageList[i + 3 * (WorldSelectNum - 1)].GetComponentInChildren<Image>().color = new Color(0, 0, 0, 255);
                    }
                }
            }

            IconMoveCount++;

            // 新しいワールドマスを徐々に大きくしながら表示
            if (IconMoveCount > 50)
            {
                // 徐々に大きくする処理
                WorldStageList[WorldSelectNum - 1].transform.localScale = new Vector3((IconMoveCount - 50) / 100, 0.1f, (IconMoveCount - 50) / 100);

                // 大きくなりきったら固定
                if (IconMoveCount > 180)
                {
                    IconMoveCount = 0;
                    // それ以上大きくならないように大きさを固定
                    WorldStageList[WorldSelectNum - 1].transform.localScale = new Vector3(1, 1, 1);
                    // 新しいステージがあるならステージを開く処理へ
                    if (PlayDataManager.ClearStage == PlayDataManager.OpenStage && PlayDataManager.ClearStage != PlayDataManager.StageMax) Shift = 3;
                    else Shift = 2;
                }
            }
        }
    }
    // 次のシーンへ移動する
    void NextScene()
    {
        // 画面が閉じきったら選択中のステージ情報を記録し、ステージへ
        if (SceneMove.Close())
        {
            PlayDataManager.WorldStageNum = WorldSelectNum;
            PlayDataManager.StageNum = StageSelectNum;
            SceneManager.LoadScene("Stage" + WorldSelectNum);
        }
    }
    //タイトル画面へ戻る用のウィンドウの情報を取得
    void TitleBackStatus()
    {
        if(BackSelect != 0 && BackSelect != 1)
        {
            Shift = 7;
            return;
        }

        BackHint.SetActive(false);
        BackCanvas.SetActive(true);
        for (int i = 0; i < 2; i++)
        {
            SelectButton[i].transform.localScale = new Vector2(0.7f, 0.5f);
            SelectButton[i].GetComponent<Outline>().enabled = false;
        }
        SelectButton[BackSelect].transform.localScale = new Vector2(0.91f, 0.65f);
        SelectButton[BackSelect].GetComponent<Outline>().enabled = true;
        Shift = 7;
    }
    //タイトル画面へ戻る動作
    void TitleBack()
    {
        if (Input.GetKeyDown(KeyCode.KeypadEnter) || Input.GetButtonDown("Mouse_Fire1") || Input.GetButtonDown("Pad_Fire1")) BackFlag = true;

        //シーンが閉じるまで操作できてしまっていた為固定するように
        if (BackFlag == false)
        {
            int BackSelectLog = BackSelect;
            //左右キーで選択
            if (Input.GetKey(KeyCode.RightArrow) || Input.GetAxisRaw("Horizontal") > 0 || Input.GetAxisRaw("Pad_D_H") > 0 || Input.GetAxis("Mouse_ScrollWheel") < 0 && BackSelect == 0) BackSelect++;
            if (Input.GetKey(KeyCode.LeftArrow) || Input.GetAxisRaw("Horizontal") < 0 || Input.GetAxisRaw("Pad_D_H") < 0 || Input.GetAxis("Mouse_ScrollWheel") > 0 && BackSelect == 1) BackSelect--;

            //0か１以外にはならないようにする
            if (BackSelect < 0) BackSelect = 0;
            if (BackSelect > 1) BackSelect = 1;
            if (BackSelectLog != BackSelect)
            {
                SoundManager.Instance.PlaySeByName("PaperSelect", gameObject);
                Shift = 6;
            }

        }

    }
    //タイトル画面に戻るときの表示関連
    void TitleBackFix()
    {
        if (BackFlag == true)
        {
            switch (BackSelect)
            {
                case 0://いいえを押したらウィンドウを閉じる
                    BackFlag = false;
                    BackCanvas.SetActive(false);
                    BackHint.SetActive(true);
                    //効果音を鳴らす
                    SoundManager.Instance.PlaySeByName("PaperClose", gameObject);
                    Shift = 1;
                    break;
                case 1://はいを押したらタイトルへ戻る
                    if (SceneMove.Close2())
                    {
                        BackSelect = 0;
                        BackFlag = false;
                        SceneManager.LoadScene("Title");
                    }
                    break;
            }
        }
    }
    //ストーリーを表示
    void Story()
    {
        if (StoryShift == 0)
        {
            X += 3;
            if (Input.anyKey) X += 15;
            StoryText.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, -StoryText.GetComponent<RectTransform>().sizeDelta.y / 2 + X);
            if (X >= StoryText.GetComponent<RectTransform>().sizeDelta.y + Screen.height / 2)
            {
                X = 0;
                StoryShift = 1;
            }
        }
        if (StoryShift == 1)
        {
            X++;
            Camera.transform.rotation = Quaternion.Euler(X / 2, 0, 0);
            if (X >= 180)
            {
                X = 0;
                Camera.transform.rotation = Quaternion.Euler(90, 0, 0);
                StorySkipText.SetActive(false);
                StoryShift = 2;
            }
        }
        if (StoryShift == 2)
        {

            X++;
            Camera.transform.position = new Vector3(0, 50 - X / 4, 0);
            if (X >= 20)
            {
                for (int i = 0; i < 12; i += 4) Cloud[i].GetComponent<RectTransform>().anchoredPosition = new Vector2(Cloud[i].GetComponent<RectTransform>().anchoredPosition.x + X / 3, Cloud[i].GetComponent<RectTransform>().anchoredPosition.y + X / 2);
                for (int i = 1; i < 12; i += 4) Cloud[i].GetComponent<RectTransform>().anchoredPosition = new Vector2(Cloud[i].GetComponent<RectTransform>().anchoredPosition.x - X, Cloud[i].GetComponent<RectTransform>().anchoredPosition.y - X / 4);
                for (int i = 2; i < 12; i += 4) Cloud[i].GetComponent<RectTransform>().anchoredPosition = new Vector2(Cloud[i].GetComponent<RectTransform>().anchoredPosition.x - X / 5, Cloud[i].GetComponent<RectTransform>().anchoredPosition.y + X / 2);
                for (int i = 3; i < 12; i += 4) Cloud[i].GetComponent<RectTransform>().anchoredPosition = new Vector2(Cloud[i].GetComponent<RectTransform>().anchoredPosition.x + X / 3, Cloud[i].GetComponent<RectTransform>().anchoredPosition.y - X / 6);
            }
            if (X >= 152)
            {
                X = 0;
                Camera.transform.position = new Vector3(0, 12, 0);
                StoryShift = 3;
            }
        }
        if (StoryShift == 3)
        {
            if (X++ > 50)
            {
                //カメラは揺らさない
                X = 50;
                Camera.transform.position = new Vector3(0, 12, 0);
                Camera.transform.rotation = Quaternion.Euler(90, 0, 0);
                StoryText.SetActive(false);
                Clouds.SetActive(false);
                Shift = 3;
            }
        }

    }
}
