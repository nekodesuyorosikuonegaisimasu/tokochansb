﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.AI;
using UnityEngine.UI;

// 主に姫の動きや当たり判定を扱うcs
public class PrincessManager : MonoBehaviour
{
    //ゲームオブジェクト

    public GameObject Player;   // プレイヤー
    public GameObject FootPrint; // 足跡
    GameObject FootPrintBox; // 足跡の親オブジェクト
    GameObject FootPrintObj; // インスタンスのため
    GameObject Enemy; // ゲームオーバー時に姫を倒した相手の名前を出すためにある

    //ナビメッシュ
    private NavMeshAgent Princess; // 姫のナビメッシュ

    //値
    public float PSpeed;     //姫の移動速度
    public float PHp;       //姫の体力
    private float FootPrinttime; // 足跡を生成するスパン
    private float FootSetime; // 足音を鳴らすスパン
    public float PHpMax; // 姫の体力の最大値(現在は100で固定) 
    private Vector3 GoalDistance; //姫とゴールの距離

    //判定
    public bool MoveFlag;   //移動を制限する時に使うフラグ
    public bool ObstaclesStopFlag; //障害物によって止まるフラグ

    //位置
    public GameObject Goal; // 目的地
    
    // UI
    public GameObject PrincessUI; // イメージ画像
    private GameObject ClearUI; // クリア時のUI
    public float ShakeCount;//被ダメージ時に揺れさせるのに使用
    public float span = 1f; // 揺れるまでの間隔

    //HPUI
    public GameObject Gauge; // イメージ画像
    private readonly float Max = 1.00f; // ゲージの最大値
    private readonly float Min = 0.25f; // ゲージの最小値(きれいに見える数値に設定)
   
    //旗のMaterial
    public Material FlagMaterial; // 直接入れる

    public void PrincessStart()
    {
        FootPrintBox = GameObject.Find("FootPrintBox"); // FootPrintの親オブジェクトを取得

        Princess = GetComponent<NavMeshAgent>(); // Princessにナビメッシュを入れる

        Goal = GameObject.Find("Goal"); // ゴール取得

        ClearUI = GameObject.Find("ClearUI"); // クリアUI取得
        PrincessUI = GameObject.Find("PrincessUIImage"); // クリアUI取得
        Gauge = GameObject.Find("FrontGaugeImage"); // クリアUI取得


        MoveFlag = false;   // MoveFlagの初期化
        ObstaclesStopFlag = false; //障害物にはぶつかっていない
        PSpeed = 3.0f;      // 移動速度を設定
        PHpMax = 100.0f;
        PHp = 100.0f;        // 体力を設定
        GoalDistance = new Vector3(0.0f,0.0f,0.0f); //空にしておく

        //HPゲージ
        Gauge.GetComponent<Image>().fillAmount = PHp * (Max - Min) / 100 + Min;
        Gauge.GetComponent<Image>().color = Color.green;
    }

    void Update()
    {
        //もし、Playerがいなかったら探す
        if (Player == null){Player = GameObject.Find("Player");}
        
        //ダメージ処理
        Gauge.GetComponent<Image>().fillAmount = PHp * (Max - Min) / 100 + Min;

        //PHpのゲージが 100%:緑 50%:黄色 25%:赤
        if (PHp <= PHpMax / 4) Gauge.GetComponent<Image>().color = Color.red;
        else if (PHp > PHpMax / 4 && PHp <= PHpMax / 2) Gauge.GetComponent<Image>().color = Color.yellow;
        else Gauge.GetComponent<Image>().color = Color.green;
        // 体力がPHpMax(ここでは100に設定)以上になったらPHpMaxに戻す。
        if (PHp > PHpMax){PHp = PHpMax; }

        //距離を計算
        GoalDistance= transform.position - Goal.transform.position; 
        //正数に変換
        if(GoalDistance.x < 0.0f) { GoalDistance.x *= -1.0f;}
        if (GoalDistance.z < 0.0f) { GoalDistance.z *= -1.0f; }

        //一定の距離
        if ((GoalDistance.x < 1.0f)&& (GoalDistance.z < 1.0f)  && ClearManager.ClearFlag == false) 
        {
            Princess.destination = Goal.transform.position; //姫の位置固定
            MoveFlag = false; //動かない
            //クリアフラグを立てる
            SceneManager.LoadScene("Stage4");

        }
        
        //動ける
        if (MoveFlag)
        {
            Princess.destination = Goal.transform.position; //姫の目標をゴールに
            this.FootPrinttime += Time.deltaTime; //生成時間をカウント
            this.FootSetime += Time.deltaTime; //鳴らす時間をカウント
            //足跡を0.35秒ごとに生成
            if (this.FootPrinttime > 0.35f)
            {
                this.FootPrinttime = 0f; //リセット
                //足跡の生成
                FootPrintObj = (GameObject)Instantiate(FootPrint, new Vector3(transform.position.x, transform.position.y - 0.7f, transform.position.z), transform.rotation);
               //親の変更
                FootPrintObj.transform.parent = FootPrintBox.transform;
            }
            //足音を1.45秒ごとに鳴らす
            if (this.FootSetime > 1.45f)
            {
                this.FootSetime = 0f; //リセット
                SoundManager.Instance.PlaySeByName("footsteps4", this.gameObject); //SEを鳴らす
            }
        }
        else if(!ObstaclesStopFlag)
        {
            MoveFlag = true; //動ける
        }
        
        //一定の間隔でダメージエフェクト
        if (ShakeCount >= 50)
        {
            PrincessUI.GetComponent<RectTransform>().anchoredPosition = new Vector3(-813, 385, 0);
            PrincessUI.GetComponent<Image>().color = Color.white; //イメージを白
            StopCoroutine("Shake"); //ダメージを受けた演出
            ShakeCount = 0; //リセット
        }
    }
    //敵に触れたらダメージ
    public void OnTriggerEnter(Collider collider)
    {
        //Hpがある
        if (PHp > 0)
        {
            //当たったのが敵だった
            if (collider.gameObject.CompareTag("Enemy"))
            {
                //攻撃を与えた敵を記録
                if (collider.name == "Lich(Clone)") Enemy = collider.gameObject;
                else Enemy = collider.transform.parent.gameObject;
                Hit(); //攻撃が当たった時の処理
            }
        }
        for (int i = 0;i < 5;i++)
        {   
         if(collider.gameObject.name == "type[" + i + "]")
            {

            }
        }
    }
    public void Hit() // 攻撃に当たった時
    {
        //すべての敵パターンと照合
        for (int i = 0; i < PlayDataManager.ENEMY_NAME.Length; i++)
        {
            //該当の敵キャラ情報を見つけた
            if (Enemy.name == PlayDataManager.ENEMY_NAME[i])
            {
                SoundManager.Instance.PlaySeByName("PrincessFaceAttack", Player); // ダメージ音
                StartCoroutine("Shake"); //ダメージ演出
                PHp -= PlayDataManager.ENEMY_STATUS[i,1]; //該当のダメージを与える
                //Hpが0になった
                if (PHp <= 0)
                {
                    GameOverManager.ReasonNum = i; //殺してきた敵の指定番号を送る
                    SceneManager.LoadScene("GameOver"); //ゲームオーバー画面へ
                }
            }
        }
    }
    //ダメージ演出
    private IEnumerator Shake()
    {
        while (true)
        {
            ShakeCount++; //カウント増加
            PrincessUI.GetComponent<Image>().color = Color.red; //イメージの色を赤に
            PrincessUI.GetComponent<RectTransform>().anchoredPosition = new Vector3(Random.Range(-10.0f, 10.0f) - 813, Random.Range(-10.0f, 10.0f) + 385, 0);
            yield return new WaitForSeconds(span);
        }
    }
}
