﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

// 敵の出現を扱うcs
public class EnemySpown : MonoBehaviour
{
    public GameObject Princess; // 姫
    private float PrincessDistance; // 姫までの距離を測る
    public GameObject[] EnemyPrefabs; //オブジェクトを格納する配列変数

    public int Intarval; // 出現時間間隔をここで決める。(ステージごとに変えられる)

    private float time; //出現する間隔を制御するための変数
    private int number; //ランダム情報を入れるための変数
    public int Type; // スポーンの種類(手動でタイプは変更する)

    // 出現位置の回転のためのもの
    private bool MoveFlag; //出現場所を移動させるためのフラグ
    private Vector3 Center = Vector3.zero;// 中心点
    private Vector3 Axis = Vector3.up;// 回転軸
    private float Period = 2;// 円運動周期

    private GameObject Enemy;
    public GameObject DamageEffect; // ダメージエフェクト
    private GameObject EnemyList; //敵を入れておく箱

    public GameObject Player;   // プレイヤー
    private float PlayerDistance; // プレイヤーまでの距離を測る

    public float Hp; // 体力
    float HitCount; // 攻撃されるときに使う無敵時間
    private　bool DamageFlag; // ダメージ中は攻撃を受けないようにする
    private bool SummonFireFlag;
    Attack attack; // Attack.cs

    void Start()
    {
        time = 1.0f; //時間を待たず、最初の1回を出現

        // Princessを探す
        Princess = GameObject.Find("Princess" + "(Clone)");
        if (Princess == null) Princess = GameObject.FindGameObjectWithTag("Princess");// 姫をtagを参照して取得
        // Playerを探す
        Player = GameObject.Find("Player");
        attack = Player.GetComponent<Attack>();

        Center = Princess.transform.position; // 回転をするとき中心は姫
        Axis = Vector3.up; // 回転軸
        Period = 1.3f; // 周期

        EnemyList = GameObject.Find("EnemyList"); //検索

        HitCount = 0; // カウントを0にしておく
        DamageFlag = true; // DamageFlagの初期化
        SummonFireFlag = false; // SummonFireFlagの初期化
    }

    void Update()
    {
        // ポーズ中は操作を受け付けない
        if (Mathf.Approximately(Time.timeScale, 0f)) return;

        //もし、Princessがいなかったら探す
        if (Princess == null) Princess = GameObject.FindGameObjectWithTag("Princess");// tagを参照して取得
        //もし、Playerいなかったら探す
        if (Player == null) Player = GameObject.Find("Player");
        // Princessを探す
        Princess = GameObject.Find("Princess" + "(Clone)");
        if (Princess == null) Princess = GameObject.FindGameObjectWithTag("Princess");// 姫をtagを参照して取得
        PrincessDistance = Vector3.Distance(Princess.transform.position, transform.position);
        PlayerDistance = Vector3.Distance(Player.transform.position, transform.position);

        // ここで無敵時間をやる
        if (HitCount > 0)
        {
            HitCount -= Time.deltaTime;
        }
        else
        {
            HitCount = 0;
            DamageFlag = true;
        }

        /*
        Typeは現在3種類となっている。
        Type0は姫の周りを浮きまわり一定時間で敵を発生させていく
        Type1は姫またはプレイヤーが近づくと一定時間で敵を発生させる。これにはBoxColliderを付けて壊せるようにする。
        Type2は姫またはプレイヤーが近づくと瞬間的に敵を小～大規模発生させる。
        */

        if (Type == 0) // 姫の周りをまわり、一定時間でエネミー発生する
        {
            //移動処理
            Center = Princess.transform.position; // 姫が動くので毎回更新して回転をやる

            // 中心点Centerの周りを、軸Axisで、Period周期で円運動
            transform.RotateAround(Center, Axis, 360 / Period * Time.deltaTime);

            /*エネミーをインスタンスするときにナビメッシュ上にインスタンスする*/
            var spawnPosition = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z);
            NavMeshHit navMeshHit;
            if(NavMesh.SamplePosition(spawnPosition, out navMeshHit, 10, NavMesh.AllAreas))

            //出現するインターバルを決める
            time -= Time.deltaTime; //timeから時間を減らす
            if (time <= 0.0f) //0秒になれば
            {
                time = Intarval; //Intarval秒にする(ここが敵の出現インターバルになる)
                number = Random.Range(0, EnemyPrefabs.Length); //EnemyPrefabsの数だけランダムにする
                Enemy = Instantiate(EnemyPrefabs[number],navMeshHit.position, Quaternion.identity); //同じ位置にランダム出現、向きの設定は無し
                Enemy.transform.parent = EnemyList.transform;
                if (Enemy.gameObject.name == "Slime(Clone)" || Enemy.gameObject.name == "TurtleShell(Clone)")Enemy.GetComponent<EnemyManager>().DieFlag = true;
                else if(Enemy.gameObject.name == "Lich(Clone)")Enemy.GetComponent<LichManager>().Dieflg = true;
            }
        }
        if (Type == 1) // 固定置きで一定範囲内にプリンセスが来たらエネミーを一定間隔で発生する。(これは壊せる)
        {
            if(PrincessDistance < 26.0f || PlayerDistance < 26.0f)
            {
                /*エネミーをナビメッシュ上に生成する*/
                var spawnPosition = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z);
                NavMeshHit navMeshHit;
                if (NavMesh.SamplePosition(spawnPosition, out navMeshHit, 10, NavMesh.AllAreas))

                    //出現するインターバルを決める
                    time -= Time.deltaTime; //timeから時間を減らす
                if (time <= 0.0f) //0秒になれば
                {
                    time = Intarval; //Intarval秒にする(ここが敵の出現インターバルになる)
                    number = Random.Range(0, EnemyPrefabs.Length); //EnemyPrefabsの数だけランダムにする
                    Enemy = Instantiate(EnemyPrefabs[number], navMeshHit.position, Quaternion.identity); //同じ位置にランダム出現、向きの設定は無し
                    Enemy.transform.parent = EnemyList.transform;
                    if (Enemy.gameObject.name == "Slime(Clone)" || Enemy.gameObject.name == "TurtleShell(Clone)") Enemy.GetComponent<EnemyManager>().DieFlag = true;
                    else if (Enemy.gameObject.name == "Lich(Clone)") Enemy.GetComponent<LichManager>().Dieflg = true;
                }
            }
        }
        if (Type == 2) // 固定置きで一定範囲内にプリンセスが来たらエネミーが小～大規模発生する
        {

            if (PrincessDistance < 25.0f || PlayerDistance < 25.0f)
            {                
                /*エネミーをナビメッシュ上に生成する*/
                var spawnPosition = new Vector3(this.transform.position.x, this.transform.position.y, this.transform.position.z);
                NavMeshHit navMeshHit;
                if (NavMesh.SamplePosition(spawnPosition, out navMeshHit, 10, NavMesh.AllAreas))
                    
                //出現するインターバルを決める
                time -= Time.deltaTime; //timeから時間を減らす
                if (time <= 0.0f) //0秒になれば
                {
                    time = Intarval; //Intarval秒にする(ここが敵の出現インターバルになる)

                    for (int i = 0; i < EnemyPrefabs.Length; i++)
                    {
                        Vector3 EnemyPos = new Vector3
                                                ((i / 3 + 1) * Mathf.Cos(2 * Mathf.PI * (i % 3) / 3 + i / 3),
                                                0,
                                                (i / 3 + 1) * Mathf.Sin(2 * Mathf.PI * (i % 3) / 3 + i / 3));
                        number = Random.Range(0, EnemyPrefabs.Length); //EnemyPrefabsの数だけランダムにする
                        Enemy = Instantiate(EnemyPrefabs[number], navMeshHit.position + EnemyPos, Quaternion.identity); //同じ位置にランダム出現、向きの設定は無し
                        Enemy.transform.parent = EnemyList.transform;
                        if (Enemy.gameObject.name == "Slime(Clone)" || Enemy.gameObject.name == "TurtleShell(Clone)") Enemy.GetComponent<EnemyManager>().DieFlag = true;
                        else if (Enemy.gameObject.name == "Lich(Clone)") Enemy.GetComponent<LichManager>().Dieflg = true;
                    }
                }

            }
        }
    }

    //攻撃に触れたらダメージ
    public void OnTriggerEnter(Collider collision)
    {
        if (Hp > 0)
        {
            if (DamageFlag)
            {
                // 通常攻撃ヒット
                if (collision.gameObject.CompareTag("NormalWeapon"))
                {
                    // ヒット処理へ
                    Hit(Player.GetComponent<Player>().NattackPower);
                }
                // 必殺技ヒット
                if (collision.gameObject.CompareTag("SkillWeapon"))
                {
                    // ヒット処理へ
                    Hit(Player.GetComponent<Player>().SattackPower);
                }
            }
        }
    }
    public void Hit(float Power) // 攻撃に当たった時
    {
        // エフェクトの生成
        Instantiate(DamageEffect, transform.position + transform.forward + transform.up, transform.rotation);
        // ダメージ処理
        Hp -= Power * Player.GetComponent<Attack>().Item; // プレイヤーの攻撃力分減らす
        if (Hp >= 1)
        {
            DamageFlag = false;
            HitCount = 1;
        }
        else // Hpが0になったら
        {
            // デストロイ
            Destroy(this.gameObject); // 消える
        }
    }
}
