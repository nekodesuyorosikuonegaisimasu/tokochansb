﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    //メインカメラ格納用
    public GameObject mainCamera;
    //サブカメラ格納用
    public GameObject subCamera;      
    //ポーズ画面検索
    private GameObject PauseManager;
    // カメラがサブカメラであるかどうか
    private bool subCameraFlag;
    // カメラの角度を保存
    public float camerarotation;
    
    //呼び出し時に実行される関数
    void Start()
    {
        //メインカメラとサブカメラをそれぞれ取得
        mainCamera = GameObject.Find("Main Camera");
        subCamera = GameObject.Find("Sub Camera");
        PauseManager = GameObject.Find("PauseManager");
        //サブカメラを非アクティブにする
        subCamera.SetActive(false);
        // 最初はサブカメラをオフにする
        subCameraFlag = false;
    }
    
    //単位時間ごとに実行される関数
    void Update()
    {
        // ポーズ中は操作を受け付けない
        if (Mathf.Approximately(Time.timeScale, 0f)) return;

        // スライドアウトしきってないとカメラ切り替えができない
        if(PauseManager.GetComponent<Pause>().number != -1) return;

        // サブカメラをアクティブにする
        if ((Input.GetButtonDown("Pad_L2") || Input.GetButtonDown("Pad_R2") || Input.GetButtonDown("Pad_R3") || Input.GetButtonDown("Mouse_R2")) && !subCameraFlag)
        {
            subCameraFlag = true;
            // カメラの角度を保存
            camerarotation = mainCamera.GetComponent<Camera>().angleX;
            //サブカメラをアクティブに設定
            subCamera.SetActive(true);
            mainCamera.SetActive(false);
            // メインカメラの角度をサブカメラに反映
            subCamera.GetComponent<Camera>().angleX = camerarotation;
        }
        // メインカメラをアクティブにする
        else if ((Input.GetButtonDown("Pad_L2") || Input.GetButtonDown("Pad_R2") || Input.GetButtonDown("Pad_R3") || Input.GetButtonDown("Mouse_R2")) && subCameraFlag)
        {
            subCameraFlag = false;
            // カメラの角度を保存
            camerarotation = subCamera.GetComponent<Camera>().angleX;
            //メインカメラをアクティブに設定
            mainCamera.SetActive(true);
            subCamera.SetActive(false);
            // サブカメラの角度をメインカメラに反映
            mainCamera.GetComponent<Camera>().angleX = camerarotation;
        }
    }
}
