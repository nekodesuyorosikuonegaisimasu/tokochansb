﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;


public class ControllerChecker : MonoBehaviour
{
    private GameObject Back;
    private GameObject Warning;

    public static bool CheckController;//コントローラー操作かマウス操作か識別する
    public static int CheckConnected;// コントローラーが挿されているか識別する

    public static bool CheckFlag;//フラグが立っていないと操作が出来なくなる

    void Awake()
    {
        //コントローラーチェッカーという名前のオブジェクトを探す
        GameObject CCC = GameObject.Find("ControllerChecker");
        //見つけたオブジェクトが自分ではなかった場合オブジェクトを削除する
        if (CCC != this.gameObject) Destroy(gameObject);
        //スクリーンが空のときは画像を探す
        if (Back == null) Back = GameObject.Find("CCBack");
        if (Warning == null) Warning = GameObject.Find("CCWarning");
        //画像を探した後非表示にする
        Back.SetActive(false);
        Warning.SetActive(false);
        //シーンを跨いでもオブジェクトが消えないようにする
        DontDestroyOnLoad(gameObject);
        CheckFlag = true;
    }

    private void Start()
    {
        CheckController = true;
    }

    // Update is called once per frame
    void Update()
    {
        var controllerNames = Input.GetJoystickNames();//コントローラが接続されていたら、コントローラ名を入れる

        string controllerName = "";
        int i = 0;
        //コントローラ名の文字数を格納
        while (controllerNames.Length > i && controllerNames[i] == "")
        {
            i++;
        }
        //stringに格納
        if (controllerNames.Length > i)
        {
            controllerName = controllerNames[i];
        }

        //コントローラーが挿されていない時はガイドをマウス＆キーボードに変更するようにする
        if (controllerName == "")
        {
            CheckController = false;
            CheckConnected = 0;
        }
        //コントローラーが刺さっているとき押されたキーに応じてコントローラーとキーボード操作を切り替える
        else if (Input.anyKeyDown)
        {
            foreach (KeyCode code in Enum.GetValues(typeof(KeyCode)))
            {
                if (Input.GetKeyDown(code))
                {
                    if (CheckController == true)
                    {
                        //コントローラー以外の操作が行われたとき
                        if (!code.ToString().Contains("Joystick"))
                        {
                            //チェックコントローラーを切り替える
                            CheckController = false;
                        }
                    }
                    //チェックコントローラーがfalseのとき
                    if (CheckController == false)
                    {
                        //コントローラー操作が行われたとき
                        if (code.ToString().Contains("Joystick"))
                        {
                            //チェックコントローラーを切り替える
                            CheckController = true;
                        }
                    }
                }
            }
        }
        // ステージシーン以前にコントローラーが挿された場合
        else if (controllerName != "" && CheckConnected == 0 && !SceneManager.GetActiveScene().name.Contains("Stage"))
        {
            CheckConnected = 2;
        }
        // ステージシーンの途中でコントローラーが挿された場合
        else if (controllerName != "" && CheckConnected == 0 && SceneManager.GetActiveScene().name.Contains("Stage") && SceneManager.GetActiveScene().name != "StageSelect")
        {
            CheckConnected = 1;
        }

        // ステージシーンとエンドロールシーン以外のときのマウスカーソルのオンオフ
        if (!(SceneManager.GetActiveScene().name.Contains("Stage") && SceneManager.GetActiveScene().name != "StageSelect") && SceneManager.GetActiveScene().name != "EndRoll")
        {
            if(CheckController) Cursor.visible = false;
            else Cursor.visible = true;
        }
        
    }
}

