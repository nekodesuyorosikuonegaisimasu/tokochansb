﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextSetting : MonoBehaviour
{
    GameObject Camera; //カメラのオブジェクトを入れる
    // Start is called before the first frame update
    void Start()
    {
        //メインカメラを検索
        Camera = GameObject.Find("Main Camera"); 
    }

    // Update is called once per frame
    void Update()
    {
        //もしカメラが切り替えられていたら
        if(!Camera.activeSelf)
        {
            //現在選択中のカメラごとに処理する
            switch(Camera.name)
            {
                case "Main Camera": //現在メインカメラを選択中
                    //サブカメラに変える
                    Camera = GameObject.Find("Sub Camera");
                    break;
                case "Sub Camera": //現在サブカメラを選択中
                    //メインカメラに変える
                    Camera = GameObject.Find("Main Camera");
                    break;
                default: 
                    break;
            }
        }
        //もし何も入っていなかったらとりあえずメインカメラを入れておく
        if (Camera == null) Camera = GameObject.Find("Main Camera");
        else this.transform.LookAt(Camera.transform); //アイコンの角度を変えておく
    }
}
