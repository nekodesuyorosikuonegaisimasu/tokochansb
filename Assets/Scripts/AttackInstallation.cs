﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackInstallation : MonoBehaviour
{
    // 消えるカウント
    private float count;
    // 射程
    private float NLength;
    private float SLength;
    // プレイヤー6-？
    [SerializeField]
    private int summonNumber;

    // Start is called before the first frame update
    void Start()
    {
        // プレイヤー6なら
        if(PlayDataManager.CharaNum == 6)
        {
            // 射程を選択した召喚物に設定
            NLength = PlayDataManager.CHARA_STATUS[summonNumber, 1];
        }
        else 
        {
            // プレイヤー毎の射程を設定
            NLength = PlayDataManager.CHARA_STATUS[PlayDataManager.CharaNum, 1];
            SLength = PlayDataManager.CHARA_STATUS[PlayDataManager.CharaNum, 3];
        }
    }

    // Update is called once per frame
    void Update()
    {
        // カウント
        count += Time.deltaTime;

        // 消滅処理
        if(summonNumber == 4)
        {
            if (count > NLength) Destroy(this.gameObject);
        }
        else
        {
            if(count > 10 * NLength) Destroy(this.gameObject);
            if(count > 10 * SLength) Destroy(this.gameObject);
        }
    }
}
