﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
using UnityEngine.SceneManagement;

// プレイヤーの生成とステージBGMの再生に使います
public class PlayerManager : MonoBehaviour
{
    // プレイヤーオブジェクト
    private GameObject playerObj;
    // 姫オブジェクト
    private GameObject princessObj;
    // 生成オブジェクト
    private GameObject instance;
    // 選択されたキャラクター情報
    private int playerNumber;
    // プレイヤー5,6用キャンバス
    public GameObject PlayerCanvas;
    // ランダムで選択された数値を取得
    private int randomNum;
    // BGMオブジェクトを探す
    private GameObject getBgm;
    // スタート地点を入れる
    private GameObject StartPoint;
    // CheckPointManagerが入っているオブジェクトを探す
    private GameObject princessRoute;
    // ボス部屋のモヤ
    //private GameObject haze;
    // ポーズしているか
    public bool pauseFlag;
    // 切り替え前のプレイヤーの処理
    private Vector3 PlayerPos;
    // 切り替え前のプレイヤーをとる
    public GameObject BeforePlayer;
    //スタート地点変更用のスクリプト
    private CheckPointManager StartPoinMane;

    // キャラクターの生成
    void Awake()
    {
        StartPoinMane = GameObject.Find("PrincessRoute").GetComponent<CheckPointManager>(); //検索
        StartPoinMane.InstanceCheckPoint(); //スタート地点の更新
        StartPoint = GameObject.Find("StartPoint"); //スタート地点の検索

        // Resourcesファイルからキャラクターのロードと生成
        playerObj = (GameObject)Resources.Load("Player0");
        princessObj = (GameObject)Resources.Load("Princess");

        // 姫をチェックポイントの場所に生成
        instance = (GameObject)Instantiate(princessObj, new Vector3(StartPoint.transform.position.x, StartPoint.transform.position.y, StartPoint.transform.position.z), Quaternion.identity);
        instance.name = "Princess"; // クローンしたオブジェクトの名前を変更
        instance.transform.parent = this.gameObject.transform;
        instance.GetComponent<PrincessManager>().PrincessStart(); //初期情報を設定

        // プレイヤーをチェックポイントの場所に生成
        instance = (GameObject)Instantiate(playerObj, new Vector3(StartPoint.transform.position.x + 3, StartPoint.transform.position.y, StartPoint.transform.position.z + 3), Quaternion.identity);
        instance.name = "Player";
        instance.transform.parent = this.gameObject.transform;

        // カウントの生成
        if (playerNumber == 5) Instantiate(PlayerCanvas, new Vector3(0, 0, 0), transform.rotation);
        if(playerNumber == 6) Instantiate(PlayerCanvas, new Vector3(0, 0, 0), transform.rotation);

        //if(PlayDataManager.StageNum == 3) haze = GameObject.Find("Haze1");

    }

    // Start is called before the first frame update
    void Start()
    {
        randomNum = 0;
        getBgm = GameObject.Find("BGM");
        // Resourcesファイルの中のAudioファイルの中のBGMファイルを読み込み文字列にする
        string path = Application.dataPath + "/" + "Resources";
        // BGMファイル内のフォルダの拡張子が「.mp3」のものを配列に入れる
        string[] files = Directory.GetFiles(path, "*.mp3", SearchOption.AllDirectories);
        int i = 0;
        for(int j = 0; j < files.Length; j++)
        {
            // 配列内のステージごとのBGMの数を数える
            if(files[j].Contains("Stage0" + PlayDataManager.WorldStageNum))
            {
                i++;
            }
        }
        
        randomNum = UnityEngine.Random.Range(1, i+1);
        // ステージBGMの再生
        if(getBgm != null) SoundManager.Instance.PlayBgmByName("Stage0" + PlayDataManager.WorldStageNum + "-" + randomNum, "Main");
        pauseFlag = false;
    }

    void Update()
    {
        // ステージ3以外のオプションから戻った時などの処理
        if(PlayDataManager.WorldStageNum != 3)
        {
            if(getBgm.GetComponent<AudioSource>().clip == null)
            {
                SoundManager.Instance.PlayBgmByName("Stage0" + PlayDataManager.WorldStageNum + "-" + randomNum, "Main");
            }
        }
        // ステージ3の処理
        else
        {
            // ボス戦中にオプションから戻った場合
            if(getBgm.GetComponent<AudioSource>().clip == null)
            {
                SoundManager.Instance.PlayBgmByName("BossBattle", "Main"); // ボス戦用のBGMを流す
            }
            else
            {
                if (getBgm.GetComponent<AudioSource>().clip == null)
                {
                    SoundManager.Instance.PlayBgmByName("Stage0" + PlayDataManager.WorldStageNum + "-" + randomNum, "Main");
                }
            }
        }
        // ステージシーンのみマウスカーソルを非表示にする
        if(SceneManager.GetActiveScene().name.Contains("Stage") && SceneManager.GetActiveScene().name != "StageSelect" && !pauseFlag && !ClearManager.ClearFlag)
        {
            // カーソルを表示しない
            Cursor.visible = false;
        }
        else
        {
            // マウスキーボード操作ならカーソルを表示する
            if(!ControllerChecker.CheckController)Cursor.visible = true;
        }
    }
    // プレイヤー切り替え
    public void PlayerSpown(int PlayerNum)
    {
        // 切り替え前のプレイヤーをの情報を取っておく
        BeforePlayer = instance;
        PlayerPos = instance.transform.position;
        // 切り替え後のプレイヤーを切り替え前の場所に生成
        playerObj = (GameObject)Resources.Load("Player" + PlayerNum);
        instance = (GameObject)Instantiate(playerObj, PlayerPos, BeforePlayer.transform.rotation);
        instance.name = "Player";
        // 切り替え（登場）効果音
        SoundManager.Instance.PlaySeByName("PlayerChange", instance);
    }
}
