﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// 姫が遠くに行ったときサブカメラ時にプレイヤーの頭上に矢印を出すためのcs
// 姫との距離が遠いほど大きくする(上限はあり)
// プレイヤーの子オブジェクトにすることで頭上に出ているようにする
public class ArrowSymbolManager : MonoBehaviour
{
    private GameObject Princess; // 姫
    private float PrincessDistance; // 姫との距離
    public Transform ArrowSymbol; // 矢印 // Transformなので直接入れます。
    private Vector3 ArrowScale; // 矢印の大きさ

    void Start()
    {
        Princess = GameObject.FindGameObjectWithTag("Princess");// 姫をtagを参照して取得
    }
    
    void Update()
    {
        // LookAtで矢印の先端が姫に向くようにする // LoocAtだけではY軸も動いてしまうのでrotationでY軸は固定する
        ArrowSymbol.LookAt(new Vector3(Princess.transform.position.x, 0, Princess.transform.position.z)); 
        ArrowSymbol.transform.rotation = new Quaternion(this.gameObject.transform.rotation.x, 0, this.gameObject.transform.rotation.z, 0);

        // 距離を測り、大きさを決める
        PrincessDistance = Vector3.Distance(Princess.transform.position, this.gameObject.transform.position);
        ArrowScale = new Vector3(1, 1, 1) * PrincessDistance / 20;

        // 最大最小
        // 3f以下はサブカメラにしても見えないように0fにする
        if (ArrowScale.x < 3f) ArrowScale.x = 0f;
        if (ArrowScale.y < 3f) ArrowScale.y = 0f;
        if (ArrowScale.z < 3f) ArrowScale.z = 0f;
        // 最大上限
        if (ArrowScale.x > 7) ArrowScale.x = 7;
        if (ArrowScale.y > 7) ArrowScale.y = 7;
        if (ArrowScale.z > 7) ArrowScale.z = 7;

        transform.localScale = ArrowScale; // 大きさを変更する
    }
}
