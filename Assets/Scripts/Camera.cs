﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera : MonoBehaviour
{
    //プレイヤー情報格納用
    private GameObject player;
    //相対距離取得用
    private Vector3 offset;      

    //　旋回するターゲット
    [SerializeField]
    private Transform target;
    //　現在の角度
    public float angleX;
    public float angleY;
    //　ターゲットからの距離
    [SerializeField]
    private Vector3 distanceFromTarget;
    // ターゲットのオブジェクト
    GameObject targetObj;
    // ターゲットの座標
    Vector3 targetPos;
    // 追加座標（プレイヤーからカメラの位置を遠のけるのに使う）
    Vector3 plusPos;
    // サブカメラ（遠くからのカメラ）
    private bool subCameraFlag;
    // プレイヤー6用
    private bool onceFlag;
    private GameObject truePlayer;
    // カメラの揺れ
    private float time;
    // カメラの揺れの角度
    private float shakerot;
    // コントローラー判別
    string XAxis;
    string YAxis;
    // 視点移動
    float cameraInputX;
    float cameraInputY;
    // カメラの距離を操作する
    UnityEngine.Camera cam; 
    // テキストのコライダー
    [SerializeField]
    BoxCollider[] text = new BoxCollider[5];

    private GameObject Pause;

    // Use this for initialization
    void Start()
    {
        // プレイヤーの座標を探す
        target = GameObject.Find("Player").transform;
        // プレイヤーを探す
        player = GameObject.Find("Player");
        // MainCameraとplayerとの相対距離を求める
        offset = transform.position - player.transform.position;
        // 追加座標
        plusPos = new Vector3(0, 5, 0);
        // 初期化
        onceFlag = false;
        // プレイヤーを探す
        truePlayer = GameObject.Find("Player" + PlayDataManager.CharaNum);
        // このスクリプトがついているカメラの名前が「Sub Camera」ならサブカメラとして扱う
        if(this.gameObject.name == "Sub Camera") subCameraFlag = true;
        // カメラの距離を操作する
        cam = GetComponent<UnityEngine.Camera>();
        // ポーズマネージャーを探す
        Pause = GameObject.Find("PauseManager");
    }

    // Update is called once per frame
    void Update()
    {
        // プレイヤー切り替えの時の処理
        if(player == null) PlayerChange();
        // コントローラー対応処理
        SetController();

        // ポーズ中は操作を受け付けない
        if (Mathf.Approximately(Time.timeScale, 0f))
        {
            if(Pause.GetComponent<Pause>().pauseFlag == true)
            {   
                // ポーズ中の処理へ
                PauseCamera();
            }
            return;
        }
        else ResetPauseCamera();

        // 新しいトランスフォームの値を代入する
        transform.position = player.transform.position + offset;
        // カメラの移動
        if(ControllerChecker.CheckController)
        {
            cameraInputX = Input.GetAxis(XAxis) * 180.0f;
            cameraInputY = Input.GetAxis(YAxis) * 180.0f;
        }
        else
        {
            cameraInputX = Input.GetAxis(XAxis) * (float)PlayDataManager.CameraSensitive;
            cameraInputY = Input.GetAxis(YAxis) * (float)PlayDataManager.CameraSensitive;
        }
        // カメラの位置 = ターゲットの位置 ＋ ターゲットから見たユニットの角度 ×　ターゲットからの距離
        transform.position = plusPos + target.position + Quaternion.Euler(angleY, angleX, 0f) * distanceFromTarget;
        // カメラの角度 = ターゲットから見たカメラの方向の角度を計算しそれをカメラの角度に設定する
        transform.rotation = Quaternion.LookRotation(-transform.position + new Vector3(target.position.x, target.position.y, target.position.z), Vector3.up + new Vector3(Random.Range(-shakerot, shakerot), Random.Range(-shakerot, shakerot), Random.Range(-shakerot, shakerot)));
        if(PlayDataManager.CharaNum == 4)transform.position += new Vector3(0, 10, 0);
        // 角度を変更
        angleX += cameraInputX * Time.deltaTime;
        if(!subCameraFlag)angleY += cameraInputY * Time.deltaTime;
        // Y軸カメラの移動制限
        if(angleY < -50) angleY = -50;
        if(angleY > 100) angleY = 100;
        // シェイクタイムが0でないなら
        if (time != 0.0f) ShakeTime();
    }

    // プレイヤーを切り替えた時の処理
    public void PlayerChange()
    {
        // プレイヤー近距離の取得・カメラのターゲットを変更
        target = GameObject.Find("Player").transform;// + PlayerChangeManager.ChangeCount
        player = GameObject.Find("Player");// + PlayerChangeManager.ChangeCount
        offset = transform.position - player.transform.position;
    }
    // カメラのターゲットをリセットする
    void ResetCamera()
    {
        // プレイヤーの座標を探す
        target = truePlayer.transform;
        // プレイヤーを探す
        player = truePlayer;
        // MainCameraとplayerとの相対距離を求める
        offset = transform.position - player.transform.position;

        onceFlag = false;
    }
    // 攻撃された時の画面を揺らす処理
    public void Shake(float ShakeRotation, float ShakeTime)
    {
        shakerot = ShakeRotation;
        time = ShakeTime;
    }
    // 画面を揺らす時間のカウント
    private void ShakeTime()
    {
        time -= Time.deltaTime;
        if(time <= 0.0f) shakerot = 0.0f;
    }
    // ゲーム中のカメラのポーズ中の処理
    private void PauseCamera()
    {
        // ポーズ中なら座標をうごかす
        transform.position = new Vector3(376.9f, 337.1f, 462.3f);
        transform.rotation = new Quaternion(0, 180, 0, 0);
        for(int i = 0; i < 5; i++)
        {
            text[i].size = new Vector3(500, 90, 1);
            text[i].GetComponent<Collider>().enabled = true;
        }
        cam.fieldOfView = 60;
    }
    // ポーズ解除時のカメラのリセット処理
    private void ResetPauseCamera()
    {
        cam.fieldOfView = 60;
        for (int i = 0; i < 5; i++)
        {
            text[i].size = new Vector3(400, 90, 1);
            text[i].GetComponent<Collider>().enabled = false;
        }
    }

    // コントローラー処理
    private void SetController()
    {
        var controllerNames = Input.GetJoystickNames();//コントローラが接続されていたら、コントローラ名を入れる

        string controllerName = "";
        int i = 0;
        //コントローラ名の文字数を格納
        while (controllerNames.Length > i && controllerNames[i] == "")
        {
            i++;
        }
        //stringに格納
        if (controllerNames.Length > i)
        {
            controllerName = controllerNames[i];
        }

        // コントローラー操作をしている場合
        if (ControllerChecker.CheckController)
        {
            // コントローラの右スティックの名前を格納
            XAxis = "Pad_R_Horizontal";
            YAxis = "Pad_R_Vertical";

            // （学校備品）色付きコントローラの右スティック上下（DirectXInput）
            if (controllerName == "JC-U3613M - DirectXInput Mode")
            {
                YAxis = "Pad_R_Vertical_2";
            }
            // （学校備品）色付きのコントローラの右スティックの左右（Xinput）
            if (controllerName == "Controller (JC-U3613M - Xinput Mode)")
            {
                XAxis = "Pad_X_Horizontal";
            }
        }
        // マウス操作をしている場合
        else
        {
            XAxis = "Mouse X";
            YAxis = "Mouse Y";
        }

    }
}
