﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossHaze : MonoBehaviour
{
    public GameObject Player;   // プレイヤー
    public GameObject Princess;   // 姫
    public GameObject EnemySpown;   // 敵の発生源
    public GameObject Boss; // BossDragonのTypeのためにある // 直接いれる
    public GameObject BossUI; // Boss戦UIの表示のためにある // 直接いれる

    public bool HazeFlag = false; // Haze(もや)に当たった時のフラグ
    public bool BossBattleFlag; // ドラゴンと戦闘中のフラグ
    public bool StartFlag; // Princesが動き出すためのフラグ

    void Start()
    {
        Player = GameObject.Find("Player"); // Playerを探す
        Princess = GameObject.FindGameObjectWithTag("Princess");// 姫をtagを参照して取得
        EnemySpown = GameObject.Find("EnemySpownPoint"); // EnemySpownPointを探す
        Boss = GameObject.Find("RedDragon");    // ボスドラゴンを探す

        /*  SetActiveをfalseにしているために片方しか入らない。
            BossUIはHaze1,2どちらも必要なため直接入れる。   */
        if (BossUI == null)BossUI = GameObject.Find("Canvas/BossUI").gameObject;
        BossUI.SetActive(false);

        HazeFlag = false;
        StartFlag = false;
        if (gameObject.name == "Haze2") StartFlag = true;
        BossBattleFlag = false;
    }

    void Update()
    {
        if (HazeFlag) Princess.GetComponent<Rigidbody>().transform.position = new Vector3(-700f, 3.8f, 650f); // ボス戦中は姫の位置を固定する
    }

    // 触れたら
    public void OnTriggerEnter(Collider collider)
    {
        Boss = GameObject.FindWithTag("BossDragon"); // 死んだときにMissingになるのでFindしてNullにする
        if (collider.gameObject == Player)
        {
            if (!StartFlag)
            {
                SoundManager.Instance.StopBgm(); // ステージのBGMを止める
                SoundManager.Instance.PlayBgmByName("BossBattle", "Main"); // ボス戦用のBGMを流す

                if (BossUI != null)BossUI.SetActive(true); // Boss戦UIを表示させる(体力等)
                BossBattleFlag = true;
                StartFlag = true;
                Player.GetComponent<Rigidbody>().position = new Vector3(-470f, 3f, 900f); // ボス部屋へテレポートする
            }
            if (StartFlag && Boss == null)
            {
                if (BossUI != null)BossUI.SetActive(false); // Boss戦UIを非表示させる
                HazeFlag = false;
                this.transform.parent.gameObject.SetActive(false); // Hazeの一つ親オブジェクトを非表示にする
            }
        }

        if (collider.gameObject == Princess)
        {
            EnemySpown.SetActive(false); // ボス戦の時に敵に殺されないようにする
            HazeFlag = true;
        }
    }
}
