﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

// BossBattle時のUIを管理するcs
public class BossBattleManager : MonoBehaviour
{
    [Header("ボス戦")]
    [SerializeField] private Image DragonGauge; // DragonHpUiを変化させるためにとる。
    [SerializeField] private Image PlayerGauge; // PlayerHpUiを変化させるためにとる

    private Player player;

    public void Playergauge()
    {
        // Playerゲージ減少
        PlayerGauge.fillAmount -= 0.2f;
        // 通常はPlayerにHPはないのでUIのゲージの減りでHPということにする
        if(PlayerGauge.fillAmount <= 0.2f)
        {
             // 死んだ内容をいじってゲームオーバーに飛ぶ
            GameOverManager.ReasonNum = 5;
            SceneManager.LoadScene("GameOver");
        }
    }
    public void Dragongauge()
    {
        // Dragonゲージ減少
        DragonGauge.fillAmount -= 0.2f;
    }
}
