﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//岩の砕けたエフェクト
public class Efect : MonoBehaviour
{
    public float Second;    // エフェクトの滞在時間 
    Transform[]transforms;  // トランスフォーム
    
    //最初
    void Start()
    {
        
    }

    //破砕演出の開始
    public void EffectStart(float scale)
    {
        // 自身の物理挙動の取得
        transforms = gameObject.GetComponentsInChildren<Transform>();
        for (int i = 0;i < transforms.Length; i++) {// 自身の子の数だけ
            transforms[i].localScale *= scale;// 自身の子の大きさを変更。
        }
        //Invoke("EffectEnd", Second);// エフェクト処理後自身を消す
    }

    // 破砕演出の終了
    void EffectEnd()
    {
        Destroy(this.gameObject);// 削除
    }

    void Update()
    {
    }
}
