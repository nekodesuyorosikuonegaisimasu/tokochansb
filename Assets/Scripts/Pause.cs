﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Pause : MonoBehaviour
{
    // ポーズした時に表示するUI
    [SerializeField]
    public GameObject pauseUI;
    // 操作説明UI
    [SerializeField]
    public GameObject operationUI;
    //プレイ画面ちゅうのみ表示するUI
    public GameObject[] PlayUI = new GameObject[7];
    // ポーズしたときに表示するテキスト
    public GameObject[] TextList;
    // 選択した番号
    public int number;
    // 選択している番号
    public int SelectNum;
    // ポーズボタンを押しているかの判定
    public bool pauseFlag;
    // シーン遷移の項目を選択したかの判定
    private bool sceneFlag;
    // スティックが一気に下に行かないためのクールタイム
    private float coolTime;
    // プレイヤーマネージャーを探す
    private GameObject playerManager;

    public int Shift;
    private float X;
    // オプション画面遷移時に使う
    public bool optionFlag = false;
    // 操作説明画面表示フラグ
    public bool operationFlag;

    private float Smoothing;

    void Awake()
    {
        pauseUI = GameObject.Find("PanelUI");
        operationUI = GameObject.Find("Method of operation");
        TextList = new GameObject[5];
        for (int i = 0; i < 5; i++) TextList[i] = GameObject.Find("Text" + i);
        pauseFlag = false;
        sceneFlag = false;
        operationFlag = false;
        number = -1;
        SelectNum = 0;
        coolTime = 0;
        Shift = 0;
    }

    // Start is called before the first frame update
    void Start()
    {
        operationUI.SetActive(false);
        pauseUI.SetActive(false);
        playerManager = GameObject.Find("PlayerManager");
    }

    // Update is called once per frame
    void Update()
    {
        // ポーズを開くボタンを押したとき
        if (((Input.GetButtonDown("Mouse_StartButton") || Input.GetButtonDown("Pad_StartButton")) && !sceneFlag && !operationFlag && !ClearManager.ClearFlag && !LoopEffectManage.LoopEffectFlag) || ControllerChecker.CheckConnected == 1)
        {
            // ステージシーンの途中以外でコントローラーが挿された時は反応しないようにする
            ControllerChecker.CheckConnected = 2;
            //　ポーズUIのアクティブ、非アクティブを切り替え
            pauseUI.SetActive(!pauseUI.activeSelf);
            //プレイ時のUIを非表示
            for(int i = 0; i < PlayUI.Length; i++) {if(PlayUI[i] != null) PlayUI[i].SetActive(!PlayUI[i].activeSelf); }
            //　ポーズUIが表示されてる時は停止(キャラ操作)
            if (pauseUI.activeSelf)
            {
                playerManager.GetComponent<PlayerManager>().pauseFlag = true;
                Time.timeScale = 0f;
                pauseFlag = true;
            }
            else
            {
                playerManager.GetComponent<PlayerManager>().pauseFlag = false;
                Shift = 0;
                Time.timeScale = 1f;
                SelectNum = 0;
                pauseFlag = false;
            }
            // 召喚時効果音の再生
            SoundManager.Instance.PlaySeByName("TitleSelect", this.gameObject);
        }
        // ポーズを閉じるボタンを押したとき
        else if ((Input.GetButtonUp("Mouse_Jump") || Input.GetButtonUp("Pad_Jump") || Input.GetButtonDown("Mouse_StartButton")) && pauseFlag && !operationFlag && !sceneFlag)
        {
            //プレイ時のUIを表示
            for (int i = 0; i < PlayUI.Length; i++) { if (PlayUI[i] != null) PlayUI[i].SetActive(!PlayUI[i].activeSelf); }
            //　ポーズUIのアクティブ、非アクティブを切り替え
            pauseUI.SetActive(!pauseUI.activeSelf);
            Shift = 0;
            Time.timeScale = 1f;
            SelectNum = 0;
            pauseFlag = false;
            // 召喚時効果音の再生
            SoundManager.Instance.PlaySeByName("TitleSelect", this.gameObject);
        }
        // 決定ボタンを押したとき
        if ((Input.GetButtonDown("Mouse_Fire1") || Input.GetButtonDown("Pad_Fire1")) && pauseFlag && !sceneFlag)
        {
            number = SelectNum;

            if (!operationFlag)
            {
                // 召喚時効果音の再生
                if (number != 0) SoundManager.Instance.PlaySeByName("StageSelectOK", this.gameObject);
            }
        }

        if (TimeSync())
        {
            switch (number)
            {
                case 0: //ゲームの再開
                    //プレイ時のUIを表示
                    for (int i = 0; i < PlayUI.Length; i++) { if (PlayUI[i] != null) PlayUI[i].SetActive(!PlayUI[i].activeSelf); }
                    playerManager.GetComponent<PlayerManager>().pauseFlag = false;
                    //　ポーズUIのアクティブ、非アクティブを切り替え
                    pauseUI.SetActive(!pauseUI.activeSelf);
                    number = -1;
                    Shift = 0;
                    Time.timeScale = 1f;
                    SelectNum = 0;
                    pauseFlag = false;
                    // 召喚時効果音の再生
                    SoundManager.Instance.PlaySeByName("TitleSelect", this.gameObject);
                    break;
                case 1:
                    sceneFlag = true;
                    if (!optionFlag)
                    {
                        if (SceneMove.Close())
                        {
                            optionFlag = true;
                            SceneManager.LoadScene("Option", LoadSceneMode.Additive);
                        }
                    }
                    if (optionFlag == true && SceneManager.sceneCount == 1)
                    {
                        if (SceneMove.Open2())
                        {
                            PlayDataManager.BGM.GetComponent<SoundManager>().Volume = (float)PlayDataManager.VolumeList[0] / 100.0f;
                            PlayDataManager.BGM.GetComponent<SoundManager>().BgmVolume = (float)PlayDataManager.VolumeList[1] / 100.0f;
                            PlayDataManager.BGM.GetComponent<SoundManager>().SeVolume = (float)PlayDataManager.VolumeList[2] / 100.0f;
                            optionFlag = false;
                            sceneFlag = false;
                            number = -1;
                        }
                    }
                    break;
                case 2:
                    if (!operationFlag)
                    {
                        operationUI.SetActive(true);
                        operationFlag = true;
                    }
                    break;
                case 3:
                    sceneFlag = true;
                    if (SceneMove.Close())
                    {
                        Time.timeScale = 1f;
                        SceneManager.LoadScene("StageSelect");
                    }
                    break;
                case 4:
                    sceneFlag = true;
                    if (SceneMove.Close2())
                    {
                        Time.timeScale = 1f;
                        SceneManager.LoadScene("Title");
                    }
                    break;
            }
        }

        // ポーズ画面の操作・文字関連の処理
        if (pauseFlag && !operationFlag)
        {
            X += 0.51f;
            switch (Shift)
            {
                case 0:
                    Status();
                    break;
                case 1:
                    Select();
                    break;
            }
        }

        if (operationFlag && (Input.GetButtonUp("Mouse_Jump") || Input.GetButtonUp("Pad_Jump")))
        {
            operationUI.SetActive(false);
            operationFlag = false;
            number = -1;
            // 召喚時効果音の再生
            SoundManager.Instance.PlaySeByName("TitleSelect", this.gameObject);
        }
    }
    void Status()
    {
        //テキストの位置を初期化する
        for (int i = 0; i < 5; i++)
        {
            TextList[i].GetComponent<RectTransform>().anchoredPosition = new Vector3(0, 150 -120 * i, 0);
            TextList[i].transform.localScale = new Vector3(1, 1, 1);
        }
        //選択中の項目のみ大きく表示する
        TextList[SelectNum].transform.localScale = new Vector3(1.3f, 1.3f, 1.3f);
        SoundManager.Instance.PlaySeByName("TitleSelect", gameObject);
        Shift = 1;
    }
    void Select()
    {
        //選択中の項目のみスイングするように
        TextList[SelectNum].GetComponent<RectTransform>().anchoredPosition = new Vector3(0,  150 - SelectNum * 120 + 10 * Mathf.Cos(X / 50), 0);


        if (Input.GetAxisRaw("Vertical") == 0 && Input.GetAxisRaw("Pad_D_V") == 0) coolTime = 0;
        if ((Input.GetAxisRaw("Vertical") > 0 || Input.GetAxisRaw("Pad_D_V") > 0 || Input.GetAxis("Mouse_ScrollWheel") > 0) && SelectNum > 0 && coolTime <= 0 && !sceneFlag)
        {
            coolTime++;
            SelectNum--;
            Shift = 0;
        }
        if ((Input.GetAxisRaw("Vertical") < 0 || Input.GetAxisRaw("Pad_D_V") < 0 || Input.GetAxis("Mouse_ScrollWheel") < 0) && SelectNum < 4 && coolTime <= 0 && !sceneFlag)
        {
            coolTime++;
            SelectNum++;
            Shift = 0;
        }
    }
    //fixedupdateでもtimescale = 0のupdateでも同じ動きが出来るようにする
    //TimeSyncとopen、closeを同時に使用すると動作がおかしくなるので注意
    bool TimeSync()
    {
        Smoothing += Time.unscaledDeltaTime;
        if (Smoothing >= Time.fixedUnscaledDeltaTime)
        {
            Smoothing = 0;
            return true;
        }
        return false;
    }
}


