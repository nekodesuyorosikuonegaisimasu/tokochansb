﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

// 主に敵の動作や当たり判定を扱うcs
public class LichManager : MonoBehaviour
{
    private Animator Animator;
    public GameObject Princess; // 姫
    private GameObject DropItemBox; // ドロップアイテムの親の空オブジェクト
    public GameObject dropItem; // ドロップアイテム
    public GameObject Player;   // プレイヤー
    private GameObject SummonPlayer;    // プレイヤー6の召喚物を入れる

    public int Shift;//行動の管理用

    public GameObject DamageEffect;

    public float Hp; // 体力
    public bool Dieflg;//死亡フラグ
    public float Length;//索敵範囲
    public int CoolTime;
    public int FireSpeed;//火の玉の速度
    public GameObject Magic;
    public GameObject Flame;
    public int FlameCount;//地面に生成する火の数

    public GameObject Target;
    private float PlayerDistance;
    private float PrincessDistance;
    public float TargetDistance;
    private Vector3 Poslog;
    private float Angle;//向く方向
    private float AngleReal;//向かせたい方向

    Attack attack; // Attack.cs

    // 攻撃関係判定
    bool DamageFlag;
    int Count; // 攻撃用で使うが、死ぬ時にも使う
    float HitCount; // 攻撃されるときに使う無敵時間
    private bool SummonFireFlag;
                    //private GameObject DropItem;



    void Start()
    {
        // Playerを探す
        Player = GameObject.Find("Player");
        attack = Player.GetComponent<Attack>();
       
        Animator = GetComponent<Animator>();
        Hp = PlayDataManager.ENEMY_STATUS[4,0];
        DamageFlag = true;  // DamageFlagの初期化
        Count = 0;       // カウントを0にしておく
        HitCount = 0;    // カウントを0にしておく
        FlameCount = 50;
        FireSpeed = 1000;
        if(Dieflg) Destroy(this.gameObject, 50); // エネミーが増えすぎないように
    }
    void Update()
    {
        // ポーズ中は操作を受け付けない
        if (Mathf.Approximately(Time.timeScale, 0f)) return;
     
        Search();
        if(Animator.GetCurrentAnimatorStateInfo(0).IsName("Idle") && Shift != 3)
        {
            Shift = 0;
            Animator.SetBool("Attack",false);
            Animator.SetBool("Hit",false);
            Animator.SetBool("Skill",false);
            GetComponent<NavMeshAgent>().speed = 5;
        }
    }
    void FixedUpdate()
    {
        switch (Shift)
        {
            case 0:
                Move();
                break;
            case 1:
                Attack();
                break;
            case 2:
                Skill();
                break;
                case 3:
                Die();
                break;
        }
        if(CoolTime-- <= 0)
            {
            CoolTime = 0;
        }
        // ここで無敵時間をやる
        if (HitCount-- <= 0)
        {
            HitCount = 0;
            DamageFlag = true;
        }
    }
    void Search()
    {
        if (Target == null)
        {
            TargetDistance = Length;
            //もし、PlayerまたはPrincessがいなかったら探す
            if (Player == null) Player = GameObject.Find("Player");
            if (Princess == null) Princess = GameObject.FindGameObjectWithTag("Princess");// 姫をtagを参照して取得
        }
        //プレイヤー、姫との距離を計算
        if (Target != null) TargetDistance = Vector3.Distance(Target.transform.position, transform.position);
        if (Princess != null) PrincessDistance = Vector3.Distance(Princess.transform.position, transform.position);
        if (Player != null) PlayerDistance = Vector3.Distance(Player.transform.position, transform.position);
        if (PlayerDistance <= TargetDistance && PlayerDistance < PrincessDistance)
        {
            Target = Player;
        }
        if (PrincessDistance <= TargetDistance && PlayerDistance >= PrincessDistance)
        {
            Target = Princess;
        }
        if (TargetDistance > Length) Target = null;
        if (Target != null)
        {
            Animator.SetBool("Walk", true);

        }
        if (Target == null)
        {
            Animator.SetBool("Walk", false);
        }
    }
     void Move()
    {
      
        
        
        //遠くにいるときは火を放つ
        if(TargetDistance >= 5.0f  && TargetDistance <= 12&& CoolTime == 0 && Shift != 3)
        {
            CoolTime = 30;
            Shift = 1;
            GetComponent<Animator>().SetBool("Attack",true);
            GetComponent<NavMeshAgent>().speed = 0;
        }

        //ターゲットが近くにいたら攻撃する
        if (TargetDistance < 5.0f && CoolTime == 0 && Shift != 3)
        {
            CoolTime = 150;
          Shift = 2;
            GetComponent<Animator>().SetBool("Skill",true);
            //攻撃中は動かないようにする
            GetComponent<NavMeshAgent>().speed = 0;
        }

        //ターゲットがいる場合
        if (Target != null)
        {
            //ターゲットに向かって移動する
            GetComponent<NavMeshAgent>().destination = Target.transform.position;

            //自身の座標とターゲットの座標から角度を算出
            Angle = Mathf.Atan2(Target.transform.position.z - transform.position.z, Target.transform.position.x - transform.position.x) * Mathf.Rad2Deg;
            //振り向く角度が180度を上回っていた場合
            if (Angle - AngleReal > 180) AngleReal = 360 + AngleReal;
            if (Angle - AngleReal < -180) AngleReal = -360 + AngleReal;
            //滑らかに方向転換できる関数を用意
            AngleReal += (Angle - AngleReal) / 20;
            //プレイヤーが移動していた場合向きを変更する
            if (transform.position.x != Poslog.x || transform.position.z != Poslog.z)
                transform.rotation = Quaternion.Euler(0, -AngleReal + 90.0f, 0);
            //プレイヤーの座標を更新
            Poslog = transform.position;
        }
    }
    public void Attack() // 攻撃をするとき
    {
        if(CoolTime%15 == 0)
        SoundManager.Instance.PlaySeByName("Fire", gameObject);
        
        GameObject NoralAttack =
         Instantiate(Magic, transform.transform.position + transform.forward + new Vector3(0, 1, 0),
         Quaternion.Euler(transform.rotation.eulerAngles.x,transform.rotation.eulerAngles.y + Random.Range(-15,15),transform.rotation.eulerAngles.z));
        NoralAttack.tag = "Enemy";
        NoralAttack.name = "Lich(Clone)";
        Shift = 1;
    }
    void Skill()
    {
        SoundManager.Instance.PlaySeByName("Summon Appear3", gameObject);

        for (int i = 0;i < FlameCount ; i++)
        {
           
            Vector3 FlamePos = new Vector3
                ((i/8+1)* Mathf.Cos(2 * Mathf.PI * (i%8)/8 +i/8),
                0,
                (i/8+1)* Mathf.Sin(2 * Mathf.PI * (i%8)/8+i/8));
            GameObject SkillAttack = Instantiate(Flame,transform.position+FlamePos,Quaternion.Euler(0,0,0));
            SkillAttack.tag = "Enemy";
            SkillAttack.transform.root.parent = transform;
            SkillAttack.name = "Lich(Clone)";
        }
        Shift = 0;
    }
   

    //攻撃に触れたらダメージ
    public void OnTriggerEnter(Collider collision)
    {
        if (Hp > 0)
        {
            if (DamageFlag)
            {
                // 通常攻撃ヒット
                if (collision.gameObject.CompareTag("NormalWeapon") || collision.gameObject.CompareTag("DollNormalWeapon"))
                {
                    // ヒット処理へ
                    Hit(Player.GetComponent<Player>().NattackPower);
                }
                // 必殺技ヒット
                if (collision.gameObject.CompareTag("SkillWeapon") || collision.gameObject.CompareTag("DollSkillWeapon"))
                {
                    // ヒット処理へ
                    Hit(Player.GetComponent<Player>().SattackPower);
                }
            }
        }

    }
   
    public void Hit(float Power) // 攻撃に当たった時
    {
        // エフェクトの生成
        Instantiate(DamageEffect, transform.position + transform.forward + transform.up, transform.rotation);
        // 召喚物
        if (Player == SummonPlayer)
        {
            Hp -= Power; // プレイヤーの攻撃力分減らす
        }
        // それ以外
        else
        {
            // ダメージ処理
            Hp -= Power * Player.GetComponent<Attack>().Item; // プレイヤーの攻撃力分減らす
        }
        
        if (Hp >= 1)
        {
            Animator.SetBool("Hit", true);
            DamageFlag = false;
            HitCount = 1;
        }
        else // Hpが0になったら死ぬ
        {Shift = 3;
         Animator.SetBool("Die",true);
        }
    }
    public void Die() // HPを0にされたら呼び出す
    {
        Animator.SetBool("Die", true);
        GetComponent<NavMeshAgent>().speed = 0;
        Count++;

        if (120 < Count) // 死ぬアニメーションを再生してから消える(タイミングなので120ではないかもしれない)
        {
            // アイテム出現
            DropItemBox = GameObject.Find("DropItemBox");// 親オブジェクトを取得

            GameObject CloneDropItem = Instantiate(dropItem, // 出現させるプレハブ名
                new Vector3(this.transform.position.x, this.transform.position.y + 1.0f, this.transform.position.z), // 少し高い位置にしたいので
                this.transform.rotation); // 角度は回転するので適当
            CloneDropItem.GetComponent<Rigidbody>().AddForce(transform.forward.x,150,transform.forward.z,ForceMode.Force);
            CloneDropItem.name = "DropItem"; // クローンしたオブジェクトの名前を変更
            CloneDropItem.transform.SetParent(DropItemBox.transform); // DropItemBoxを親に指定
            // デストロイ
            Destroy(this.gameObject); // アイテムを出現させたら消える
        }
    }
}