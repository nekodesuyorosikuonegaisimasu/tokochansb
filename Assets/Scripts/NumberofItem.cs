﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NumberofItem : MonoBehaviour
{
    // アイテムテキスト格納用オブジェクト
    public GameObject numItem;
    // アイテムUI
    GameObject ItemUI;
    // アイテムの数
    int number;

    // Start is called before the first frame update
    void Start()
    {
        ItemUI = GameObject.Find("ItemUI");
    }
    // Update is called once per frame
    void Update()
    {
        // 数値をみる
        number = ItemUI.GetComponent<SelectItem>().itemCount[ItemUI.GetComponent<SelectItem>().NowItem];
        // 数値を文字にする
        numItem.GetComponent<Text>().text = number.ToString();
    }
}
