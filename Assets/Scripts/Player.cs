﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    // アニメーション
    private Animator animator;
    // Rigidbody
    private Rigidbody m_rigidbody;
    // 攻撃したかの情報
    private Attack attack;
    // 移動
    private Vector3 input;
    // 移動に必要な力
    [SerializeField]
    private float moveForceMultiplier;
    // 接地判定
    public bool ground;
    // ジャンプ判定
    public bool jumpFlag;
    // スタン時間中か否か（Attack.csでも参照）
    public bool stunFlag;
    // 攻撃を受けた時の無敵時間
    public float invincibleTime;
    // 無敵時の点滅周期
    private float interval;
    public GameObject invisibleObj;
    // メインカメラを探す
    private GameObject mainCamera;
    // ゲーム画面のオブジェクト
    private GameObject Play;
    // 種オブジェクト
    public GameObject Seed;
    public bool IsTouchSeed;
    // 接触しているグラウンドオブジェクトの名前
    public bool IsStickGround;
    // 接触しているグラウンドオブジェクトの座標
    public float StartSeedPosY;

    // ステータス情報
    public float charaSpeed;
    private float jumpPower;
    public float NattackPower;
    public float SattackPower;
    public float StunSeconds;

    // Start is called before the first frame update
    void Start()
    {
        // 生成時に「Play」オブジェクトの子オブジェクトになる
        Play = GameObject.Find("Play");
        this.transform.parent = Play.transform;

        // アニメーションの取得
        animator = this.GetComponent<Animator>();
        // RigidBodyの取得
        m_rigidbody = this.GetComponent<Rigidbody>();
        // Attackの情報を持つ
        attack = this.GetComponent<Attack>();
        // 初期化
        jumpFlag = false;
        stunFlag = false;
        invincibleTime = 0;
        moveForceMultiplier = 10.0f;
        //選択したキャラクターに応じてステータスを変更      
        NattackPower = PlayDataManager.CHARA_STATUS[PlayDataManager.CharaNum, 0];
        SattackPower = PlayDataManager.CHARA_STATUS[PlayDataManager.CharaNum, 2];
        StunSeconds = 10 - PlayDataManager.CHARA_STATUS[PlayDataManager.CharaNum, 4] / 3;
        charaSpeed = PlayDataManager.CHARA_STATUS[PlayDataManager.CharaNum, 5];
        jumpPower = 10 * PlayDataManager.CHARA_STATUS[PlayDataManager.CharaNum, 6];

        mainCamera = GameObject.Find("Main Camera");
        // 種に触れているフラグの初期化
        IsTouchSeed = false;
    }

    // Update is called once per frame
    void Update()
    {
        // ポーズ中は操作を受け付けない
        if (Mathf.Approximately(Time.timeScale, 0f)) return;

        if (!stunFlag)
        {
            // 左スティックで動くようにする
            input.x = Input.GetAxisRaw("Horizontal");
            input.y = 0;
            input.z = Input.GetAxisRaw("Vertical");
            // ジャンプをしていないかつ接地しているなら
            if (!jumpFlag && ground)
            {
                if (ControllerChecker.CheckController) jumpFlag = Input.GetButtonDown("Pad_Jump");
                else jumpFlag = Input.GetButtonDown("Mouse_Jump");
            }
        }

        // 壁をすり抜けた時のゲームオーバー遷移処理
        if (transform.position.y < -50)
        {
            GameOverManager.ReasonNum = 6;
            SceneManager.LoadScene("GameOver");
        }

        // 種が格納されているなら
        if (Seed != null) SeedTransform();
    }

    void FixedUpdate()
    {
        // 移動処理
        Vector3 forward = Vector3.Scale(UnityEngine.Camera.main.transform.forward, new Vector3(1, 0, 1)).normalized;
        Vector3 direction = forward * input.z + UnityEngine.Camera.main.transform.right * input.x;  //  テンキーや3Dスティックの入力（GetAxis）があるとdirectionに値を返す
        Vector3 move = direction * charaSpeed;
        Vector3 vel = new Vector3(m_rigidbody.velocity.x, 0, m_rigidbody.velocity.z);

        // スタン中でないなら
        if (!stunFlag)
        {
            // 接地しているなら
            if (ground)
            {
                // キャラクターに力を加える
                Vector3 mult = moveForceMultiplier * (move - vel);
                if (mult.magnitude > 0.001f)
                {
                    m_rigidbody.AddForce(moveForceMultiplier * (move - vel));
                }

                // 移動した場合方向転換（ベクトルの大きさが少しでも変わったなら）
                if (direction.magnitude > 0.01f)
                {
                    // directoinのxとz方向にキャラクターを回転させる
                    transform.rotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
                    // 走るアニメーションの再生
                    animator.SetBool("Running", true);
                }
                // 移動していない場合
                else if (input == Vector3.zero)
                {
                    animator.SetBool("Running", false);
                }
                // ジャンプ処理
                if (jumpFlag)
                {
                    m_rigidbody.AddForce(transform.up * jumpPower);
                    jumpFlag = false;
                }
            }
            else
            {
                // キャラクターに力を加える
                m_rigidbody.AddForce(move - vel);

                // 移動した場合方向転換（ベクトルの大きさが少しでも変わったなら）
                if (direction.magnitude > 0.01f)
                {
                    // directoinのxとz方向にキャラクターを回転させる
                    transform.rotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
                }
            }
        }
        // 無敵時間の処理
        if (invincibleTime > 0)
        {
            // 無敵時間中
            invincibleTime -= Time.fixedDeltaTime;
            interval = (Mathf.Sin(Time.time) / 2 + 0.5f) * 5 + 1;
            if ((int)interval % 2 == 0) invisibleObj.SetActive(true);
            if ((int)interval % 2 == 1) invisibleObj.SetActive(false);
        }
        if (invincibleTime < 0)
        {
            // 無敵時間終了
            invisibleObj.SetActive(true);
            interval = 0;
            invincibleTime = 0;
        }

    }

    void OnCollisionStay(Collision other)
    {
        // 地面との当たり判定
        if (other.gameObject.CompareTag("Ground"))
        {
            ground = true;
            // ジャンプのアニメーションをオフにする
            animator.SetBool("Jumping", false);
        }
        //// 地面にふれているのでフラグを立てる
        //if (other.gameObject.name.Contains("Terrain")) GroundPos = other.gameObject.transform;
    }

    // 接地していないなら（ジャンプ中なら）
    void OnCollisionExit(Collision other)
    {
        if (PlayDataManager.CharaNum != 4)
        {
            ground = false;
            animator.SetBool("Running", false);
            // ジャンプのアニメーションをオフにする
            if (jumpFlag)
            {
                animator.SetBool("Jumping", true);
            }
        }
    }
    // Colliderに触れた時の当たり判定
    void OnTriggerEnter(Collider other)
    {
        // 敵との当たり判定
        if (other.gameObject.CompareTag("Enemy") && invincibleTime == 0 && attack.NormalCoolTime == 0.0f && attack.SPCoolTime == 0.0f && !stunFlag)
        {
            m_rigidbody.constraints = RigidbodyConstraints.FreezePositionY;
            // ダメージ処理
            animator.SetBool("Hitting", true);
            animator.SetBool("Flinching", true);
            // スタン中でないなら
            // キャラ毎のスタン時間+数秒無敵
            invincibleTime = StunSeconds * 1.5f;
            // カメラを揺らす処理
            mainCamera.GetComponent<Camera>().Shake(0.02f, 0.5f);
            // ループできないようにする
            PlayerLoopButton.IsLoop = false;
            StartCoroutine("Move");
        }
        if (other.gameObject.CompareTag("BossAttack") && invincibleTime == 0)
        {
            // ダメージ処理
            animator.SetBool("Hitting", true);
            animator.SetBool("Flinching", true);
            // スタン中でないなら
            // キャラ毎のスタン時間+数秒無敵
            invincibleTime = StunSeconds * 1.5f;
            StartCoroutine("Move");
        }
        // 種との当たり判定中なら
        if (other.gameObject.name.Contains("Plant") && !other.gameObject.GetComponent<LoopObj>().IsPlant) IsTouchSeed = true;
    }
    // Colliderと接触中の処理
    void OnTriggerStay(Collider other)
    {
        // 種を格納する
        if (other.gameObject.name.Contains("Plant") && attack.IsHoldingSeed && Seed == null)
        {
            StartSeedPosY = other.gameObject.transform.position.y;
            Seed = other.gameObject;
        }
        if (other.gameObject.CompareTag("Seed")) IsStickGround = true;
    }
    // Colliderから離れた時の当たり判定
    void OnTriggerExit(Collider other)
    {
        // 種との当たり判定中に種から離れたら
        if (other.gameObject.name.Contains("Plant")) IsTouchSeed = false;
        // 地面に触れていないのでフラグを降ろす
        IsStickGround = false;
    }

    // スタン時の処理
    IEnumerator Move()
    {
        // 操作をできなくする
        stunFlag = true;
        m_rigidbody.constraints = RigidbodyConstraints.FreezePositionX;
        m_rigidbody.constraints = RigidbodyConstraints.FreezePositionZ;
        // キャラ毎のスタン時間動けない
        yield return null; //1フレーム待機
        yield return new WaitForSeconds(StunSeconds);
        // ダメージを受けるアニメーションをオフにする
        animator.SetBool("Hitting", false);
        animator.SetBool("Flinching", false);
        // 操作できるようにする
        m_rigidbody.constraints = RigidbodyConstraints.FreezeRotation;
        stunFlag = false;
        // ループできるようにする
        PlayerLoopButton.IsLoop = true;
        // コルーチンの終了
        yield break;
    }

    // 種の座標を決める
    private void SeedTransform()
    {
        // 種の位置をプレイヤーの前にする
        Seed.transform.position = this.gameObject.transform.position + transform.forward;
    }
}