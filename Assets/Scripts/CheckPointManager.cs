﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPointManager : MonoBehaviour
{
    // ルートを入れる
    [SerializeField]
    private GameObject[] route;
    //ルートごとの見えない壁
    [SerializeField]
    private GameObject[] Wall;

    // 姫が通るルート
    [SerializeField]
    private int nowPrincessRoute;
    // 作ったルートの数を数える
    private int i = 0;
    // 姫のルート保存
    public static int princessRoute;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
    }

    // プレイヤー生成より前に行われる
    public void InstanceCheckPoint()
    {
        // ステージセレクトまたはゲームオーバーからきた場合
        if (SceneMove.SceneName == "StageSelect" ||SceneMove.SceneName == "GameOver")
        {
            i = 0;
            // 配列の中身の数分iを足していく
            for (int j = 0; j < route.Length; j++)
            {
                i++;
            }
            // 選んだステージをもとにルートを選択
            nowPrincessRoute = PlayDataManager.StageNum - 1 - (3 * (PlayDataManager.WorldStageNum - 1));
            // ルートを記録
            princessRoute = nowPrincessRoute;
            // 選択されたルートじゃないルートを非表示
            for (int k = 0; k < i; k++)
            {
                if (k != nowPrincessRoute)
                {
                    route[k].SetActive(false);
                    Wall[k].SetActive(false);
                }
            }
        }
        else //デバック用
        {
            i = 0;
            // 配列の中身の数分iを足していく
            for (int j = 0; j < route.Length; j++)
            {
                i++;
            }
            // 選んだステージをもとにルートを選択
            //nowPrincessRoute = PlayDataManager.StageNum - 1 - (5 * (PlayDataManager.WorldStageNum - 1));
            //Debug.Log(nowPrincessRoute);

            /*デバッグ用*/
            nowPrincessRoute = 2;

            // ルートを記録
            princessRoute = nowPrincessRoute;
            // 選択されたルートじゃないルートを非表示
            for (int k = 0; k < i; k++)
            {
                if (k != nowPrincessRoute)
                {
                    route[k].SetActive(false);
                    Wall[k].SetActive(false);
                }
            }
        }
    }
}
