﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoopObj : MonoBehaviour
{
    public GameObject[] gameobjects;    // 設定するゲームオブジェクト用の配列
    public GameObject ObstaclesObj;     // 姫を止めるための範囲

    public int Loop_Cnt;               // ループした回数
    public int NoneAlt_Num;             // 以降変化しないX回目
    public bool IsPlant;                // 木が自身のプレハブ内に設定されているかどうか
    public bool IsSeed;                 // 種
    public bool IsGrow;                 // 生えない
    public bool StopNull;               //Nullで止まる時用


    void Start()
    {
        Init();         //初期化
        Gene_Obj();     //オブジェクトの生成
    }

    //初期化
    void Init()
    {
        Loop_Cnt = 0;  //ループした回数
        IsSeed = false; 
        IsGrow = false;
    }

    //オブジェクトの生成
    void Gene_Obj()
    {
        for (int i = 0; i < gameobjects.Length; i++)//設定したオブジェクトの個数分生成
        {
            //親
            var parent = this.transform;
            //生成
            gameobjects[i] = Instantiate(gameobjects[i], this.transform.position, gameobjects[i].transform.localRotation, parent);

            
            //名前をつける。
            switch (gameobjects[i].name)
            {
                case "Rock_Broken(Clone)":     //壊れる岩
                    gameobjects[i].name = "type[" + 1 + "]"; //type[1]
                    break;  
                case "Rock_Normal(Clone)":     //普通の岩
                    gameobjects[i].name = "type[" + 2 + "]"; //type[2]
                    break; 
                case "Seed(Clone)":            //苗木
                    gameobjects[i].name = "type[" + 3 + "]"; //type[3]
                    IsSeed =true;
                    break;
                case "Rock_Move(Clone)":      //苗木
                    gameobjects[i].name = "type[" + 4 + "]"; //type[4]
                    break;
                case "Tree(Clone)":            //苗木
                    gameobjects[i].name = "type[" + 5 + "]"; //type[5]
                    break;
                case "Null(Clone)":            //Null
                    gameobjects[i].name = "type[" + 6 + "]"; //type[6]
                    break;
                default:    
                    gameobjects[i].name = "type[ERROR]";     //type[0]
                    break; 
            }
            
            //生成物に名前をつける。
            //gameobjects[i].name = "Obj[" + i + "]";
            //条件：最初の生成   対象：オブジェクト[0]                  処理：アクティブ　ON
            if (i < 1) gameobjects[i].SetActive(true);
            //条件：最初以降     対象：オブジェクト[i]                  処理：アクティブ　OFF
            else gameobjects[i].SetActive(false);

        }
        //初め何もなかった時用
        if (!StopNull && gameobjects[0].name == "type[4]" && ObstaclesObj != null)
        { ObstaclesObj.GetComponent<StopPrincess>().GoPrinces(); }
    }

    //アクティブを切り替える関数
    void Active_Alt()
    {
        // 子オブジェクトが移動した際、親である自身も同じ位置に移動する
        this.transform.position = new Vector4(gameobjects[Loop_Cnt - 1].transform.position.x, this.transform.position.y, gameobjects[Loop_Cnt - 1].transform.position.z);
        // 子オブジェクトが移動位置を初期化
        gameobjects[Loop_Cnt - 1].transform.position = new Vector4(this.transform.position.x, this.transform.position.y, this.transform.position.z);
        //this.transform.position = gameobjects[Loop_Cnt - 1].transform.position;

        // ループの区切りで初期化
        if (Loop_Cnt >= gameobjects.Length) Loop_Cnt = 0;

        // ループの区切りで、配列に格納されている最後のオブジェクトを非アクティブにする
        if (Loop_Cnt == 0) gameobjects[gameobjects.Length - 1].SetActive(false);

        // 配列に格納されているアクティブなオブジェクトを非アクティブにする
        else gameobjects[Loop_Cnt - 1].SetActive(false);

        // ループ回数に該当したオブジェクトをアクティブにする
        gameobjects[Loop_Cnt].SetActive(true);

        if (!StopNull)
        {
            //前回何もなかった時
            if (Loop_Cnt != 0)
            {
                if (gameobjects[Loop_Cnt - 1].name == "type[6]" && ObstaclesObj != null)
                { ObstaclesObj.GetComponent<StopPrincess>().StopPrinces(); }
            }
            else
            {
                if (gameobjects[gameobjects.Length - 1].name == "type[6]" && ObstaclesObj != null)
                { ObstaclesObj.GetComponent<StopPrincess>().StopPrinces(); }
            }
            //ループ中で何もないオブジェクトになった時
            if (gameobjects[Loop_Cnt].name == "type[6]" && ObstaclesObj != null)
            { ObstaclesObj.GetComponent<StopPrincess>().GoPrinces(); }
        }


    }

    //入力
    public void PushButton()
    {
        // 木に関する設定確認。
        Set_Plant();

        

        // 指定回数の入力以前,あるいは回数設定をしていないのであれば。
        if ((Loop_Cnt < NoneAlt_Num) && NoneAlt_Num != 99 || NoneAlt_Num == 0&&NoneAlt_Num != 99)
        {
            // ループカウンタ
            Loop_Cnt++;
            // アクティブ切り替えを行う
            Active_Alt();
        }            
    }

    // 配列の中身をチェック
    void Check()
    {

        for (int i = 0; i < gameobjects.Length; i++)// 設定したオブジェクトの個数分チェック
        {
            if (gameobjects[i] == null)// 中身が入っていないなら
            {
                Debug.Log("配列内のオブジェクトが削除されました");
                //姫の動きを再開可能か送る
                if(ObstaclesObj != null) ObstaclesObj.GetComponent<StopPrincess>().GoPrinces();
                Destroy(this.gameObject);// このオブジェクトを削除
            }
        }
    }

    //当たり判定
    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Tree")&& !IsSeed)// 苗木と当たったなら
        {
            Debug.Log("Treeと当たりました");
            Destroy(gameobjects[Loop_Cnt]);// このオブジェクトを削除する。            
        }
        Check();
    }
    public void ExistNearPlayer(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))// 苗木と当たったなら
        {
            IsGrow = true;
        }
    }
    public void Not_ExistNearPlayer(Collider other)
    {       
            IsGrow = false;
    }
    // 苗木設定
    void Set_Plant()
    {
        // 種を植えていないなら
        if (!IsPlant && IsSeed) NoneAlt_Num = 99;// ループをしない。
        // 種を植えているなら
        if(IsSeed && !IsGrow && IsPlant) NoneAlt_Num = 1;   // ループする。
    }
    void DebugLoop()
    {
        //if ((Input.GetKeyDown(KeyCode.RightShift)/*デバッグ用*/|| Input.GetAxis("Pad_R2") > 0))// && !LoopEffectManage.LoopEffectFlag && PlayerLoopButton.IsLoop
        //{
        //    PushButton();
        //}
    }
  
    void Update()
    {
        DebugLoop();
        // 確認
        Check();
        // 位置を送っておく
        if (gameobjects[Loop_Cnt] != null && ObstaclesObj!= null)ObstaclesObj.GetComponent<StopPrincess>().PositionHoming(gameobjects[Loop_Cnt].gameObject.transform);
    }
}
