﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoolTimeUI : MonoBehaviour
{
    // クールタイムのUI
    public GameObject objFront;
    public GameObject objBack;
    // イメージを取得
    public Image UIobj;
    // ループフラグ
    public bool roop;
    // クールタイム
    public float countTime;
    // プレイヤー
    GameObject player;
    bool attackFlag;

    public float maxCoolTimeRange;
    // ゲーム画面のオブジェクト
    private GameObject Play;

    // Start is called before the first frame update
    void Start()
    {
        // 生成時に「Play」オブジェクトの子オブジェクトになる
        Play = GameObject.Find("Play");
        this.transform.parent = Play.transform;
        // プレイヤーを探す
        player = GameObject.Find("Player");
        attackFlag = false;
    }

    // Update is called once per frame
    void Update()
    {
        // ポーズ中は操作を受け付けない
        if (Mathf.Approximately(Time.timeScale, 0f)) return;

        // クールタイム中なら描画を減らしていく
        if (roop && player != null)
        {
            // アイテムクールタイム
            if(objFront.GetComponent<RectTransform>().anchoredPosition.x == 650)
            {
                UIobj.fillAmount -= 1.0f / countTime * Time.deltaTime * (player.GetComponent<Attack>().itemCoolTime / player.GetComponent<Attack>().itemCoolTime);
            }
            // 通常攻撃・必殺技クールタイム
            else
            {
                // プレイヤー6の召喚時にはクールタイムは減らない
                if(PlayDataManager.CharaNum == 6 && player.GetComponent<Player>().enabled == false && objFront.GetComponent<RectTransform>().anchoredPosition.x == 320)
                {}
                else
                {
                    UIobj.fillAmount -= 1.0f / countTime * Time.deltaTime * player.GetComponent<Attack>().itemCoolTime;
                }
            }
            // クールタイムが終わったなら
            if (UIobj.fillAmount == 0)
            {
                // オブジェクトを消す
                Destroy(this.gameObject);
            }
        }
        else if(!roop && player != null)
        {
            // プレイヤー「人形遣い」
            if(PlayDataManager.CharaNum == 3)
            {
                // 人形を出せるゲージが徐々に回復
                UIobj.fillAmount += 1.0f / 100 * Time.deltaTime * player.GetComponent<Attack>().itemCoolTime;
                // 攻撃時のクールタイム処理
                if ((Input.GetButtonDown("Mouse_Fire1") || Input.GetButtonDown("Pad_Fire1")))
                {
                    DollAttack();
                }
                // 攻撃できるか判別
                if(player.GetComponent<Attack>().NormalCoolTime == 0.0f)
                {
                    attackFlag = true;
                }
                else attackFlag = false;
            }
            // プレイヤー「ドラゴン」
            if(PlayDataManager.CharaNum == 4)
            {
                // ドラゴンが火を吐けるゲージが徐々に回復
                UIobj.fillAmount += 1.0f / 30 * Time.deltaTime * player.GetComponent<Attack>().itemCoolTime;
                if ((Input.GetButton("Mouse_Fire2") || Input.GetButton("Pad_Fire2")))
                {
                    if(PlayDataManager.CharaNum == 4) DragonAttack();
                }
            }
        }
        else PlayerChange();
    }
    // 人形を生成したときのクールタイム処理
    public void DollAttack()
    {
        if(UIobj.fillAmount > maxCoolTimeRange / PlayDataManager.CHARA_STATUS[3,1] && attackFlag)
        {
            // クールタイム（ゲージを減らす）
            UIobj.fillAmount -= maxCoolTimeRange / PlayDataManager.CHARA_STATUS[3,1];
        }
    }
    // ドラゴンが火を噴く時のクールタイム処理
    public void DragonAttack()
    {
        if (UIobj.fillAmount > 0)
        {
            // クールタイム（ゲージを減らす）
            UIobj.fillAmount -= countTime * Time.deltaTime * (player.GetComponent<Attack>().itemCoolTime / player.GetComponent<Attack>().itemCoolTime) / 50;
        }
    }

    // クールタイムの座標を決める
    public void Count(float time, float PosX, float PosY)
    {
        // クールタイムの時間を代入
        countTime = time;
        // クールタイムが回るようにする（人形使い、ドラゴンを除く）
        roop = true;
        if (PlayDataManager.CharaNum == 3 && PosX == 150) roop = false;
        if (PlayDataManager.CharaNum == 4 && PosX == 320) roop = false;
        // クールタイマーを受け取った座標へ移動させる
        objFront.GetComponent<RectTransform>().anchoredPosition = new Vector3(PosX, PosY, 0);
        objBack.GetComponent<RectTransform>().anchoredPosition = new Vector3(PosX, PosY, 0);
        objFront.SetActive(true);
        objBack.SetActive(true);

        maxCoolTimeRange = UIobj.fillAmount;
    }
    // プレイヤー切り替え時の処理
    public void PlayerChange()
    {
        Destroy(this.gameObject);
    }
}
