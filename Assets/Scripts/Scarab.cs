﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[DisallowMultipleComponent]
public class Scarab : MonoBehaviour
{
    private int LifeTimer;
    private GameObject Princess;
    public GameObject Effect;
    // Start is called before the first frame update
    void Start()
    {
        if (gameObject.name == "AttackBall(Clone)")
        {
            Princess = GameObject.FindGameObjectWithTag("Princess");// 姫をtagを参照して取得
            LifeTimer = 600;
        }
    }
    void FixedUpdate()
    {
        //一定時間後に消えるようにする
        if (gameObject.name == "AttackBall(Clone)")
        {
            LifeTimer--;
            //しばらくしたらエフェクトを表示
            if (LifeTimer <= 120)
            {
                Effect.SetActive(true);
                if(LifeTimer == 120)
                {
                    // 効果音再生
                    SoundManager.Instance.PlaySeByName("Summon10BallSE", this.gameObject);
                } 
            }
            if(LifeTimer <= 90)transform.localScale = transform.localScale *0.95f;
            if (LifeTimer <= 0)
            {
                //姫のHPを回復してオブジェクトを消す
                Princess.GetComponent<PrincessManager>().PHp += (transform.childCount -2) * 15;
                Debug.Log((transform.childCount - 2) * 15);
                Destroy(gameObject);
            }
        }
    }
    //敵に触れたら子供にする
    private void OnTriggerEnter(Collider other)
    {
       //ヒットしたオブジェクトのタグがエネミーだったら
        if (other.gameObject.CompareTag("Enemy"))
        {
            //エネミーのタグのついたオブジェクトの親を参照
            GameObject Enemy = other.transform.root.gameObject;
            //親の名前がAttackBall(Clone)じゃなければ物理判定を付与する
            if(other.transform.root.gameObject.name != "AttackBall(Clone)")
            Enemy.AddComponent<SphereCollider>().center = new Vector3(0, 0.4f, 0);
            //Vector3 Dir;
            //float Dis;
            //Physics.ComputePenetration(this.GetComponent<SphereCollider>(), this.transform.position, this.transform.rotation,
            //    Enemy.GetComponent<SphereCollider>(), Enemy.transform.position, Enemy.transform.rotation, out Dir, out Dis);
            //Enemy.transform.position += Dir*1.1f;
            //エネミーの不要なスクリプト、コンポーネントを削除する
            Destroy(Enemy.GetComponent<EnemyManager>());
            Destroy(Enemy.GetComponent<NavMeshAgent>());

            Destroy(Enemy.GetComponent<LichManager>());
            //誤作動防止のためタグを変えておく
            Enemy.tag = "Ground";
            //くっついたエネミー自身にも敵がくっつくようにする
            Enemy.AddComponent<Scarab>();
            //エネミーをボールの子オブジェクトにする
            Enemy.transform.parent = transform;
        } 
    }
  
}
