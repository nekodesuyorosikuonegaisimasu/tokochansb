﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HowToImage : MonoBehaviour
{
    public GameObject Controller;
    public GameObject Mouse;

    // Update is called once per frame
    void Update()
    {
        //チェックコントローラーがfalseのときにコントローラー操作が行われたら画像を切り替える
        if(Mouse.activeSelf)
        {
            if(ControllerChecker.CheckController == true)
            {
                Mouse.SetActive(false);
                Controller.SetActive(true);
            }
        }
        //チェックコントローラーがtrueのときにコントローラー以外の操作が行われたら画像を切り替える
        if(Controller.activeSelf)
        {
            if(ControllerChecker.CheckController == false)
            {
                Mouse.SetActive(true);
                Controller.SetActive(false);
            }
        }
    }
}
