﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EndRollManager : MonoBehaviour
{
    public GameObject EndingText;//ストーリーのテキスト
    public GameObject CreditText;//クレジットのテキスト
    public GameObject BlindBack;//ストーリー再生からクレジットに切り替わる際の背景

    private int Shift;

    private int ScrollY;
    // Start is called before the first frame update
    void Start()
    {
        //リソースからストーリーとクレジットのテキストファイルを検索、代入し、ちょうどいい感じの場所に配置する
        EndingText.GetComponent<Text>().text = (Resources.Load("EndStory") as TextAsset).text;
        EndingText.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, -EndingText.GetComponent<RectTransform>().sizeDelta.y / 2);
        CreditText.GetComponent<Text>().text = (Resources.Load("EndRoll") as TextAsset).text;
        CreditText.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, -CreditText.GetComponent<RectTransform>().sizeDelta.y / 2);

        if (SceneMove.SceneName == "Title")BlindBack.GetComponent<Image>().color = new Color(0,0,0,1);
        Shift = 2;
    }
    
    void FixedUpdate()
    {
        switch(Shift)
        {
            //ストーリーの再生
            case 0:Ending();
                break;
                //クレジットの再生
            case 1:EndRoll();
                break;
                //シーンにやってきたときの動き
            case 2:SceneOpen();
                break;
                //シーンを去るときの動き
            case 3:NextScene();
                break;
        }
        
    }
    void Ending()
    {
       
        ScrollY+= 5;
        EndingText.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, -EndingText.GetComponent<RectTransform>().sizeDelta.y / 2 + ScrollY);
        if (ScrollY >=2000)
        {
            SoundManager.Instance.BgmVolume -= 0.005f;
            BlindBack.GetComponent<Image>().color = BlindBack.GetComponent<Image>().color + new Color(0, 0, 0, (float)1/255);
            if (BlindBack.GetComponent<Image>().color.a >= 1)
            {
                SoundManager.Instance.BgmVolume = (float)PlayDataManager.VolumeList[1]/100;
                ScrollY = 0;
                Shift = 1;
                SoundManager.Instance.PlayBgmByName("EndRoll", "Main");
            }
        }

    }
    void EndRoll()
    {
        
        if(ScrollY < CreditText.GetComponent<RectTransform>().sizeDelta.y)
        {
            ScrollY += 5;
            if (Input.anyKey) ScrollY += 10;
        }
        else
        {
            if (Input.anyKey) Shift = 3;
        }
        CreditText.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, -CreditText.GetComponent<RectTransform>().sizeDelta.y / 2 + ScrollY);
    }
    void SceneOpen()
    {
        if (SceneMove.Open3())
        {
            if (SceneMove.SceneName == "Title") 
            {
                SoundManager.Instance.PlayBgmByName("EndRoll", "Main");
                Shift = 1;
            }
            else
            {
                SoundManager.Instance.PlayBgmByName("Story", "Main");
                Shift = 0;
            }
        }
    }
    void NextScene()
    {
        //// マウスキーボード操作ならカーソルを表示する
        //if(!ControllerChecker.CheckController)Cursor.visible = true;
        if (SceneMove.Close3()) SceneManager.LoadScene("Title");
    }
}
