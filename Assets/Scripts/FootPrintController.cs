﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// 姫の足跡を扱うcs
public class FootPrintController : MonoBehaviour
{
    public int step; // 徐々に消えるための変数
    void Start()
    {
        step = 200; // 長くなく遅くなく200に設定
        StartCoroutine(Disappearing()); // コルーチン開始
    }

    IEnumerator Disappearing()
    {
        for (int i = 0; i < step; i++)
        {
            GetComponent<MeshRenderer>().material.color = new Color(1, 1, 1, 1 - 1.0f * i / step); // 徐々に透明度を上げていく
            yield return new WaitForSeconds(0.1f); // 0.1fに1回呼ばれるようにする(stepが200なので20秒で消える)
        }
        Destroy(gameObject); // 消す
    }
}
