﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// 名前を変更するクラス
public class AltName : MonoBehaviour
{   
    //トリガー
    public GameObject TriggerObj;
    public GameObject Efects_Star;
    private GameObject Exhibits;
    private BoxCollider component;

    private void Start()
    {
        var parent = gameObject.transform;
        Exhibits = Instantiate(TriggerObj, new Vector3(transform.position.x, transform.position.y - 15, transform.position.z), TriggerObj.transform.localRotation, parent);
        Efects_Star = Instantiate(Efects_Star, new Vector3(transform.position.x, transform.position.y - 5, transform.position.z), Efects_Star.transform.localRotation, Exhibits.transform);

        //名前をつける。
        switch (Exhibits.name)
        {
            case "Rock_Broken(Clone)":     //壊れる岩
                Exhibits.name = "type[" + 1 + "]"; //type[1]
                break;
            case "Rock_Normal(Clone)":     //普通の岩
                Exhibits.name = "type[" + 2 + "]"; //type[2]
                break;
            case "Seed(Clone)":            //苗木
                Exhibits.name = "type[" + 3 + "]"; //type[3]
                break;
            case "Rock_Broken(Move)":      //壊れる岩
                Exhibits.name = "type[" + 4 + "]"; //type[4]
                break;
            case "Tree(Clone)":            //木
                Exhibits.name = "type[" + 5 + "]"; //type[5]
                break;
            default:
                Exhibits.name = "type[ERROR]";     //type[0]
                break;
        }
        
        component = Exhibits.GetComponent<BoxCollider>();       
        component.enabled = false; 
        Efects_Star.transform.localScale /= 15;
        Exhibits.transform.localScale /= 2;
    }
    //当たり判定時
    private void OnTriggerEnter(Collider other)
    {
        // トリガーと当たったなら自身の名前をOPENに変更
        if(other.gameObject.name == Exhibits.name) gameObject.name = "Open";
    }

    private void Update()
    {
        // トリガーがフィールドにいないなら自身の名前をOPENに変更
        if(TriggerObj == null) gameObject.name = "Open";
        if(Exhibits.name != "type[1]" && Exhibits.name != "type[2]" && Exhibits.name != "type[4]")
        {
            Exhibits.transform.Rotate(new Vector3(0, Time.deltaTime * 50, 0));
        }      
        else Exhibits.transform.Rotate(new Vector3(0, 0, Time.deltaTime * 50));
        Exhibits.transform.position = new Vector3(transform.position.x, transform.position.y - 1.5f, transform.position.z);
    }
}
