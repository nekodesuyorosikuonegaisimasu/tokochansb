﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

// 主に敵の動作や当たり判定を扱うcs
public class EnemyManager : MonoBehaviour
{
    private Animator animator; // アニメーション
    private NavMeshAgent Enemy;

    public GameObject Princess; // 姫
    private float PrincessDistance; // 姫までの距離を測る
    private GameObject DropItemBox; // ドロップアイテムの親の空オブジェクト
    public GameObject dropItem; // ドロップアイテム
    private int RandomDropNumber;
    public GameObject Player;   // プレイヤー
    private float PlayerDistance; // プレイヤーまでの距離を測る
    public GameObject Body;     // 当たり判定用に敵の子オブジェクトのBody
    [SerializeField]
    private GameObject DamageEffect;    // 被ダメージエフェクト
    [SerializeField]
    private GameObject DieEffect;       // 消滅エフェクト
    private GameObject DeadEffect;      // 消滅エフェクトを入れる

    public float Hp; // 体力
    public float Atk; // 攻撃力

    Attack attack; // Attack.cs

    // 攻撃関係判定
    bool AttackFlag;
    bool DamageFlag;
    int Count; // 攻撃用で使うが、死ぬ時にも使う
    float HitCount; // 攻撃されるときに使う無敵時間
    //private GameObject DropItem;
    public bool DieFlag; 
    private bool SummonFireFlag;

    

    void Start()
    {
        // Playerを探す
        Player = GameObject.Find("Player" + (PlayerChangeManager.ChangeCount));
        // アニメーションの取得
        animator = GetComponent<Animator>();
        // ナビメッシュの取得
        Enemy = GetComponent<NavMeshAgent>();

        AttackFlag = false; // AttackFlagの初期化
        DamageFlag = true;  // DamageFlagの初期化
        Count = 0;       // カウントを0にしておく
        HitCount = 0;    // カウントを0にしておく
        if(DieFlag) Destroy(this.gameObject, 50); // エネミーが増えすぎないように

        for (int i = 0; i < PlayDataManager.ENEMY_NAME.Length; i++)
        {
            if (Enemy.name == PlayDataManager.ENEMY_NAME[i])
            {
                Hp = PlayDataManager.ENEMY_STATUS[i, 0] * Random.Range(0.9f,1.2f);
                Atk = PlayDataManager.ENEMY_STATUS[i,1] * Random.Range(0.9f,1.2f);
            }
        }
    }
    void FixedUpdate()
    {
        //もし、PlayerまたはPrincessがいなかったら探す
        if (Player == null) Player = GameObject.Find("Player");
        // Princessを探す
        Princess = GameObject.Find("Princess" + "(Clone)");
        if (Princess == null) Princess = GameObject.FindGameObjectWithTag("Princess"); // 姫をtagを参照して取得

        // ターゲット(プレイヤーと姫)の距離を計算
        PrincessDistance = Vector3.Distance(Princess.transform.position, transform.position);
        PlayerDistance = Vector3.Distance(Player.transform.position, transform.position);

        Animetor(); // アニメーションの初期化

        if (Hp > 0)
        {
            // 攻撃を行う、攻撃しないときは動いている。
            if (AttackFlag)
            {
                Attack();
            }
            else
            {
                // 移動した場合
                if (PrincessDistance < 1.3f || (PlayerDistance < 3.0f)) //&& Player.GetComponent<Player>().stunFlag == false
                {
                    AttackFlag = true;
                }
                else if (PrincessDistance < 25.0f || PlayerDistance < 25.0f)
                { // より近いターゲットの位置を目的地に設定する。
                    // プレイヤースタン時には姫のほうにシフトチェンジする
                    if (PrincessDistance > PlayerDistance && Player.GetComponent<Player>().stunFlag == false)
                    {
                        animator.SetBool("Running", true); // 走るアニメーションの再生
                        if (Enemy.isOnNavMesh) Enemy.destination = Player.transform.position; // プレイヤーへ移動(ナビメッシュ)
                        else Die();
                    }
                    else if (PrincessDistance < PlayerDistance || Player.GetComponent<Player>().stunFlag == true)
                    {
                        animator.SetBool("Running", true); // 走るアニメーションの再生
                        if (Enemy.isOnNavMesh) Enemy.destination = Princess.transform.position; // プリンセスへ移動(ナビメッシュ)
                        else Die();
                    }
                }
                else if (PrincessDistance > 2000.0f) // あまりにも遠い敵は消えておいてもらう
                {
                    Die();
                }
                // 移動していない場合
                else
                {
                    animator.SetBool("Idle", true); // 移動していないときのアニメーションの再生
                    Enemy.destination = Enemy.transform.position; // 動いていない時、今いる場所に行くようにすることで滑らなくする。
                }
            }
        }
        else
        {
            Enemy.destination = Enemy.transform.position; // 死んだとき時、今いる場所に行くようにすることで滑らなくする。
            Die();
        }
        
    }

    void Update()
    {
        // ポーズ中は操作を受け付けない
        if (Mathf.Approximately(Time.timeScale, 0f)) return;

        // ここで無敵時間をやる
        if (HitCount > 0)
        {
            HitCount -= Time.deltaTime;
        }
        else
        {
            HitCount = 0;
            DamageFlag = true;
        }

       
    }

    //攻撃に触れたらダメージ
    public void OnTriggerEnter(Collider collision)
    {
        if (Hp > 0 && DamageFlag)
        {
            // 通常攻撃ヒット
            if (collision.gameObject.CompareTag("NormalWeapon") || collision.gameObject.CompareTag("DollNormalWeapon"))
            {
                // ヒット処理へ
                Hit(Player.GetComponent<Player>().NattackPower);
            }
            // 必殺技ヒット
            else if(collision.gameObject.CompareTag("SkillWeapon") || collision.gameObject.CompareTag("DollSkillWeapon"))
            {
                // ヒット処理へ
                Hit(Player.GetComponent<Player>().SattackPower);
                if(HitStop.AttackFlag == true)HitStop.HitStopFlag = true;
            }
        }

    }

    public void Animetor() // アニメーションの再生を邪魔しないようにする
    {
        animator.SetBool("Idle", false);
        animator.SetBool("Walk", false);
        animator.SetBool("Running", false);
        animator.SetBool("Attack", false);
        animator.SetBool("Hit", false);
        animator.SetBool("Die", false);
    }

    public void Attack() // 攻撃をするとき
    {
        // 少し待ってから攻撃を開始する
        if (40 < Count) // カウントを0に戻す
        {
            AttackFlag = false;
            Count = 0;
        }
        else if (30 < Count) // 攻撃する
        {
            animator.SetBool("Attack", true);
            Count++;
        }
        else // カウントが30になるまで立ち尽くす
        {
            animator.SetBool("Idle", true);
            Count++;
        }
    }

    public void Hit(float Power) // 攻撃に当たった時
    {
        // エフェクトの生成
        Instantiate(DamageEffect, transform.position + transform.forward + transform.up, transform.rotation);
        SoundManager.Instance.PlaySeByName("EnemyDamage", this.gameObject); // ダメージ音// ダメージ処理
        Hp -= Power * Player.GetComponent<Attack>().Item; // プレイヤーの攻撃力分減らす
        animator.SetBool("Hit", true);
        if (Hp >= 1)
        {
            DamageFlag = false;
            HitCount = 1;
        }
        else // Hpが0になったら死ぬ
        {
            Die();
        }
    }
    public void Die() // HPを0にされたら呼び出す
    {
        animator.SetBool("Die", true);
        Body.SetActive(false); // Bodyの当たり判定を消す
        Count++;

        if (Count == 85)
        {
            DeadEffect = Instantiate(DieEffect, transform.position + transform.up, transform.rotation * Quaternion.Euler(90, 0f, 0f));
            DeadEffect.transform.SetParent(this.gameObject.transform);
        }
        if (120 < Count) // 死ぬアニメーションを再生してから消える(タイミングなので120ではないかもしれない)
        {   
            RandomDrop();
            // デストロイ
            Destroy(this.gameObject); // アイテムを出現させたら消える
        }
    }
    public void RandomDrop()
    {
        RandomDropNumber = Random.Range(0, 4); // 0～3の四択
        
        // 75%でアイテム出現
        if (RandomDropNumber != 3) // 上でランダムで選んだ数字を3以外にすることで75%にする。(50%だと出なさ過ぎたため)
        {
            // アイテム出現
            DropItemBox = GameObject.Find("DropItemBox"); // 親オブジェクトを取得
            GameObject CloneDropItem = Instantiate(dropItem, // 出現させるプレハブ名
                new Vector3(transform.position.x, transform.position.y + 1.0f, transform.position.z), // 少し高い位置にしたいので
                transform.rotation); // 角度は回転するので適当
            CloneDropItem.name = "DropItem"; // クローンしたオブジェクトの名前を変更
            CloneDropItem.transform.SetParent(DropItemBox.transform); // DropItemBoxを親に指定
        }

    }

}