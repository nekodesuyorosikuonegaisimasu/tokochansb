﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitStop : MonoBehaviour
{
    //ヒットストップ
    public static bool AttackFlag;
    public static bool HitStopFlag;
    private float HitTimer;

    private GameObject PauseManager;

    private void Start()
    {
        PauseManager = GameObject.Find("PauseManager");
    }
    // Update is called once per frame
    void Update()
    {
        //ポーズ中、リザルト中は動かないようにする
        if (PauseManager.GetComponent<Pause>().pauseFlag == false && ClearManager.ClearFlag == false)
        {
            if (gameObject.name == "Main Camera")
            {
                //ヒットストップフラグが立っているとき時間を少し止める
                if (HitStopFlag == true && AttackFlag == true)
                {
                    if ((Time.timeScale -= 0.075f * Time.unscaledDeltaTime / Time.fixedUnscaledDeltaTime) < 0.1f) Time.timeScale = 0.1f;
                    HitTimer += Time.unscaledDeltaTime;
                    if (HitTimer > 0.7f)
                    {
                        Time.timeScale = 1;
                        HitTimer = 0;
                        HitStopFlag = false;
                        AttackFlag = false;
                    }
                }
            }
            if (gameObject.name == "Sub Camera")
            {
                Time.timeScale = 1;
                HitTimer = 0;
                HitStopFlag = false;
                AttackFlag = false;
            }
        }
    }
}
