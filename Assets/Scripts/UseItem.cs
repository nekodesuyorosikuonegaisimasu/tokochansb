﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UseItem : MonoBehaviour
{
    // プレイヤーオブジェクト
    [SerializeField]
    GameObject playerObj;
    // プリンセスオブジェクト
    GameObject princess;
    // アイテムのクールタイム
    [SerializeField]
    private float CoolTime;
    public int[] itemCoolTime;
    public bool coolTimeFlag;
    // 選択したアイテムナンバー
    private int itemNumber;
    // クールタイマーオブジェクト
    GameObject coolTime;
    GameObject instance;
    // 無敵時の光オブジェクト
    GameObject invincibleLight;
    //ポーズの管理
    Pause PauseManager;

    // Start is called before the first frame update
    void Start()
    {
        // プレイヤーオブジェクト
        playerObj = GameObject.Find("Player");
        // プリンセスオブジェクト
        princess = GameObject.FindGameObjectWithTag("Princess");// 姫をtagを参照して取得
        // アイテムのクールタイムの初期化
        CoolTime = 5;
        coolTimeFlag = false;
        // クールタイマー生成
        coolTime = (GameObject)Resources.Load("CoolTimeUI");
        // 無敵時の光を取得
        invincibleLight = GameObject.Find("invincibleLight");
        // アイテムを使うまでオフにしておく
        invincibleLight.SetActive(false);
        //スクリプト取得
        PauseManager = GameObject.Find("PauseManager").GetComponent<Pause>();
    }

    // Update is called once per frame
    void Update()
    {
        // ポーズ中は操作を受け付けない
        if (Mathf.Approximately(Time.timeScale, 0f)) return;

        // アイテム使用中の効果処理
        if (coolTimeFlag) ItemTime();

        // キャラ切り替えで無敵時の光がなくなった場合
        if (invincibleLight == null) GetInvincibleLight();
        if (playerObj == null) PlayerChange();
    }
    // アイテム選択時の処理
    public void SelectedItem(int NowItem)
    {
        // 選択したアイテムのナンバーを保存
        itemNumber = NowItem;
        // クールタイム中にする（アイテムの使用不可）
        if (0 <= itemNumber && itemNumber <= 6) coolTimeFlag = true;
        // アイテム効果処理
        switch (NowItem)
        {
            case 0:// スピード
                playerObj.GetComponent<Player>().charaSpeed *= 2.5f;
                CoolTime = itemCoolTime[0];
                //StartCoroutine("ItemTime");
                break;
            case 1:// パワー
                playerObj.GetComponent<Attack>().Item *= 2.5f;
                CoolTime = itemCoolTime[1];
                break;
            case 2:// スタン時間
                playerObj.GetComponent<Player>().StunSeconds *= 0.75f;
                CoolTime = itemCoolTime[2];
                break;
            case 3:// 姫の体力
                princess.GetComponent<PrincessManager>().PHp += 10;
                CoolTime = itemCoolTime[3];
                break;
            case 4:// 攻撃クールタイム
                playerObj.GetComponent<Attack>().itemCoolTime = 2.5f;
                CoolTime = itemCoolTime[4];
                break;
            case 5:// 無敵
                CoolTime = itemCoolTime[5];
                InvincibleItem();
                break;
            case 6:// オール
                playerObj.GetComponent<Player>().charaSpeed *= 3.0f;
                playerObj.GetComponent<Attack>().Item *= 3.0f;
                princess.GetComponent<PrincessManager>().PHp += 50;
                playerObj.GetComponent<Attack>().itemCoolTime = 5.0f;
                CoolTime = itemCoolTime[6];
                InvincibleItem();
                break;
            default:
                break;
        }
        // クールタイマー生成処理
        instance = (GameObject)Instantiate(coolTime, new Vector3(0, 0, 0), Quaternion.identity);
        //クールタイムが発生
        PauseManager.PlayUI[6] = instance;
        // クールタイマー生成
        instance.GetComponent<CoolTimeUI>().Count((float)CoolTime, 650, -420);
    }
    // 無敵処理
    void InvincibleItem()
    {
        invincibleLight.SetActive(true);
        playerObj.GetComponent<Player>().invincibleTime = (float)CoolTime;
    }
    // アイテムの持続時間＆クールタイム
    void ItemTime()
    {
        CoolTime -= Time.deltaTime;
        if (CoolTime < 0) ItemTimeOver();
    }
    void ItemTimeOver()
    {
        // 処理が終わったらクールタイム終了
        coolTimeFlag = false;
        // アイテム効果終了処理
        switch (itemNumber)
        {
            case 0:// スピード
                playerObj.GetComponent<Player>().charaSpeed /= 2.5f;
                break;
            case 1:// パワー
                playerObj.GetComponent<Attack>().Item /= 2.5f;
                break;
            case 2:// スタン時間
                playerObj.GetComponent<Player>().StunSeconds /= 0.75f;
                break;
            case 3:// 姫の体力

                break;
            case 4:// クールタイム
                playerObj.GetComponent<Attack>().itemCoolTime = 1.00f;
                break;
            case 5:// 無敵
                invincibleLight.SetActive(false);
                break;
            case 6:// オール
                playerObj.GetComponent<Player>().charaSpeed /= 3.0f;
                playerObj.GetComponent<Attack>().Item /= 3.0f;
                playerObj.GetComponent<Player>().StunSeconds /= 0.5f;
                playerObj.GetComponent<Attack>().itemCoolTime = 1.0f;
                invincibleLight.SetActive(false);
                break;
            default:
                break;
        }
    }


    // 無敵時の光を探す
    void GetInvincibleLight()
    {
        // 無敵時の光を取得
        invincibleLight = GameObject.Find("invincibleLight");
        // アイテムを使うまでオフにしておく
        invincibleLight.SetActive(false);
    }
    // プレイヤー切り替え時の処理
    public void PlayerChange()
    {
        // プレイヤーオブジェクト
        playerObj = GameObject.Find("Player");
    }
}
