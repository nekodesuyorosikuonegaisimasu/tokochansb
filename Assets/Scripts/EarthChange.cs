﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EarthChange : MonoBehaviour
{
    //定値
    const int METEORITENUM = 4; //隕石の数

    //オブジェクト
    GameObject EarthObj; //地球
    GameObject BreakEarthObj; //壊れる地球
    GameObject[] MeteoriteObj = new GameObject[METEORITENUM]; //隕石

    //プレハブ
    public GameObject EarthPre; //地球
    public GameObject BreakEarthPre; //壊れる地球
    public GameObject MeteoritePre; //隕石

    //メッシュ
    MeshRenderer EarthMesh; //地球のメッシュ

    //マテリアル
    public Material[] EarthMate = new Material[5]; //地球の見た目変化用のマテリアル

    //値
    public float TimeCount; //タイミングカウント用

    //位置
    public Transform[] MeteoriteFirstPos = new Transform[METEORITENUM]; //隕石の初期位置

    //判定
    bool StartLoopflag; //ループ開始判定

    // Start is called before the first frame update
    void Start()
    {
        BreakEarthObj = null; //空
        for(int i = 0; i < METEORITENUM; i++) { MeteoriteObj[i] = null; } //すべて空

        EarthObj =  this.gameObject.transform.GetChild(0).transform.GetChild(0).transform.GetChild(0).gameObject; //子オブジェクトの検索
        EarthMesh = EarthObj.gameObject.GetComponent<MeshRenderer>(); //メッシュを取得

        EarthMesh.material = EarthMate[0]; //初めマテリアルは、0番

        TimeCount = 0; //カウントリセット

        //StartLoopflag = false; //ループしてない
        //デバック用
        StartLoopflag = true; //ループ
    }

    // Update is called once per frame
    void Update()
    {
        //ループ時のみ実行
        if (StartLoopflag)  {StartLoop();}
    }

    //ループ時の処理
    void StartLoop()
    {
        TimeCount += Time.deltaTime;
        //タイミングに合わせてマテリアルを切り替える → 壊れるオブジェクトに切り替える
        if (TimeCount > 9 && TimeCount < 11) { EarthMesh.material = EarthMate[1]; } //1番のマテリアル
        else if (TimeCount > 11 && TimeCount < 13) { EarthMesh.material = EarthMate[2]; } //2番のマテリアル
        else if (TimeCount > 13 && TimeCount < 15) { EarthMesh.material = EarthMate[3]; } //3番のマテリアル
        else if ( TimeCount > 15 && MeteoriteObj[0] == null){CreateMereorite(); } //隕石生成
        else if (TimeCount > 15 && TimeCount < 17) { EarthMesh.material = EarthMate[4]; } //4番のマテリアル
        else if (TimeCount > 17 && BreakEarthObj == null) { ChangeEarthObj(); } //オブジェクトの切り替え
    }

    //隕石生成
    void CreateMereorite()
    {
        //規定値の隕石を生成
        for (int i = 0; i < METEORITENUM; i++)
        {
            //地球から一定の距離に隕石を生成
            MeteoriteObj[i] = Instantiate(MeteoritePre, MeteoriteFirstPos[i].position, Quaternion.identity);
            //親を変更
            MeteoriteObj[i].transform.parent = this.transform.GetChild(1);
        }
    }
    

    //壊れるオブジェクトに切り替え
    void ChangeEarthObj()
    {
        //プレハブ(壊れる地球生成)
        BreakEarthObj = Instantiate(BreakEarthPre, EarthObj.transform.position, Quaternion.identity);
        BreakEarthObj.transform.parent = this.gameObject.transform;
        //前の地球を非表示
        this.gameObject.transform.GetChild(0).gameObject.SetActive(false);

    }

    //ループ時以外の処理
    void Other()
    {
        TimeCount = 0; //カウントを戻す

        //地球を戻しておく
        this.gameObject.transform.GetChild(0).gameObject.SetActive(true);
        EarthMesh.material = EarthMate[0]; //初めマテリアルは、0番
        EarthMesh = EarthObj.gameObject.GetComponent<MeshRenderer>(); //メッシュを取得
        EarthMesh.material = EarthMate[0]; //マテリアルを0番に戻す

        Destroy(BreakEarthObj); //壊れた地球を消す
        BreakEarthObj = null; //綺麗にしておく
        
        //すべての隕石に対して実行
        for (int i = 0; i < METEORITENUM; i++)
        {
            ////隕石を消す
            Destroy(MeteoriteObj[i]);
            //綺麗にしておく
            MeteoriteObj[i] = null; 
        }

    }

    //ループスタートと破棄の処理
    public void GetLoopFlag (bool LoopFlag)
    {
        StartLoopflag = LoopFlag; //判定を受け取る
        if (!StartLoopflag) { Other();} //破棄処理の時だけ実行
    }

}
