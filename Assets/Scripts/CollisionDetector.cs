﻿using UnityEngine;
using System;
using UnityEngine.Events;

[RequireComponent(typeof(Collider))]
public class CollisionDetector : MonoBehaviour
{
    [SerializeField] private TriggerEvent onTriggerEnter = new TriggerEvent();
    [SerializeField] private TriggerEvent onTriggerStay = new TriggerEvent();
    [SerializeField] private TriggerEvent onTriggerExit = new TriggerEvent();
    //is trigger がon で他の collider と重なっているときこのメソッドが常に呼ばれる。
    private void OnTriggerEnter(Collider other)
    {
        onTriggerEnter.Invoke(other);
    }
    private void OnTriggerStay(Collider other)
    {
        onTriggerStay.Invoke(other);
    }
    private void OnTriggerExit(Collider other)
    {
        onTriggerExit.Invoke(other);
    }
    //UnityEventを継承したクラスに[Sellializable]属性を付与することで、Inspectatorウインドウ上に表示できるようになる。
    [Serializable]
    public class TriggerEvent : UnityEvent<Collider>
    {

    }
}