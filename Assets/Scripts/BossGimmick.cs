﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// ボス戦のギミックを扱うcs
public class BossGimmick : MonoBehaviour
{
    public GameObject Player;   // プレイヤー
    public GameObject Dragon;   // ドラゴン

    Attack attack; // Attack.cs
    
    public int GimmickCount; // 作動すべき個数
    public int OperateCount; // 作動済みの個数

    public GameObject[] Gimmicks = new GameObject[11];

    protected BossBattleManager DragonHpUi; // ステージ3用のHp

    void Start()
    {
        Player = GameObject.Find("Player"); // Playerを探す
        Dragon = GameObject.Find("RedDragon"); // Dragonを探す
        attack = Player.GetComponent<Attack>(); // アタックの取得

        GimmckFind();

        Gimmicks[0].SetActive(true);
        Gimmicks[1].SetActive(true);

        GimmickCount = 2;

        DragonHpUi = GameObject.FindObjectOfType<BossBattleManager>();
    }
    
    void Update()
    {
        if (Dragon != null && Dragon.GetComponent<BossDragonManager>().Hp != 0)
        {
            for (int i = 0; (6 - Dragon.GetComponent<BossDragonManager>().Hp) * 2 > i; i++)
            {
                if(Gimmicks[i].GetComponent<BossLightGimmick>().OperateFlag) OperateCount++;
            }

            if (GimmickCount == OperateCount) DragonDamage();
            else OperateCount = 0;
        }
        else { }
        
    }

    public void GimmckFind()
    {
        for (int i = 0; i < 10; i++)
        {
            Gimmicks[i] = GameObject.Find("Gimmick" + i);
            Gimmicks[i].SetActive(false);
        }
    }

    public void DragonDamage()
    {
        DragonHpUi.Dragongauge();
        Dragon.GetComponent<BossDragonManager>().Hp -= 1;

        if(Dragon.GetComponent<BossDragonManager>().Hp != 0)
        {
            Gimmicks[(5 - Dragon.GetComponent<BossDragonManager>().Hp) * 2].SetActive(true);
            Gimmicks[(5 - Dragon.GetComponent<BossDragonManager>().Hp) * 2 + 1].SetActive(true);

            for (int i = 0; (6 - Dragon.GetComponent<BossDragonManager>().Hp) * 2 > i; i++)
            {
                Gimmicks[i].GetComponent<BossLightGimmick>().OperateFlag = false;

            }
            GimmickCount = (6 - Dragon.GetComponent<BossDragonManager>().Hp) * 2;
        }
    }
}

