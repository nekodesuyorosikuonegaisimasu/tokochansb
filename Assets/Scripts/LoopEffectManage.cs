﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoopEffectManage : MonoBehaviour
{
    //ゲームオブジェクト
    GameObject EffectList; //エフェクトが入っているオブジェクト
    public GameObject MainPlay; //プレイ画面

    //スクリプト
    public EarthChange EarthChangeScript; //地球の見た目
    public TimeText TimeTextScript; //時間の表示

    //スカイボックスマテリアル
    Material DefaultSkyBoxMate; //元のスカイボックスマテリア
    public Material SpaceSkyBoxMate; //宇宙のスカイボックスマテリア

    //判定
    public static bool LoopEffectFlag; //ループ中のフラグ
    public static bool EndFlag; //ループが終了した

    private void Start()
    {
        EffectList = this.gameObject.transform.GetChild(0).gameObject; //検索
        EffectList.SetActive(false); //エフェクト非表示
        DefaultSkyBoxMate = RenderSettings.skybox; //保存
        

        LoopEffectFlag = false; //ループ中ではない
        EndFlag = false;
    }

    // Update is called once per frame
    void Update()
    {
        ////ループ実行キー
        //if(Input.GetKeyDown(KeyCode.RightShift))
        //{
        //    LoopEffectFlag = true; //ループ中
        //    MainPlay.SetActive(false); //非表示
        //    DefaultSkyBoxMate = RenderSettings.skybox; //保存
        //    RenderSettings.skybox = SpaceSkyBoxMate; //スカイボックス変更
        //    EarthChangeScript.GetLoopFlag(true); //地球の動き開始
        //    TimeTextScript.StartTime(); //時間をリセット
        //    EffectList.SetActive(true); //エフェクト表示
        //}
        //if(Input.GetKeyDown(KeyCode.Backspace)|| TimeTextScript.EndFlag)
        //{
        //    EffectList.SetActive(false); //エフェクト非表示
        //    EarthChangeScript.GetLoopFlag(false); //地球の動きリセット
        //    RenderSettings.skybox = DefaultSkyBoxMate; //スカイボックス変更
        //    MainPlay.SetActive(true); //表示
        //    LoopEffectFlag =false; //ループ中ではない
        //}
        EndFlag = TimeTextScript.EndFlag; //取得
    }
    // ループ演出開始
    public void LoopStart()
    {
        LoopEffectFlag = true; //ループ中
        MainPlay.SetActive(false); //非表示
        DefaultSkyBoxMate = RenderSettings.skybox; //保存
        RenderSettings.skybox = SpaceSkyBoxMate; //スカイボックス変更
        EarthChangeScript.GetLoopFlag(true); //地球の動き開始
        TimeTextScript.StartTime(); //時間をリセット
        EffectList.SetActive(true); //エフェクト表示
    }
    // ループ演出スキップ
    public void LoopSkip()
    {
        EffectList.SetActive(false); //エフェクト非表示
        EarthChangeScript.GetLoopFlag(false); //地球の動きリセット
        RenderSettings.skybox = DefaultSkyBoxMate; //スカイボックス変更
        MainPlay.SetActive(true); //表示
        LoopEffectFlag = false; //ループ中ではない
    }
    
}
