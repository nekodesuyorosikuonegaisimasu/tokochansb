﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire : MonoBehaviour
{
    private int Speed;//飛んでいく速度
    private int Timer;//消えるまでの時間
    private float StartPosY;//飛び始めの高さ
    // Start is called before the first frame update
    void Start()
    {
        Speed = 1000;
        Timer = 120;
        StartPosY = transform.position.y;
        // 打ち出した前方にとんでいく
        GetComponent<Rigidbody>().velocity = transform.forward * Speed * Time.fixedDeltaTime;
    }
    void Update()
    {
        transform.position = new Vector3(transform.position.x,StartPosY,transform.position.z);
        transform.rotation = Quaternion.Euler(0,0,0);
    }
    //何かに触れるか一定時間経ったら消える
    void FixedUpdate()
    {
        if(Timer--<=0)Destroy(gameObject);
    }
    public void OnTriggerEnter(Collider collider)
    {
        if(!collider.transform.root.gameObject.CompareTag("Enemy"))
       Destroy(gameObject);
    }
}
