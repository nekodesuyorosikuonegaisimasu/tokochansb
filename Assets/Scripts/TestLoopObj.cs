﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestLoopObj : MonoBehaviour
{
    public GameObjects[]gameObjects;//ループオブジェクトの情報設定用

    //エディタ設定から変更できるようにする
    [System.Serializable]
    public class GameObjects        // 設定内容を設定するクラス
    {
        public string name;         // 名前
        public Material material;   // マテリアル
        public Transform transform; // 位置
        public FlgMaker flgMaker;   // フラグ配列
    }
    //エディタ設定から変更できるようにする
    [System.Serializable]
    public class FlgMaker   // フラグ設定用クラス
    {
        public string name; // 名前
        public bool Flg;    // フラグ
    }
}
