﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayDataManager : MonoBehaviour
{
    public static int WorldMax = 3;// 最大ワールド数
    public static int StageMax = 9;// 最大ステージの数

    public static int WorldStageNum = 0;   //プレイ中最後に選択したワールド
    public static int ClearStage = 0; //ゲームの進行度
    public static int OpenWorldStage = 0;//解放済みのワールド
    public static int PrincesPoint = 0;//所持プリンセスポイント
    public static int CharaNum = 0;//選択中のジョブ
    public static int[] VolumeList = new int[3]{75,75,75} ;//音量変更用(使用する際には値を1/100にする)
    public static GameObject BGM;
    public static int CameraSensitive = 900;//カメラ感度
    public static int StageNum = 0; // プレイ中最後に選択したステージ
    public static int OpenStage = 0; // 開放済みのステージ
   
    //後から編集できるようにしておきたい情報まとめ
    public static int[] CLERA_REWARD = new int[3]//ステージのクリア報酬一覧
        {500,1000,3000};
    public static string[] CHARA_NAME = new string[] {"騎士","魔法使い","奇術師","人形使い","？？？？","狩人","召喚師"};//キャラクターの職名
    public static int[] CHARA_PRICE = new int[] { 0, 500, 1000, 1500, 15000, 1000,8000}; //キャラクターの値段
    public static float[,] CHARA_STATUS = new float[,]{//各キャラのステータスを配列で管理
            { 12.5f, 1.0f, 25.0f, 5.0f, 24.0f, 10.0f, 30.0f },//騎士
            { 15.0f, 2.0f, 18.0f, 10.0f, 15.0f, 6.0f, 30.0f },//魔法使い
            { 7.0f, 1.0f, 0.0f, 5.0f, 10.0f, 7.0f, 20.0f },//奇術師
            { 7.5f, 20.0f, 30.0f, 13.0f, 18.0f, 6.0f, 30.0f },//人形使い
            { 30.0f, 3.0f, 50.0f, 15.0f, 21.0f, 20.0f, 80.0f },//ドラゴン
            { 7.5f, 1.2f, 20.0f, 3.0f, 18.0f, 15.0f, 50.0f },//狩人
   　　　　 { 2.5f, 0.7f, 0.0f, 5.0f, 8.0f, 10.0f, 20.0f } };//召喚
    //攻撃力、クールタイム(攻撃)、必殺技の攻撃力、クールタイム(スキル)、防御(スタン時間は10-防御/3)、速度、跳躍力
    public static int[] CharaLock = new int[CHARA_NAME.Length];//キャラクターの開放状況

    public static float[,] SUMMON_STATUS = new float[,] {//召喚で呼び出すもののステータス
         { 3.75f, 1.0f,15.0f, 10.0f, 50.0f,0},//ひよこ
         { 6.0f, 1.0f, 5.0f, 7.0f, 40.0f ,0},//にわとり
         { 10.0f, 2.0f, 2.0f, 5.0f, 15.0f,1},//リッチ
         { 7.5f, 2.5f, 2.0f, 3.0f, 5.0f,1},//マミー
         { 30.0f, 0.1f, 10.0f, 15.0f, 30.0f,1},//ボム
         { 10.0f, 2.0f, 7.0f, 4.0f, 10.0f,1},//オーガ
   　　  { 18.0f, 3.0f, 15.0f, 3.0f, 8.0f,1},//ゴーレム
         { 2.5f, 1.0f, 15.0f, 10.0f, 20.0f,0},//スライム
         { 2.0f, 2.0f, 5.0f, 8.0f, 20.0f,0},//タートル
         { 60.0f, 0.95f, 2.0f, 9.0f, 20.0f,1} };//鎧蟲
    //攻撃力、クールタイム(攻撃)、HP、速度、跳躍力、SE発生タイプ
    public static string[] ENEMY_NAME = new string[] {
        "Slime(Clone)",
        "TurtleShell(Clone)",
        "BossSlime",
        "BossTuetleShell",
        "Lich(Clone)",
        "RedDragon"};
    public static float[,] ENEMY_STATUS = new float[,]{//エネミーのステータス
        {20,3},//スライム
        {25,4},//タートル
        {100,10},//ビッグスライム
        {190,8},//ビッグタートル
        {50,0.5f},//リッチ
    };
    //HP、攻撃力

    private static int[] PlayData = new int[12];    //セーブ＆ロード用
    private static string PlayDataString = "";//プレイデータの配列を文字列に変更用

    //プレイデータの保存
    public static void DataSave()
    {
        //各種値を配列にまとめる
        PlayData = new int[]{ WorldStageNum, ClearStage, OpenWorldStage, PrincesPoint, VolumeList[0], VolumeList[1], VolumeList[2], CameraSensitive, CharaNum, 0, StageNum, OpenStage};
        //キャラクターの開放状況は一つにまとめる
        for(int i = 0;i<CHARA_NAME.Length;i++)PlayData[9] += CharaLock[i]*(int)Mathf.Pow(10,i);
        //配列を文字列に変換してセット      
        PlayerPrefs.SetString("プレイデータ", JsonUtility.ToJson(new ChangeStringTo<int>(PlayData)));
        //セーブする
        PlayerPrefs.Save();
    }
    //プレイデータの取得
    public static void DataLoad()
    {
        //DataSave();
        //BGMのオブジェクトが空のときはBGMを探す
        if (!BGM)BGM =  GameObject.Find("BGM");
        //ロード
        PlayDataString = PlayerPrefs.GetString("プレイデータ", JsonUtility.ToJson(new ChangeStringTo<int>(PlayData)));
        //ロードした文字列を配列に変換
        ChangeStringTo<int> String = JsonUtility.FromJson<ChangeStringTo<int>>(PlayDataString);
        //変換した結果を配列に入れる
        PlayData = String.ToType();

        //初回起動時、音量とマウス感度の値が0になるため初期値を設定
        if(PlayData[7] == 0)
        {
            for(int i = 4;i <7 ;i++)PlayData[i] = 70;
            PlayData[7] = 900;
        }

        //取得した配列を各ステータスに分配する
        WorldStageNum = PlayData[0];
        ClearStage = PlayData[1];
        OpenWorldStage = PlayData[2];
        PrincesPoint = PlayData[3];
        for (int i = 0; i < 3; i++) VolumeList[i] = PlayData[i + 4];
        BGM.GetComponent<SoundManager>().Volume = (float)VolumeList[0] / 100.0f;
        BGM.GetComponent<SoundManager>().BgmVolume = (float)VolumeList[1] / 100.0f;
        BGM.GetComponent<SoundManager>().SeVolume = (float)VolumeList[2] / 100.0f;
        CameraSensitive = PlayData[7];
        CharaNum = PlayData[8];
        for (int i = 0; i < CHARA_NAME.Length; i++)CharaLock[i] = PlayData[9]/(int)Mathf.Pow(10,i)%2;   
        StageNum = PlayData[10];
        OpenStage = PlayData[11];
        //Debug.Log(PlayDataString);
    }
    //文字配列をint型の配列に変換する
    public class ChangeStringTo<Type>
    {
        public Type[] String;
        public static string CharaLockString = "";

        //文字列を配列に変換
        public ChangeStringTo(Type[] String)
        {
            this.String = String;
        }
        //クローンを生成して返還
        public Type[] ToType()
        {
            return (Type[])String.Clone();
        }
    }
    //データの初期化
    public static void DataReset()
    {
        //BGMのオブジェクトが空のときはBGMを探す
        if (!BGM) BGM = GameObject.Find("BGM");
        //PrincesPoint = 900;
        CharaNum = 0;
        CharaLock = new int[] {1,0,0,0,0,0,0};
        ClearStage = 0;
        WorldStageNum = 1;
        OpenWorldStage = 0;
        StageNum = 1;
        OpenStage = 0;
    }
    //デバック用のデータ作成
    public static void DataDebug()
    {
        //PrincesPoint = 50000;
        CharaNum = 0;
        CharaLock = new int[] { 1, 0, 0, 0, 0, 0, 0 };
        ClearStage = StageMax;
        WorldStageNum = 1;
        OpenWorldStage = WorldMax;
        StageNum = 1;
        OpenStage = StageMax;
        DataSave();
    }　
    //データを削除する
    public static void DataDelete()
    {
        CharaLock[0] = 0;
        ClearStage = 0;
        WorldStageNum = 1;
        OpenWorldStage = 0;
        StageNum = 1;
        OpenStage = 0;
    }
}
